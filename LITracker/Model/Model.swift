//
//  Model.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import Foundation
import SwiftyJSON

// MARK: - start model for Home
class HomeSt{
    
    var payment_status             : PaymentStatusST
    var next_payment               : NextPaymentST
    var expected_date_completion   : ExpectedDateCompletionST
    var payment_schedule           : PaymentScheduleST
    var pre_auth_schedule          : PreAuthScheduleST
    var alert                      : DataST?
    
    init(_ item:[String:Any]) {
        payment_status             = PaymentStatusST(item["payment_status"] as? [String:Any] ?? [:])
        next_payment               = NextPaymentST(item["next_payment"] as? [String:Any] ?? [:])
        expected_date_completion   = ExpectedDateCompletionST(item["expected_date_completion"] as? [String:Any] ?? [:])
        payment_schedule           = PaymentScheduleST(item["payment_schedule"] as? [String:Any] ?? [:])
        pre_auth_schedule          = PreAuthScheduleST(item["pre_auth_schedule"] as? [String:Any] ?? [:])
        alert                      = DataST(item["alert"] as? [String:Any] ?? [:])
    }
    
}

class PaymentStatusST {
    var text          : String
    var flag          : FlagType
    var amount        : String
    
    
    init(_ item:[String:Any]) {
        self.text          = item["text"] as? String ?? "N/A"
        self.flag          = FlagType(item["flag"] as? String ?? "N/A")
        self.amount        = item["amount"] as? String ?? "0.00"
    }
}

class NextPaymentST {
    var date          : String
    var amount        : Int
    
    
    init(_ item:[String:Any]) {
        self.date          = item["date"] as? String ?? ""
        self.amount        = item["amount"] as? Int ?? 0
    }
}

class ExpectedDateCompletionST {
    var date          : String
    
    init(_ item:[String:Any]) {
        self.date          = item["date"] as? String ?? ""
    }
}

class PaymentScheduleST {
    var id                  : Int
    var debtor_id           : Int
    var due_date            : String
    var due_amount          : Double
    var deposited_amount    : Double
    var o_s_amount          : Double
    var complete_date       : String
    var is_complete         : Int
    
    
    init(_ item:[String:Any]) {
        self.id                 = item["id"] as? Int ?? 0
        self.debtor_id          = item["debtor_id"] as? Int ?? 0
        self.due_date           = item["due_date"] as? String ?? "N/A"
        self.due_amount         = item["due_amount"] as? Double ?? 0.0
        self.deposited_amount   = item["deposited_amount"] as? Double ?? 0.0
        self.o_s_amount         = item["o_s_amount"] as? Double ?? 0.0
        self.complete_date      = item["complete_date"] as? String ?? "N/A"
        self.is_complete        = item["is_complete"] as? Int ?? 0
    }
}

class PreAuthScheduleST {
    var id            : Int
    var debtor_id     : Int
    var due_date      : String
    var type          : String
    var status        : String
    var deposited     : Double
    var o_s_amount    : Double
    var is_complete   : Int
    
    
    init(_ item:[String:Any]) {
        self.id                 = item["id"] as? Int ?? 0
        self.debtor_id          = item["debtor_id"] as? Int ?? 0
        self.due_date           = item["due_date"] as? String ?? "N/A"
        self.type               = item["type"] as? String ?? "N/A"
        self.status             = item["status"] as? String ?? "N/A"
        self.deposited          = item["deposited"] as? Double ?? 0.0
        self.o_s_amount         = item["o_s_amount"] as? Double ?? 0.0
        self.is_complete        = item["is_complete"] as? Int ?? 0
    }
}
// MARK: - end model for Home


// MARK: Model for User
class UserSt {
    
    var id                   : Int
    var name                 : String
    var email                : String
    var phone                : String
    var estate_number        : String
    var avatar               : String
    var type                 : String
    var status               : Int
    var image                : String
    var is_first_login       : Int
    var has_two_factor_auth  : Int
    
    
    init(_ item:[String:Any]) {
        id                      = item["id"] as? Int ?? 0
        name                    = item["name"] as? String ?? ""
        email                   = item["email"] as? String ?? ""
        phone                   = item["phone"] as? String ?? ""
        estate_number           = item["estate_number"] as? String ?? ""
        avatar                  = item["avatar"] as? String ?? ""
        type                    = item["type"] as? String ?? ""
        status                  = item["status"] as? Int ?? 0
        image                   = item["image"] as? String ?? ""
        is_first_login          = item["is_first_login"] as? Int ?? 0
        has_two_factor_auth     = item["has_two_factor_auth"] as? Int ?? 0
    }
    
    static func updateMobile(_ mobile:String,_ countryCode:String){
        Auth_User.UserData.updateValue(mobile, forKey: "mobile")
        Auth_User.UserData.updateValue(countryCode, forKey: "country_code")
    }
    
    
    static func updateOnlineStatus(_ status:Bool){
        Auth_User.UserData.updateValue(status, forKey: "is_available")
    }
    
    static func updateNotifyStatus(_ value:Bool){
        Auth_User.UserData.updateValue(value, forKey: "is_notify")
    }
    
    static func updateLocations(_ value: [String:Any]) {
        Auth_User.UserData.updateValue(value, forKey: "locations")
    }
    
}


// MARK: Model for WalkthroughST
struct WalkthroughST {
    var id : Int
    var title : String
    var content : String
    var image : String
    
    init(_ item:[String:Any]) {
        self.id      = item["id"] as? Int ?? 0
        self.image   = item["image"] as? String ?? ""
        self.title   = item["title"] as? String ?? ""
        self.content = item["content"] as? String ?? ""
    }
}

// MARK: Model for TokenSt
class TokenSt {
    
    var access_token           : String
    var token_type             : String
    var expires_in             : Int
    var issued                 : String
    var expires                : String
    var refresh_token          : String
    var userId                 : Int
    
    init(_ item:[String:Any]) {
        expires_in           = item["expires_in"] as? Int ?? 0
        access_token         = item["access_token"] as? String ?? ""
        token_type           = item["token_type"] as? String ?? ""
        issued               = item[".issued"] as? String ?? ""
        expires              = item[".expires"] as? String ?? ""
        refresh_token        = item["refresh_token"] as? String ?? ""
        userId               = item["userId"] as? Int ?? 0
    }
}


// MARK: Model for TicketsST
class TicketsST {
    var id             : Int
    var title          : String
    var type           : TicketsType
    var is_active      : Int
    var message        : String
    
    
    init(_ item:[String:Any]) {
        self.id             = item["id"] as? Int ?? 0
        self.title          = item["title"] as? String ?? ""
        self.type           = TicketsType(item["type"] as? String ?? "")
        self.is_active      = item["is_active"] as? Int ?? 0
        self.message        = item["msg"] as? String ?? ""
    }
}

// MARK: Model for UnseenNotification
class UnseenNotification {
    var count_unseen_notification : Int
    
    init(_ item:[String:Any]) {
        count_unseen_notification  = item["count_unseen_notification"] as? Int ?? 0
    }
}

// MARK: Model for MessagesSt
class MessagesSt {
    
    let data: [DataST]
    let total_pages, current_page, total_records: Int
    
    init(_ item:[String:Any]) {
        
        data                 = (item["data"] as? [[String:Any]] ?? []).map {DataST($0)}
        self.total_pages     = item["total_pages"] as? Int ?? 0
        self.current_page    = item["current_page"] as? Int ?? 0
        self.total_records   = item["total_records"] as? Int ?? 0
    }
}

// MARK: - Data for Red Alert And Ticket
class DataST {
    
    let id, ticket_id       : Int
    let ticket_data         : TicketDataST
    let is_resolved         : Int
    var is_assigned         : Int
    let ticket              : TicketsST
    let created_at          : String
    let assigned_manager    : AssignedManagerST
    let managerId           : String
    
    init(_ item:[String:Any]) {
        id                  = item["id"] as? Int ?? 0
        ticket_id           = item["ticket_id"] as? Int ?? 0
        is_resolved         = item["is_resolved"] as? Int ?? 0
        is_assigned         = item["is_assigned"] as? Int ?? 0
        ticket_data         = TicketDataST(item["ticket_data"] as? [String:Any] ?? [:])
        ticket              = TicketsST(item["ticket"] as? [String:Any] ?? [:])
        created_at          = item["created_at"] as? String ?? ""
        assigned_manager    = AssignedManagerST(item["assigned_manager"] as? [String:Any] ?? [:])
        managerId           = item["manager_id"] as? String ?? ""
        
    }
    
    func toDic() -> [String:Any] {
        return [
            "id" : self.id,
            "id" : self.id,
            "ticket" : self.ticket,
            "createdAt" : self.created_at,
        ]
    }
}

class TicketDataST{
    
    //MARK: viewChangedPhone
    let phone            : String
    let email            : String
    let address          : String
    let effective_date   : String
    
    //MARK: viewCreditorCalling
    let name             : String
    let account_number   : String
    
    //MARK: viewCounsellingSession
    let method            : String
    let day              : String
    let time             : String
    
    //MARK: viewStopPayment
    let processed_date   : String
    
    //MARK: viewRequestDocument
    let types            : [String]
    
    //MARK: viewExtraPayment
    let taken_date       : String
    let amount           : String
    
    //MARK: viewTalkSomeone
    let summery          : String
    
    //MARK: viewAlert
    let message          : String
    let file             : String
    let file_name        : String
    let file_type        : String
    
    
    init(_ item:[String:Any]) {
        //MARK: viewChangedPhone
        phone                = item["phone"] as? String ?? ""
        email                = item["email"] as? String ?? ""
        address              = item["address"] as? String ?? ""
        effective_date       = item["effective_date"] as? String ?? ""
        
        //MARK: viewCreditorCalling
        name                 = item["name"] as? String ?? ""
        account_number       = item["account_number"] as? String ?? ""
        
        //MARK: viewCounsellingSession
        method               = item["method"] as? String ?? ""
        day                  = item["day"] as? String ?? ""
        time                 = item["time"] as? String ?? ""
        
        //MARK: viewStopPayment
        processed_date       = item["processed_date"] as? String ?? ""
        
        //MARK: viewRequestDocument
        types                = item["types"] as? [String] ?? []
        
        //MARK: viewExtraPayment
        taken_date           = item["taken_date"] as? String ?? ""
        amount               = item["amount"] as? String ?? ""
        
        //MARK: viewTalkSomeone
        summery              = item["summery"] as? String ?? ""
        
        //MARK: viewAlert
        message              = item["message"] as? String ?? ""
        file                 = item["file"] as? String ?? ""
        file_name            = item["file_name"] as? String ?? ""
        file_type            = item["file_type"] as? String ?? ""
        
    }
    
}

class AssignedManagerST {
    
    var id            : String
    var name          : String
    var email         : String
    
    
    init(_ item:[String:Any]) {
        self.id             = item["id"] as? String ?? ""
        self.name           = item["name"] as? String ?? ""
        self.email          = item["email"] as? String ?? ""
    }
}

// MARK: - Data for LinksSt
class LinksSt {
    var id             : Int
    var title          : String
    var url            : String
    var is_active      : Int
    
    
    init(_ item:[String:Any]) {
        self.id             = item["id"] as? Int ?? 0
        self.title          = item["title"] as? String ?? ""
        self.url            = item["url"] as? String ?? ""
        self.is_active      = item["is_active"] as? Int ?? 0
    }
}

// MARK: - Data for RequestSt, DaysSt and TimeSt
struct RequestSt {
    var id : Int
    var title : String = ""
    
    init(id: Int , title: String){
        self.id  = id
        self.title = title
    }
}

struct MethodSt {
    var id         : Int
    let name       : String
    
    init(id: Int ,name: String){
        self.id = id
        self.name = name
    }
    
}

struct DaysSt {
    var id         : Int
    let name       : String
    
    init(id: Int ,name: String){
        self.id = id
        self.name = name
    }
    
}

struct TimeSt {
    var id         : Int
    let name       : String
    
    init(id: Int ,name: String){
        self.id = id
        self.name = name
    }
    
}


//MARK: NotificationsST For all Alert

struct ChangedPhoneSt: Codable {
    
    let phone       : String
    let email       : String
    let address     : String
    let date        : String
    
    //    
    //    init(phone:String,email:String,address:String,date:String) {
    //        self.phone      = phone
    //        self.email      = email
    //        self.address    = address
    //        self.date       = date
    //    }
    //    
    //    init(_ item:[String:Any]) {
    //        phone    = item["phone"] as? String ?? ""
    //        email    = item["email"] as? String ?? ""
    //        address  = item["address"] as? String ?? ""
    //        date     = item["date"] as? String ?? ""
    //    }
    //    
    //    func toDic() -> [String:Any] {
    //        return [
    //            "phone"     : self.phone,
    //            "email"     : self.email,
    //            "address"   : self.address,
    //            "date"      : self.date
    //        ]
    //    }
}

struct NotifiSt {
    
    let id              : Int
    let sender_type     : String
    let created_at      : String
    let type            : String
    let message         : String
    let title           : String
    let deleted_at      : String
    let sender_id       : String
    let seen            : String
    let text            : String
    let action          : NotificationType
    let action_id       : Int
    let message_text    : String
    
    init(_ param : JSON) {
        self.id             = param["id"].int ?? -1
        self.sender_type    = param["sender_type"].string ?? ""
        self.created_at     = param["created_at"].string ?? ""
        self.type           = param["type"].string ?? ""
        self.message        = param["message"].string ?? ""
        self.title          = param["title"].string ?? ""
        self.deleted_at     = param["deleted_at"].string ?? ""
        self.sender_id      = param["sender_id"].string ?? ""
        self.seen           = param["seen"].string ?? ""
        self.text           = param["text"].string ?? ""
        self.action         = NotificationType(param["action"].string ?? "")
        self.action_id      = param["action_id"].int ?? -1
        self.message_text   = param["message_text"].string ?? ""
    }
}

class FormsSt {
    var id       : Int
    var month    : String
    var year     : String
    
    init(_ item:[String:Any]) {
        self.id                 = item["id"] as? Int ?? 0
        self.month              = item["month"] as? String ?? ""
        self.year               = item["year"] as? String ?? ""
    }
}

class IncomeSt {
    var id                : Int
    var name              : String
    var type              : String
    var total_income_type : String
    var fields            : [FieldsIncomeSt]
    var attachments       : [AttachmentsSt]
    
    
    init(_ item:[String:Any]) {
        self.id                 = item["id"] as? Int ?? 0
        self.name               = item["name"] as? String ?? ""
        self.type               = item["type"] as? String ?? ""
        self.total_income_type  = item["total_income_type"] as? String ?? ""
        fields                  = (item["fields"] as? [[String:Any]] ?? []).map {FieldsIncomeSt($0)}
        attachments             = (item["attachments"] as? [[String:Any]] ?? []).map {AttachmentsSt($0)}
    }
}

class FieldsIncomeSt {
    var id         : Int
    var name       : String
    var data       : DataIncomeSt
    
    
    init(_ item:[String:Any]) {
        self.id           = item["id"] as? Int ?? 0
        self.name         = item["name"] as? String ?? ""
        data              = DataIncomeSt(item["data"] as? [String:Any] ?? [:])
    }
}

class AttachmentsSt {
    var id           : Int
    var file         : String
    var created_at   : String
    var stringType   : StringType     //MARK: image or pdf Files
    var isLocalFile  : Bool           //MARK: is from api or Local
    
    
    init(_ item:[String:Any]) {
        self.id            = item["id"] as? Int ?? 0
        self.file          = item["file"] as? String ?? ""
        self.created_at    = item["created_at"] as? String ?? ""
        self.stringType    = StringType(item["stringType"] as? String ?? "")
        self.isLocalFile   = item["isLocalFile"] as? Bool ?? false
    }
}

class DataIncomeSt {
    var debtor      : Double
    var spouse      : Double
    var other       : Double
    
    init(_ item:[String:Any]) {
        self.debtor          = item["debtor"] as? Double ?? 0.0
        self.spouse          = item["spouse"] as? Double ?? 0.0
        self.other           = item["other"]  as? Double ?? 0.0
    }
}

class ExpenseSt {
    var id                : Int
    var name              : String
    var type              : String
    var total_income_type : String
    var fields            : [FieldsExpenseSt]
    var attachments       : [AttachmentsSt]
    
    init(_ item:[String:Any]) {
        self.id                  = item["id"] as? Int ?? 0
        self.name                = item["name"] as? String ?? ""
        self.type                = item["type"] as? String ?? ""
        self.total_income_type   = item["total_income_type"] as? String ?? ""
        fields                   = (item["fields"] as? [[String:Any]] ?? []).map {FieldsExpenseSt($0)}
        attachments              = (item["attachments"] as? [[String:Any]] ?? []).map {AttachmentsSt($0)}
    }
}

class FieldsExpenseSt {
    var id         : Int
    var name       : String
    var data       : DataExpenseSt
    
    
    init(_ item:[String:Any]) {
        self.id           = item["id"] as? Int ?? 0
        self.name         = item["name"] as? String ?? ""
        data              = DataExpenseSt(item["data"] as? [String:Any] ?? [:])
    }
}

class DataExpenseSt {
    var value      : Double
    
    init(_ item:[String:Any]) {
        self.value          = item["value"] as? Double ?? 0.0
        //        self.fields      = item["fields"] as? Int ?? 0
    }
}

struct IncomeObject {
    let id     : Int
    let data   : DataIncomeObject
}

struct ExpenseObject {
    let id     : Int
    let data   : DataExpenseObject
}

struct DataIncomeObject {
    let debtor : Double
    let spouse : Double
    let other  : Double
    
    func toDic() -> [String:Any] {
        return [
            "debtor" : self.debtor,
            "spouse" : self.spouse,
            "other" : self.other
        ]
    }
}

struct DataExpenseObject {
    let value : Double
}

//MARK: FAQST
class FAQST {
    var id              : Int
    var question        : String
    var answer          : String
    var created_at      : String
    var images          : [String]
    var isOpen          : Bool = false
    
    init(_ item:[String:Any]) {
       id              = item["id"] as? Int ?? 0
       question        = item["question"] as? String ?? ""
       answer          = item["answer"] as? String ?? ""
       images         = (item["images"] as? [String] ?? []).map {String($0)}
       created_at      = item["created_at"] as? String ?? ""
    }
}
