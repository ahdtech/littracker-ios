//
//  Colors.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit.UIColor
import Foundation

let PRIMARY_COLOR        = "ED1B24".color
let RED_LIGHT_COLOR      = "ED1B24".color
let GRAY_COLOR           = "616670".color
let BLUE_2C3758          = "2C3758".color
let GRAY_616670          = "616670".color
let GRAY_EEEEEE          = "EEEEEE".color
let BACKGROUND_EFF2F9    = "EFF2F9".color

let COLOR_39475A         = "39475A".color
