//
//  Strings.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

//GENERAL
let CONTENT_SIZE = "contentSize"


//titles
let SIGN_IN_TITLE                 = "Sign In".localized
let FORGOT_PASSWORD_TITLE         = "Forgot Password".localized
let CHANGE_PASSWORD_TITLE         = "Change Password".localized
let NOTIFY_MY_LIT                 = "Notify My LIT".localized
let MESSAGES                      = "Messages".localized
let HELPFUL_LINKS                 = "Helpful Links".localized
let MY_PROFILE                    = "My Profile".localized
let EDIT_PROFILE                  = "Edit Profile".localized
let NOTIFICATIONS_HOME            = "Notification Home".localized

let ATTENTION                     = "Attention".localized
let NO_CHAT_WITH_ADMIN            = "You cannot open the chat until assign a personal for your ticket".localized
let NOT_EXIST                     = "This user is currently unavailable for chat".localized

//Mark: MovedOrChanged
let ENTER_PHONE                   = "Enter your phone".localized
let ENTER_EMAIL                   = "Enter your email".localized
let ENTER_ADDRESS                 = "Enter your address".localized
let ENTER_DATE                    = "Enter the date".localized
let ENTER_TIME                    = "Enter the time".localized
let CHOOSE_DAY                    = "Select day of week".localized
let CHOOSE_TIME                   = "Select best time of day".localized

//Mark: CallingMe
let ENTER_NAME                    = "Enter your name".localized
let ENTER_ACCOUNTNUMBER           = "Enter your account number".localized

//Mark: TALKSOMEONE
let ENTER_DISCRIPTION             = "Enter your discription".localized
let FILL_ALL_DATA                 = "Fill all data".localized

// Mark: MakePayment
let ENTER_AMOUNT                  = "Enter the amount".localized

// Mark: AlertMessage
let SUCCESS_TITLE                 = "SUCCESS".localized
let SUCCESS_ACTION                = "Successfully Action".localized

let CHOOSE_DOCUMENT               = "You must select at least one document".localized

//validaition messages
let ENTER_PASSWORD                 = "Enter your password".localized
let INVALID_MOBILENUMBER           = "invalid mobile number".localized
let PHONENUMBER_9DIGITS            = "Phone number should be 9 digits".localized
let PHONENUMBER_NOT_START_ZERO     = "Phone number should not start with 0".localized
let ENTER_VALID_EMAIL              = "Enter a valid email".localized

let ENTER_NEW_PASSWORD             = "Enter New password".localized
let ENTER_CONFIRM_NEW_PASSWORD     = "Enter CONFIRM password".localized
let PasswordAndConfirmationNotMatch = "The New Password and Confirmation Password must match"

//GENERAL
let REFRESH                      = "refresh...".localized
let UPDATING                     = "updating...".localized
let PLEASE_WAIT                  = "Please wait...".localized
let NO_INTERNET_MESSAGE          = "Check your internet connection.".localized
let RETRY                        = "Try Again".localized
let CANCEL_TITLE                 = "Cancel".localized
let DELETE_TITLE                 = "Delete".localized
let BACK_TITLE                   = "Back".localized
let OK_TITLE                     = "Ok".localized
let DONE_TITLE                   = "DONE".localized
let LOGOUT                       = "Logout".localized
let DEACTIVATE                   = "Deactivate".localized
let LOGIN                        = "Login".localized
let LOGOUT_MESSAGE               = "Are you sure you want to logout?".localized
let DEACTIVATE_MESSAGE           = "Are you sure you want to deactivate your account?".localized
let SIGN_UP                      = "Sign Up".localized
let SIGN_OUT                     = "Sign Out".localized
let NO_NOTIFICATIONS             = "NO NOTIFICATIONS".localized
let DISABLED_ACCOUNT_MESSAGE     = "Your account has been disabled by admin.".localized
let EMPTY_PAYMENT_SCHEDULE       = "You don\'t have any payment schedule".localized
let EMPTY_PREAUTH_SCHEDULE       = "You don\'t have any Pre Auth schedule".localized
let ENTER_VERIFY_CODE            = "Enter verification code".localized

//Mark: Chat
let CUSTOMER_SERVICE_TITLE       = "Customer Service".localized
let OFFLINE_TITLE                = "Offline".localized
let ONLINE_TITLE                 = "Online".localized

let CAMERA                       = "Camera".localized
let GALLERY                      = "Gallery".localized
let FILES                        = "Files".localized
let PHOTOS                       = "PHOTOS".localized
let CANCEL                       = "Cancel".localized
let FAQ                          = "FAQS".localized

