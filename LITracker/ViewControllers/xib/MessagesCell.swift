//
//  MesagessCell.swift
//  LITtracker
//
//  Created by Mohammed on 4/25/21.
//

import UIKit

class MessagesCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_New: UILabel!
    
    var didPressChat:((String, String, Int) -> ())?
    var idMember:String?
    var roomId:String?
    var isAssigned  :Int?
    var is_resolved :Int?
    
    @IBOutlet weak var mainStackInfo: UIStackView!
    {
        didSet {
            mainStackInfo.isHidden = true
        }
    }

    //MARK: viewChangedPhone
    @IBOutlet weak var viewChangedPhone: UIView!
    {
        didSet {
            viewChangedPhone.isHidden = true
        }
    }
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    @IBOutlet weak var lbl_EffectiveDate: UILabel!
    
    
    //MARK: viewCreditorCalling
    @IBOutlet weak var viewCreditorCalling: UIView!
    {
        didSet {
            viewCreditorCalling.isHidden = true
        }
    }
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_AccountNumber: UILabel!
   
    
    //MARK: viewCounsellingSession
    @IBOutlet weak var viewCounsellingSession: UIView!
    {
        didSet {
            viewCounsellingSession.isHidden = true
        }
    }
    @IBOutlet weak var lbl_Method: UILabel!
    @IBOutlet weak var lbl_Day: UILabel!
    @IBOutlet weak var lbl_TimeCounselling: UILabel!
    
    
    //MARK: viewStopPayment
    @IBOutlet weak var viewStopPayment: UIView!
    {
        didSet {
            viewStopPayment.isHidden = true
        }
    }
    @IBOutlet weak var lbl_ProcessedDate: UILabel!
    
    
    //MARK: viewRequestDocument
    @IBOutlet weak var viewRequestDocument: UIView!
    {
        didSet {
            viewRequestDocument.isHidden = true
        }
    }
    @IBOutlet weak var lbl_RequestedDocument: UILabel!
    
    
    //MARK: viewExtraPayment
    @IBOutlet weak var viewExtraPayment: UIView!
    {
        didSet {
            viewExtraPayment.isHidden = true
        }
    }
    @IBOutlet weak var lbl_TakenDate: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    
    
    //MARK: viewTalkSomeone
    @IBOutlet weak var viewTalkSomeone: UIView!
    {
        didSet {
            viewTalkSomeone.isHidden = true
        }
    }
    @IBOutlet weak var lbl_Summary: UILabel!

    //MARK: viewAlert
    @IBOutlet weak var viewAlert: UIView!
    {
        didSet {
            viewAlert.isHidden = true
        }
    }
    @IBOutlet weak var lbl_Message : UILabel!
    @IBOutlet weak var lbl_FileName: UILabel!
    
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var viewFileName: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var reset_view : Bool = true { didSet { if reset_view { hide_views() } } }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func hide_views() {
        self.mainStackInfo.isHidden = true
        self.viewChangedPhone.isHidden = true
        self.viewCreditorCalling.isHidden = true
        self.viewCounsellingSession.isHidden = true
        self.viewStopPayment.isHidden = true
        self.viewRequestDocument.isHidden = true
        self.viewExtraPayment.isHidden = true
        self.viewTalkSomeone.isHidden = true
        self.viewAlert.isHidden = true
    }
    
    
    @IBAction func btnStartChatAction(_ sender: UIButton) {
        if isAssigned != 0 {
            self.didPressChat?(idMember!, roomId!, is_resolved!)
        } else {
            topMostController()?.showOkAlert(title: ATTENTION, message: NO_CHAT_WITH_ADMIN)
        }
    }
    
}
