//
//  NotifiCell.swift
//  LITtracker
//
//  Created by Mohammed on 4/22/21.
//

import UIKit

class NotifiCell: UITableViewCell {
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
