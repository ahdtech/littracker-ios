//
//  AttachmentCVC.swift
//  LITracker
//
//  Created by Mohammed on 11/18/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit

class AttachmentCVC: UICollectionViewCell {
    
    @IBOutlet weak var view_add     : UIView!
    @IBOutlet weak var view_image   : UIView!
    @IBOutlet weak var view_file    : UIView!
    
    @IBOutlet weak var btn_AddImage : UIButton!
    @IBOutlet weak var image        : UIImageView!
    @IBOutlet weak var lblName      : UILabel!
    @IBOutlet weak var btn_delete   : UIButton!
    
    var item: AttachmentsSt!
    
    var delegateDelete     : ((Int)->Void)?
    var delegateAddPhoto   : (()->Void)?
    var delegateShowPhoto  : (()->Void)?
    var delegateShowFile   : (()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func btn_ShowImage(_ sender: Any) {
        self.delegateShowPhoto?()
    }
    
    @IBAction func btn_FilesAction(_ sender: Any) {
        self.delegateShowFile?()
    }
    
    
    @IBAction func btn_AddImageAction(_ sender: Any) {
        self.delegateAddPhoto?()
    }
    
    @IBAction func btn_DeleteAction(_ sender: Any) {
        self.delegateDelete?(item.id)
    }
    
}
