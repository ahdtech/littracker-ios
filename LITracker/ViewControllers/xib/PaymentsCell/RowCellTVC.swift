//
//  RowCellTVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/25/21.
//

import UIKit

class RowCellTVC: UITableViewCell {
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var dueAmount: UILabel!
    @IBOutlet weak var depositedAmount: UILabel!
    @IBOutlet weak var osAmount: UILabel!
    @IBOutlet weak var completeDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
