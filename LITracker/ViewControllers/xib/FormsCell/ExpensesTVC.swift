//
//  ExpensesTVC.swift
//  LITracker
//
//  Created by Mohammed on 10/18/21.
//  Copyright © 2021 Snownet. All rights reserved.
//

import UIKit
import TextFieldEffects

class ExpensesTVC: UITableViewCell {
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var txt_Value: HoshiTextField!
    
    var pass_obj : ((FieldsExpenseSt) -> ())?
    
    var fieldsExpense            : FieldsExpenseSt!{didSet{setData()}}
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_Value.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData() {
        txt_Value.keyboardType = .decimalPad
        txt_Value.placeholder = fieldsExpense.name
        if fieldsExpense.data.value != 0.0 {
            txt_Value.text = fieldsExpense.data.value.styleFormat2
        }else{
            txt_Value.text = ""
        }
    }
    
}


extension ExpensesTVC : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let txt = textField.text?.replacingCharacter(newStr: string, range: range) ?? ""
        
        if textField == txt_Value {
            fieldsExpense.data.value = Double(txt) ?? 0.0
        }
        pass_obj?(fieldsExpense)
        return true
    }
}
