//
//  IncomeTVC.swift
//  LITracker
//
//  Created by Mohammed on 10/18/21.
//  Copyright © 2021 Snownet. All rights reserved.
//

import UIKit
import TextFieldEffects


class IncomeTVC: UITableViewCell {
    
    @IBOutlet weak var lbl_Title        : UILabel!
    @IBOutlet weak var txt_Debtor       : UITextField!
    @IBOutlet weak var txt_Spouse       : UITextField!
    @IBOutlet weak var txt_Other        : UITextField!
    @IBOutlet weak var lbl_TotalValue   : UILabel!
    
    var categoryID                : Int!
    var fieldsIncomeSt            : FieldsIncomeSt!{didSet{setData()}}
    var pass_obj : ((FieldsIncomeSt) -> ())?
    
    //    var didUpdateValue:(() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        txt_Debtor.delegate = self
        txt_Spouse.delegate = self
        txt_Other.delegate = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData() {
        txt_Debtor.keyboardType = .decimalPad
        txt_Spouse.keyboardType = .decimalPad
        txt_Other.keyboardType  = .decimalPad
        
        if fieldsIncomeSt.data.debtor != 0.0 {
            
            txt_Debtor.text = fieldsIncomeSt.data.debtor.styleFormat2
        }else{
            txt_Debtor.text = ""
        }
        
        if fieldsIncomeSt.data.spouse != 0.0 {
            txt_Spouse.text = fieldsIncomeSt.data.spouse.styleFormat2
        }else{
            txt_Spouse.text = ""
        }
        
        if fieldsIncomeSt.data.other != 0.0 {
            txt_Other.text = fieldsIncomeSt.data.other.styleFormat2
        }else{
            txt_Other.text = ""
        }
        
    }
    
    @IBAction func debtorEditingChanged(_ sender: Any) {
        //        didUpdateValue?()
    }
    
    @IBAction func spouseEditingChanged(_ sender: Any) {
        //        didUpdateValue?()
    }
    
    @IBAction func otherEditingChanged(_ sender: Any) {
        //        didUpdateValue?()
    }
    
}


extension IncomeTVC : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let txt = textField.text?.replacingCharacter(newStr: string, range: range) ?? ""
        
        if textField == txt_Debtor {
            fieldsIncomeSt.data.debtor = Double(txt) ?? 0.0
        } else if textField == txt_Spouse {
            fieldsIncomeSt.data.spouse = Double(txt) ?? 0.0
        } else if textField == txt_Other {
            fieldsIncomeSt.data.other = Double(txt) ?? 0.0
        }
        
        pass_obj?(fieldsIncomeSt)
        return true
    }
}

