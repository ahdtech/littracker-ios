//
//  FormsMainCVC.swift
//  LITracker
//
//  Created by Mohammed on 10/17/21.
//  Copyright © 2021 Mohammed Alefrangy. All rights reserved.
//

import UIKit

class FormsMainCVC: UICollectionViewCell {
    
    @IBOutlet weak var tableView   : UITableView!
    var req_type                   : IncomType = .Incomes
    
    var income              : IncomeSt!  {didSet{configure()}}
    var expense             : ExpenseSt! {didSet{configure()}}
    
    var pass_obj_Income     : ((IncomeSt) -> ())?
    var pass_obj_Expense    : ((ExpenseSt) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: IncomeTVC.self)
        tableView.registerCell(id: ExpensesTVC.self)
        tableView.reloadData()
    }

}

extension FormsMainCVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if req_type == .Incomes {
            return income.fields.count
        } else {
            return expense.fields.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if req_type == .Incomes {
            let cell : IncomeTVC = tableView.dequeueTVCell()
            let item = income.fields[indexPath.item]
            cell.selectionStyle = .none
            cell.lbl_Title.text  = item.name
            cell.fieldsIncomeSt = item
            cell.pass_obj = { obj in
                self.income.fields[indexPath.item] = obj
                self.pass_obj_Income?(self.income)
            }

            return cell
            
        } else {
            let cell : ExpensesTVC = tableView.dequeueTVCell()
            let item = expense.fields[indexPath.item]
            cell.selectionStyle = .none
            cell.lbl_Title.text = item.name
            cell.fieldsExpense = item
            cell.pass_obj = { obj in
                self.expense.fields[indexPath.item] = obj
                self.pass_obj_Expense?(self.expense)
            }
            return cell
        }
       
    }
    
    private func didUpdateValue(){
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
