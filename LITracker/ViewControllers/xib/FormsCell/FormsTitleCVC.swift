//
//  FormsTitleCVC.swift
//  LITracker
//
//  Created by Mohammed on 10/17/21.
//  Copyright © 2021 Mohammed Alefrangy. All rights reserved.
//

import UIKit

class FormsTitleCVC: UICollectionViewCell {
    
    @IBOutlet weak var lbl_Title        : UILabel!
    
    @IBOutlet weak var imageViewBorder  : UIImageView!
    @IBOutlet weak var lbl_Number       : UILabel!
    @IBOutlet weak var viewNumber       : UIView!
    
    @IBOutlet weak var imageView_Done   : UIImageView!
    @IBOutlet weak var lineView         : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

}
