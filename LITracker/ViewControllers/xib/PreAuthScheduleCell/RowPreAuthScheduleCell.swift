//
//  RowPreAuthScheduleCell.swift
//  LITracker
//
//  Created by Mohammed on 12/20/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit

class RowPreAuthScheduleCell: UITableViewCell {
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var dueDate: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var deposited: UILabel!
    @IBOutlet weak var osAmount: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
