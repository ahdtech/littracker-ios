//
//  SplashVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit

class SplashVC: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    private func configure(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            Route.startApp()
        }
    }
    
}
