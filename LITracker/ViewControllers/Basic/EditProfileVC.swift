//
//  EditProfileVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/25/21.
//

import UIKit

class EditProfileVC: BasicVC {
    
    @IBOutlet weak var txtAdress: UILabel!
    
    private var latitude : Double?
    private var longitude : Double?
    private var addressStr : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        configure()
    }
    
    
    func configure(){
        title = EDIT_PROFILE
        hideBackWord()
    }
    
    @IBAction func btnAdressAction(_ sender: Any) {
        let shared = AddressHandle.shared
        shared.showPlacePicker(self, latitude, longitude)
        shared.didPickShortAddress =  didPickAddress
    }
    
    private func didPickAddress(_ addres:String,_ lat:Double,_ lon:Double) {
        txtAdress.text = addres
        addressStr = addres
        latitude = lat
        longitude = lon
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        showToast(SUCCESS_ACTION)
        popVC()
    }
    
}
