//
//  ChangePasswordVC.swift
//  LITracker
//
//  Created by Mohammed on 10/21/21.
//  Copyright © 2021 Snownet. All rights reserved.
//

import UIKit
import TextFieldEffects

class ChangePasswordVC: UIViewController {

//    @IBOutlet weak var txt_oldPassword        : HoshiTextField!
    @IBOutlet weak var txt_NewPassword        : HoshiTextField!
    @IBOutlet weak var txt_ConfirmNewPassword : HoshiTextField!
    private let _user = Auth_User.UserInfo
    var oldPassword : String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        Auth_User.isFirstLogin = false
        configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.hideBackWord()
//        setNavigationBarHidden(false)
//        Auth_User.removeUserValues()
    }
    
    func configure(){
        title = CHANGE_PASSWORD_TITLE
//        self.hideBackWord()
    }
    
    //MARK: save
    @IBAction func saveAction(_ sender:UIButton){
        
        guard let New = txt_NewPassword.text, !New.TrimWhiteSpaces.isEmpty else {
            self.showToast(ENTER_NEW_PASSWORD)
            return
        }
        
        guard let CONFIRMNew = txt_ConfirmNewPassword.text, !CONFIRMNew.TrimWhiteSpaces.isEmpty else {
            self.showToast(ENTER_CONFIRM_NEW_PASSWORD)
            return
        }

        let Repass = txt_ConfirmNewPassword.text ?? ""

        guard New == Repass else {
            self.showToast(PasswordAndConfirmationNotMatch)
            return
        }
        
        view.endEditing(true)
        showIndicator()
        
        let param = ["old_password" : Auth_User.Password,
                     "password" : New,
                     "password_confirmation" : Repass]
        
       
        MyApi.api.changePassword(param) { (msg, status) in
            if status {
                Auth_User.Password = New
                self.showToast(msg)
                self.hideIndicator()
                self.popVC()
            } else {
                self.showToast(msg)
                self.hideIndicator()
            }
            
        }
        
    }

}
