//
//  ForgotPasswordVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit
import TextFieldEffects


class ForgotPasswordVC: BasicVC {
    
    @IBOutlet weak var txt_email: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    
    func configure(){
        title = FORGOT_PASSWORD_TITLE
    }
    
    
    //MARK: singup
    @IBAction func sendAction(_ sender:UIButton){
        
        guard let email = txt_email.text, !email.isEmptyStr else {
            showOkAlert(title: ATTENTION, message: ENTER_EMAIL)
            return
        }
        
        guard email.trimmed.isEmailValid else {
            showOkAlert(title: ATTENTION, message: ENTER_VALID_EMAIL)
            return
        }
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        
        showIndicator()
        MyApi.api.ForgotPasswordRequest(email: email) { (message, status) in
            self.hideIndicator()
            guard status else { self.showToast(message) ; return }
            self.showToast(message)
            self.popVC()
        }
        
    }
    
}
