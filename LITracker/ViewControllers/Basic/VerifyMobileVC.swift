//
//  VerifyMobileVC.swift
//  LITracker
//
//  Created by Mohammed on 11/18/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit
import SVPinView

class VerifyMobileVC: UIViewController {
    
    @IBOutlet weak var lblVerify: UILabel!
    @IBOutlet weak var lblReset: UILabel!
    @IBOutlet weak var pin_view: SVPinView!
    @IBOutlet weak var lbl_mobile: UILabel!
    @IBOutlet weak var btn_resendSMS: UIButton!
    @IBOutlet weak var lbl_timer: UILabel!
    @IBOutlet weak var lbl_verfiyMobileNumber: UILabel!
    
    var user_id = -1
    var phone = ""
    var password = ""
    private var _code = ""
    private var timer = Timer()
    private var countDown = 30

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        setupPinView()
        startTimer()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarHidden(false)
    }

    func configure(){
        hideBackWord()
        setPhone()
    }

    func setPhone(){
        lbl_verfiyMobileNumber.text = "Enter the code you have received on your email (\(phone))"
//        lbl_mobile.text = "+" + phone
    }

    //MARK: set pin view
    func setupPinView(){
        pin_view.pinLength = 4
        pin_view.interSpace = 20
        pin_view.textColor = COLOR_39475A
        pin_view.shouldSecureText = false
        pin_view.allowsWhitespaces = false
        pin_view.style = .underline
        
        pin_view.borderLineColor = "B1B1B1".color
        pin_view.activeBorderLineColor = "B1B1B1".color
        pin_view.borderLineThickness = 1
        pin_view.activeBorderLineThickness = 1
        // pin_view.placeholder = "1234"
        pin_view.keyboardType = .asciiCapableNumberPad
        pin_view.isContentTypeOneTimeCode = true
        pin_view.didChangeCallback = { _ in
            self._code = ""
        }
        pin_view.didFinishCallback = { code in
            self._code = code
        }
    }

    // MARK: Verify Action
    @IBAction func verifyAction(_ sender:UIButton){
        
        guard !_code.isEmpty else {
            self.showToast(ENTER_VERIFY_CODE)
            return
        }
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.VerifyCodeRequest(_code,password) { (message, status, statusCode) in
            self.hideIndicator()
            if status {
                Route.goHome()
            }else{
                self.showMessageAlert(message)
            }
        }
    }
    
    // MARK: resend verify code
    @IBAction func reSendCode(_ sender:UIButton){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.ResendCodeRequest(phone) { (message, status) in
            self.hideIndicator()
            if !status{self.showMessageAlert(message);return}
            self.setTimerStyle()
        }
    }
     

    //MARK: set timer
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,
                                     selector: #selector(activationTimer), userInfo: nil, repeats: true)
        timer.fire()
    }

    @objc func activationTimer() {
        if countDown == 1 {
            countDown -= 1
            lbl_timer.isHidden = true
            btn_resendSMS.isHidden = false
            timer.invalidate()
            return
        }
        
        countDown -= 1
        setCountDownStyle()
    }

    func setTimerStyle(){
        self.btn_resendSMS.isHidden = true
        self.lbl_timer.isHidden = false
        self.countDown = 30
        self.setCountDownStyle()
        self.timer.invalidate()
        self.startTimer()
    }

    func setCountDownStyle() {
        lbl_timer.text = "00:\(countDown)"
    }

}
