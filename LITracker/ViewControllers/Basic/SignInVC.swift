//
//  SignInVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit
import TextFieldEffects
import Firebase

class SignInVC: UIViewController {
    
    @IBOutlet weak var txt_userEmail: HoshiTextField!
    @IBOutlet weak var txt_password: HoshiTextField!
    private let _user = Auth_User.UserInfo
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure(){
        title = SIGN_IN_TITLE
        hideBackWord()
        
        #if DEBUG
//        txt_userEmail.text    = "mousa@lit.com"
      txt_userEmail.text    = "hashisho59@gmail.com"
        txt_password.text     = "123456"
        #endif
    }
    
    //MARK: signin
    @IBAction func signinAction(_ sender:UIButton){
        
        guard let email = txt_userEmail.text, !email.isEmptyStr else {
            showToast(ENTER_EMAIL)
            return
        }
        
        
        guard let password = txt_password.text, !password.isEmptyStr else {
            showToast(ENTER_PASSWORD)
            return
        }
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        
        self.view.endEditing(true)
        showIndicator()
        MyApi.api.LoginRequest(email, password) { (message, status, statusCode) in
            self.hideIndicator()
//            guard status else {self.showMessageAlert(message);return}
            
            if status {
//                MyApi.api.LoginFirebaseRequest(email, password) { (message, status) in
//                    if !status {
////                        self.showMessageAlert(message)
////                        return
//
//                        let user = Auth_User.UserInfo
//                        if user.has_two_factor_auth == 1 {
//                            let vc : VerifyMobileVC = AppDelegate.MainSB.instanceVC()
//                            vc.user_id = user.id
//                            vc.phone = user.phone
//                            vc.password = password
//                            self.pushNavVC(vc)
//                            return
//                        }
//                        Auth_User.User_Id = user.id
//                        Auth_User.Password = password
//                        Auth_User.isFirstLogin = user.is_first_login == 1 ? true : false
//
//                        Route.goHome()
//
//                    } else {
//
//                        let user = Auth_User.UserInfo
//                        if user.has_two_factor_auth == 1 {
//                            let vc : VerifyMobileVC = AppDelegate.MainSB.instanceVC()
//                            vc.user_id = user.id
//                            vc.phone = user.phone
//                            vc.password = password
//                            self.pushNavVC(vc)
//                            return
//                        }
//                        Auth_User.User_Id = user.id
//                        Auth_User.Password = password
//                        Auth_User.isFirstLogin = user.is_first_login == 1 ? true : false
//
//                        Route.goHome()
//
//                    }
//                }
                Auth.auth().signInAnonymously { authResult, error in

                    let user = Auth_User.UserInfo
                    if user.has_two_factor_auth == 1 {
                        let vc : VerifyMobileVC = AppDelegate.MainSB.instanceVC()
                        vc.user_id = user.id
                        vc.phone = user.email
                        vc.password = password
                        self.pushNavVC(vc)
                        return
                    }
                    Auth_User.User_Id = user.id
                    Auth_User.Password = password
                    Auth_User.isFirstLogin = user.is_first_login == 1 ? true : false
                    
                    Route.goHome()
                }
            
            
            } else {
                self.showMessageAlert(message)
                return
            }
            
        }
        
    }
    
    //
    @IBAction func forgotAction(_ sender:UIButton){
        let vc : ForgotPasswordVC = AppDelegate.MainSB.instanceVC()
        pushNavVC(vc)
    }
    
}


extension SignInVC: UITextFieldDelegate {
    
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //        <#code#>
    //    }
    
}
