//
//  WalkthoughVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit

class WalkthoughVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pager: UIPageControl!
    private var array : [WalkthroughST] = []
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnPervious: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnPervious.isHidden = true
        configure()
        getData()
    }
    
    private func configure(){
        setNavigationBarHidden()
        
        pager.numberOfPages = array.count //0
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        Auth_User.FirstOpen.toggle()
        Route.goSignIn()
    }
//    @IBAction func btnContinueAction(_ sender: Any) {
//        Auth_User.FirstOpen = false
//        Route.goSignIn()
//    }
    
    
    private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getInstruction { (items, message, status) in
            self.hideIndicator()
            self.array = items
            self.collectionView.reloadData()
            self.hideButton()
            self.pager.numberOfPages = self.array.count
        }
    }
    
    private func hideButton(){
        if pager.currentPage == 0 {
            btnPervious.isHidden = true
            btnContinue.isHidden = true
            btnNext.isHidden = false
        } else if pager.currentPage == array.count - 1 {
            btnPervious.isHidden = false
            btnContinue.isHidden = false
            btnNext.isHidden = true
        }else {
            btnPervious.isHidden = false
            btnNext.isHidden = false
            btnContinue.isHidden = true
        }
    }
    
    @IBAction func nextAction(_ sender:UIButton){
        guard !array.isEmpty else {return}
        guard let indexPath = collectionView.indexPathsForVisibleItems.first else {return}
        guard indexPath.item < 2 else {Route.goSignIn();Auth_User.FirstOpen.toggle();return}
        let index_path = IndexPath(row: indexPath.item + 1, section: 0)
        collectionView.scrollToItem(at: index_path, at: .centeredHorizontally, animated: true)
        pager.currentPage = indexPath.item + 1
        hideButton()
    }
    
    @IBAction func previousAction(_ sender:UIButton){
        guard !array.isEmpty else {return}
        guard let indexPath = collectionView.indexPathsForVisibleItems.first else {return}
        let index_path = IndexPath(row: indexPath.item - 1, section: 0)
        collectionView.scrollToItem(at: index_path, at: .centeredHorizontally, animated: true)
        pager.currentPage = indexPath.item - 1
        hideButton()
    }
    
}


extension WalkthoughVC : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : WalkthoughCVCell = collectionView.dequeueCVCell(indexPath: indexPath)
        cell.row = indexPath.item
        cell.item = array[indexPath.item]
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard let indexPath = collectionView.currentIndex else { return }
        pager.currentPage = indexPath.item
        hideButton()
    }
}

extension WalkthoughVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionView.frame.size
    }
}


class WalkthoughCVCell: UICollectionViewCell {
    
    @IBOutlet weak var img_view: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_content: UILabel!
    
    var row : Int = 0
    var item : WalkthroughST!{didSet{setData()}}
    
    private func setData(){
        //        img_view.fetchImage(item.image)
        img_view.image = "intro-\(row)".toImage
        lbl_title.text = item.title
        lbl_content.text = item.content
        //lbl_content.setParagraphLine(lbl_content.text ?? "", 4)
    }
    
}

