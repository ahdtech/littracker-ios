//
//  ChatsCell.swift
//  help
//


import UIKit


class chats{
    
    var senderName: String
    var message: String
    var senderImage: String
    var time: String
    
    init(senderName: String, message:String, senderImage:String, time:String) {
        
        self.senderName = senderName
        self.message = message
        self.senderImage = senderImage
        self.time = time
        
    }
}

class ChatsCell: UITableViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var senderImageView: UIImageView!
    @IBOutlet weak var senderNameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
