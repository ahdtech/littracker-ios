//
//  ChatViewController.swift
//

import UIKit
import JSQMessagesViewController
import Firebase
import FirebaseCore
import FirebaseDatabase
import FirebaseMessaging
import FirebaseStorage
import MobileCoreServices
import AVKit
import AMPopTip
import IQKeyboardManagerSwift
import NSDate_TimeAgo
import FirebaseStorage
import ImageSlideshow
import Alamofire



class ChatViewController: JSQMessagesViewController , UINavigationControllerDelegate, UIGestureRecognizerDelegate, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var view_title: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var btn_status: UIButton!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_unseen: UILabel!
    
    var chatRoomId: String!
    var isResolved: Int = 0
    var fromPervVC       : Bool = false
    var fromRedAlert     : Bool = false
    var fromRedAlertHome : Bool = false
    
    var reciverInfo: User!
    var adminID = "0"
    
    var messages = [JSQMessage]()
    var pageSize = 20
    let preloadMargin = 5
    var lastLoadedPage = 0
    var insertCounter = 0
    var popTip = PopTip()
    
    var  allMessagesId = [""]
    private var  array_unseen_messgaesIds : [String] = []
    private var  fisrt_unseen_messgaesId : String = ""
    private var myUserId = Auth_User.User_Id
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    var databaseRef: DatabaseReference! {
        return Database.database().reference()
    }
    
    private var userIsTypingRef: DatabaseReference!
    private var userIsActive: DatabaseReference!
    private var fetchingData: DatabaseReference!
    private var checkUserChangedValue: DatabaseReference!
    
    private var isReciverActive : Bool = false
    private var reciverAvatar :UIImage? = "guset".toImage
    private var senderAvatar :UIImage? = "guset".toImage
    
    private var localTyping: Bool = false
    private var count = 0
    private var isActiveHandle: DatabaseHandle?
    private var isTypingHandle: DatabaseHandle?
    private var FirstFetch: DatabaseHandle?
    private var checkUserChangedValueHandel: DatabaseHandle!
    
    deinit
    {
        self.func_removeObserver()
    }
    
    var isFromMap = false
    private var urlForFile: String = ""
    var documentInteractionController: UIDocumentInteractionController!
    var fileExten: String?
    private var ticketData: DataST?
        
    override func viewDidLoad(){
        super.viewDidLoad()
        getDataForTicket()
        if fromRedAlert {
            readRedAlert()
        }

        
        addNotifcaionObserver(.UpdateChatViewController, #selector(ToolBarStyel))
        
        self.collectionView.semanticContentAttribute = .forceLeftToRight
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.showLoadEarlierMessagesHeader = self.count > 20
        
        self.downloadAvatrImage(url: self.reciverInfo.UserImage)
        
        //
        if reciverInfo.UserId == Constant.Admin_Id.toString {
            self.lbl_title.text = "LIT"
            self.setOnlineStatus()
        }else {
            self.lbl_title.text = "LIT"
        }
        
        self.getMyAvatar()
        
        CountOfMessages()
        typingObserver()
        observeTypingUser()
        
        
        // change bubble background color
        let factory = JSQMessagesBubbleImageFactory()
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(with: "E8E8E8".color)
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(with: "6DB43F".color)//"6DB43F".color or PRIMARY_COLOR)
        
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize(width:0,height:0)
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize(width:0,height:0)
        collectionView.collectionViewLayout.messageBubbleFont = MyTools.appFont(size: 15)
        collectionView.backgroundColor = .clear
        
        //Mark : add background to chat
        //        let image = UIImage(named: "background")
        //        let imageView = UIImageView(image: image)
        //        imageView.frame = CGRect(x: 0, y: 0, width:  collectionView.frame.size.width, height: collectionView.frame.size.height)
        //        self.view.addSubview(imageView)
        // Mark - Customize Chat
        self.func_chagenToolBarStyel()
        
        self.fetchMessages()
        self.observeForIsResolved()
        
        IQKeyboardManager.shared.enable = false
        
        self.func_checkAppStatus()
        self.func_userDataChangeOBserver()
        self.setTitleViewSize()
        
    }
    
    
    private func getDataForTicket(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getDataTicket(id: Int(chatRoomId)!) { (tickets, message, status) in
            print("The data ios \(tickets.ticket_data.message)")
            self.ticketData = tickets
            self.setData()
            self.hideIndicator()
        }
    }
    
    
    
    func setData() {
        
        let data = self.ticketData!
        let type = data.ticket.type
        switch type {
        
        case .changed_phone:
            self.viewChangedPhone.isHidden = false
            self.lbl_PhoneNumber.text   = data.ticket_data.phone
            self.lbl_Email.text         = data.ticket_data.email
            self.lbl_Address.text       = data.ticket_data.address
            self.lbl_EffectiveDate.text = data.ticket_data.effective_date
            
        case .creditor_calling:
            self.viewCreditorCalling.isHidden = false
            self.lbl_Name.text          = data.ticket_data.name
            self.lbl_AccountNumber.text = data.ticket_data.account_number

        case .counselling_session:
            self.viewCounsellingSession.isHidden = false
            self.lbl_Method.text          = data.ticket_data.method
            self.lbl_Day.text             = data.ticket_data.day
            self.lbl_TimeCounselling.text = data.ticket_data.time

        case .stop_payment:
            self.viewStopPayment.isHidden = false
            self.lbl_ProcessedDate.text = data.ticket_data.processed_date
            
        case .request_document:
            self.viewRequestDocument.isHidden = false
            let arr = data.ticket_data.types[0].map { (text) -> String in
                return String(text)
            }.joined(separator: "\n")

            self.lbl_RequestedDocument.text = arr
            self.lbl_RequestedDocument.text = data.ticket_data.types.joined(separator: "\n")
            
        case .extra_payment:
            self.viewExtraPayment.isHidden = false
            self.lbl_TakenDate.text = data.ticket_data.taken_date
            self.lbl_Amount.text    = data.ticket_data.amount
            
        case .talk_someone:
            self.viewTalkSomeone.isHidden = false
            self.lbl_Summary.text = data.ticket_data.summery

        case .alert:
            self.viewAlert.isHidden = false
            self.viewMessage.isHidden = data.ticket_data.message.isEmpty
            self.viewFileName.isHidden = data.ticket_data.file_name.isEmpty
            self.lbl_Message.text = data.ticket_data.message
            self.lbl_FileName.text = data.ticket_data.file_name

        case .None:
            break
        }
    }
    
    func readRedAlert() {
        
        let params : [String:Any] = ["ticket_id" : chatRoomId!]
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        MyApi.api.ReadAlert(params) { (message, status) in
            print("The read alert is \(message)")
            guard status else {return}
        }
    }
    
    
    func observeForIsResolved() {
        Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId).child("is_resolved").observe(.value, with: { (snapshot) in
            print("snapshot \(snapshot)")
            if snapshot.exists(){
                let is_resolved =  snapshot.value
                print("is_resolvedman\(is_resolved!)")
                
                if is_resolved as? Int == 1 {
                    self.ToolBarStyel()
                }else {
                    self.inputToolbar.isHidden = false
                }
            }
                
            }, withCancel: nil)
    }
    
    func setTitleViewSize() {
        view_title.frame = CGRect(x: 0, y: 0, width: 280, height: 40)
        NSLayoutConstraint.activate([
            view_title.topAnchor.constraint(equalTo: view_title.topAnchor),
            view_title.bottomAnchor.constraint(equalTo: view_title.bottomAnchor),
            view_title.centerXAnchor.constraint(equalTo: view_title.centerXAnchor,
                                                constant: 0),
            view_title.widthAnchor.constraint(equalToConstant: 280)
        ])
    }
    
    @objc func ToolBarStyel() {
        self.inputToolbar.isHidden = true
        self.viewResolved.isHidden = false
    }
    
    
    func func_removeObservers(){
        if  isActiveHandle != nil
        {
            userIsActive.removeObserver(withHandle: isActiveHandle!)
        }
        if  isTypingHandle != nil
        {
            userIsTypingRef.removeObserver(withHandle: isTypingHandle!)
        }
        
        if  FirstFetch != nil
        {
            fetchingData.removeObserver(withHandle: FirstFetch!)
        }
        
        if  checkUserChangedValueHandel != nil
        {
            checkUserChangedValue.removeObserver(withHandle: checkUserChangedValueHandel!)
        }
        
        self.func_setUserAcive(false)
    }
    
    
    func func_chagenToolBarStyel(){
        if isResolved == 1{
            self.inputToolbar.isHidden = true
        }else {
            self.inputToolbar.isHidden = false
        }
        
        self.inputToolbar.contentView.textView.font = MyTools.appFont(size: 14)
        let sendImage = "send".toImage.imageFlippedForRightToLeftLayoutDirection()
        let attachImage = UIImage(named: "attach")
        // let emojyImage = UIImage(named: "attach")
        
        let sendButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        let attachButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        // let emojyButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        
        attachButton.setImage(attachImage, for: .normal)
        // emojyButton.setImage(emojyImage, for: .normal)
        sendButton.setImage(sendImage, for: .normal)
        self.inputToolbar.contentView.rightBarButtonItem = sendButton
        self.inputToolbar.contentView.leftBarButtonItem = attachButton
        self.inputToolbar.contentView.backgroundColor = "FBFBFB".color
        self.inputToolbar.contentView.textView.setBorder(width: 1, color: "E9E9F3".color)
        self.inputToolbar.contentView.textView.setRounded()
        self.inputToolbar.contentView.textView.placeHolder = "Write message here".localized
        
    }
    
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.setNavigationBarHidden(false)
        
        Constant.DBRefrence.child(Constant.Seen).child(self.chatRoomId).child(senderId).setValue(["counter":0])
        self.func_setUserAcive(true)
        
        if reciverInfo.UserId != Constant.Admin_Id.toString {
            observeIsActive()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        self.setNavigationBarHidden()
        self.func_setUserAcive(false)
        IQKeyboardManager.shared.enable = true
        self.func_removeObserver()
        self.func_removeObservers()
        did_home_load = false
    }
    
    fileprivate func observeIsActive() {
        
        self.userIsActive = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId).child(Constant.Active).child(adminID)
        isActiveHandle = self.userIsActive.observe(.value, with: { (snapshot) in
            if snapshot.exists() {
                let value = snapshot.value as! [String:Any]
                let status = value["Active"] as! Bool
                self.isReciverActive = status
                switch status {
                case false :
                    self.self.setOfflineStatus()
                default:
                    self.setOnlineStatus()
                }
            }else {
                self.setOfflineStatus()
            }
        })
    }
    
    func setOnlineStatus(){
        lbl_status.text = ONLINE_TITLE
        lbl_status.textColor = "00ABA3".color
        btn_status.backgroundColor = "00ABA3".color
    }
    
    func setOfflineStatus(){
        lbl_status.text = OFFLINE_TITLE
        lbl_status.textColor = "8AA0B7".color
        btn_status.backgroundColor = "8AA0B7".color
    }
    
    
    //MARK: check app status
    func func_checkAppStatus(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(func_setOffline), name: UIApplication.willResignActiveNotification, object: nil)
        
        notificationCenter.addObserver(self, selector: #selector(func_setOnline), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func func_removeObserver(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func func_setOffline() {
        self.func_setUserAcive(false)
    }
    
    @objc func func_setOnline() {
        Constant.DBRefrence.child(Constant.Seen).child(self.chatRoomId).child(senderId).setValue(["counter":0])
        self.func_setUserAcive(true)
    }
    
    func func_setUserAcive(_ active:Bool){
        Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId)
            .child("Active")
            .child(senderId)
            .setValue(["Active":active])
    }
    
    
    func func_userDataChangeOBserver(){
        
        self.checkUserChangedValue = Constant.DBRefrence.child(Constant.UserNode)
            .child(self.reciverInfo.UserId)
        checkUserChangedValueHandel =   self.checkUserChangedValue.observe(.value) { (snapshot) in
            let user = User(snapshot: snapshot)
            self.reciverInfo = user
        }
    }
    
    //MARK: Load more
    func LoadMore(Size:Int) {
        self.messages.removeAll()
        self.allMessagesId.removeAll()
        let messageQuery = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId).child(Constant.messages)
            .queryLimited(toLast: UInt(Size))
        
        messageQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            for snap in snapshot.children
            {
                let msg = Message(snapshot: snap as! DataSnapshot)
                
                let cDate = Date(seconds: msg.Timestamp)
                
                self.allMessagesId.append(snapshot.key)
                
                switch msg.mediaType
                {
                case "text":
                    //replcae name not used with message id
                    let msg = JSQMessage(senderId: msg.SenderUid, senderDisplayName: msg.key, date: cDate, text: msg.Message)
                    self.messages.append(msg!)
                    break
                case "image" :
                    let photo = JSQPhotoMediaItem(image: "chat_placeholder".toImage)
                    let _urlStr = msg.mediaUrl
                    
                    let mss = JSQMessage(senderId: msg.SenderUid,
                                         senderDisplayName: msg.key,
                                         date: cDate,
                                         media: photo,
                                         url: _urlStr)
                    
                    self.messages.append(mss!)
                    break
                case "file":
                    //replcae name not used with message id
                    
                    let msg = JSQMessage(senderId: msg.SenderUid, senderDisplayName: snapshot.key, date: cDate, text: msg.Message, urlFile: msg.mediaUrl)
                    self.messages.append(msg!)
                    break
                default: break
                }
            }
            self.finishReceivingMessage()
            self.collectionView.reloadData()
            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
            self.showLoadEarlierMessagesHeader =  self.pageSize <= self.messages.count
        })
    }
    
    func CountOfMessages()
    {
        let messageQuery = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId)
            .child(Constant.messages)
        
        messageQuery.observe(.value) { (snapshot) in
            self.count =  Int(snapshot.childrenCount)
        }
    }
    
    
    func fetchMessages(){
        self.fetchingData = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId)
            .child(Constant.messages)
        self.FirstFetch = self.fetchingData.queryLimited(toLast: 20).observe(.childAdded, with: { (snapshot) in
            let msg = Message(snapshot: snapshot)
            let cDate = Date(seconds: msg.Timestamp)
            
            print("All Data msgID \(msg.SenderUid) || myID \(self.myUserId) || reciverID \(self.reciverInfo.UserId)")
            
            if msg.SenderUid != "\(self.myUserId)" {
                if self.reciverInfo.UserId != msg.SenderUid {
                    
                    self.reciverInfo.UserId = msg.SenderUid
                    
                }
            }
            
            
            self.allMessagesId.append(snapshot.key)
            if msg.SenderUid != "\(self.myUserId)" &&  msg.status == 0 {
                if self.fisrt_unseen_messgaesId == "" {
                    self.fisrt_unseen_messgaesId = snapshot.key
                }
                
                Constant.DBRefrence.child(Constant.chatRoomsNode)
                    .child(self.chatRoomId)
                    .child(Constant.messages)
                    .child(snapshot.key).updateChildValues(["status":1])
            }
            
            print("msg.mediaType \(msg.mediaType)")
            switch msg.mediaType {
            case "text":
                //replcae name not used with message id
                let msg = JSQMessage(senderId: msg.SenderUid, senderDisplayName: snapshot.key, date: cDate, text: msg.Message)
                self.messages.append(msg!)
                break
            case "image":
                let photo = JSQPhotoMediaItem(image: "chat_placeholder".toImage)
                let mss = JSQMessage(senderId: msg.SenderUid, senderDisplayName: msg.key, date: cDate, media: photo, url: msg.mediaUrl)
                self.messages.append(mss!)
                break
            case "file":
                //replcae name not used with message id
                let msg = JSQMessage(senderId: msg.SenderUid, senderDisplayName: snapshot.key, date: cDate, text: msg.Message, urlFile: msg.mediaUrl)
                self.messages.append(msg!)
                break
            default: break
            }
            self.finishReceivingMessage()
            self.collectionView.reloadData()
            self.showLoadEarlierMessagesHeader =  self.pageSize <= self.messages.count
        })
    }
    
    func typingObserver()
    {
        
        let typeRef = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId)
            .child(Constant.TypeIndicator)
            .child(senderId)
        
        typeRef.setValue(["TypeStatus":false])
        
    }
    override func textViewDidChange(_ textView: UITextView)
    {
        super.textViewDidChange(textView)
        if textView.text.count > 0
        {
            Constant.DBRefrence.child(Constant.chatRoomsNode)
                .child(chatRoomId)
                .child(Constant.TypeIndicator)
                .child(senderId)
                .child(Constant.TypeStatus).setValue(true)
        }else
        {
            Constant.DBRefrence.child(Constant.chatRoomsNode)
                .child(chatRoomId)
                .child(Constant.TypeIndicator)
                .child(senderId)
                .child(Constant.TypeStatus)
                .setValue(false)
        }
    }
    
    
    fileprivate func observeTypingUser()
    {
        self.userIsTypingRef = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId)
            .child(Constant.TypeIndicator)
            .child(adminID)
        
        isTypingHandle = self.userIsTypingRef.observe(.childChanged, with: { (snapshot) in
            
            if snapshot.value as! Bool
            {
                self.showTypingIndicator = true
                self.scrollToBottom(animated: true)
            }else
            {
                self.showTypingIndicator = false
            }
        })
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return messages.count
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!)
    {
        
        let cell =  collectionView.cellForItem(at: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        if message.urlFile != nil {
            
            if message.urlFile.contains("pdf") || message.text.contains("pdf") {
                fileExten = "pdf"
            } else if message.urlFile.contains("docx") || message.text.contains("docx") {
                fileExten = "docx"
            } else if message.urlFile.contains("xlsx")  || message.text.contains("xlsx") {
                fileExten = "xlsx"
            }
            
            self.downloadFile(urlFile: message.urlFile, fileExten: fileExten ?? "pdf") { (fileURL, error) in
                        if error != nil {
                            self.hideIndicator()
                            return
                        }else{
                            if let url = URL(string: fileURL!) {
                                self.documentInteractionController = UIDocumentInteractionController(url: url)
                                self.documentInteractionController.delegate = self
//                                self.documentInteractionController.presentOptionsMenu(from: self.view.frame, animated: true)
                                self.documentInteractionController.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)
                            }
                        }
                    }
            
        }
        /*
         popTip.hide()
         popTip = PopTip()
         
         if message.senderId == senderId
         {
         
         popTip.show(text: "Delete".localized, direction: .down, maxWidth: 200, in: view, from: CGRect(x: cell.frame.origin.x+90, y: cell.frame.origin.y+60, width: cell.frame.size.width, height: cell.frame.size.height))
         
         }else{
         popTip.show(text: "Delete".localized, direction: .down, maxWidth: 200, in: view, from: CGRect(x: cell.frame.origin.x-90, y: cell.frame.origin.y+60, width: cell.frame.size.width, height: cell.frame.size.height))
         }
         
         popTip.tapHandler = { popTip in
         
         let messageId = self.allMessagesId[indexPath.row + 1]
         let messageQuery = self.databaseRef.child(Constant.chatRoomsNode)
         .child(self.chatRoomId)
         .child(Constant.messages)
         .child(messageId)
         
         messageQuery.removeValue()
         
         self.popTip.hide()
         self.messages.remove(at: indexPath.row)
         super.collectionView.reloadData()
         }
         
         //        messageBubbleContainerView
         ////////////
         
         if message.isMediaMessage
         {
         if let media = message.media as? JSQVideoMediaItem
         {
         let player = AVPlayer(url: media.fileURL)
         let avPlayerViewController = AVPlayerViewController()
         avPlayerViewController.player = player
         self.present(avPlayerViewController, animated: true, completion: nil)
         }
         }
         */
        if message.isMediaMessage {
            print("The message for media \(message)")
            self.view.endEditing(true)
            let mediaItem: JSQMessageMediaData? = message.media
            if (mediaItem is JSQPhotoMediaItem) {
                let photoItem = mediaItem as? JSQPhotoMediaItem
                let newImageView = UIImageView(image: photoItem?.image)
                newImageView.frame = UIScreen.main.bounds
                newImageView.backgroundColor = .black
                newImageView.contentMode = .scaleAspectFit
                newImageView.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
                newImageView.addGestureRecognizer(tap)
                self.view.addSubview(newImageView)
                self.tabBarController?.tabBar.isHidden = true // tabBarController exists
                self.navigationController?.isNavigationBarHidden = true // default  navigationController
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return -10.0
    }
//
//    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return -10.0
//    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
            return self
        }
    
    func downloadFile(urlFile : String,fileExten : String, completion: @escaping (String?, Error?) -> Void) {
            self.showIndicator()
            let destination: DownloadRequest.Destination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent("file.\(fileExten)")
                return (documentsURL, [.removePreviousFile])
            }
        let file = urlFile.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            AF.download(file, to: destination).responseData { response in
                self.hideIndicator()
                
                if let destinationUrl = response.fileURL {
                    completion("\(destinationUrl)", nil)
                }
            }
        }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview() // This will remove image from full screen
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!)
    {
        if !self.isConnectedToNetwork()
        {
            self.showAlertNoInternt()
            return
        }
        
        if text == "" {
            return
        }
        
        self.finishSendingMessage()
        
        
        let messageRefSender = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(chatRoomId)
            .child(Constant.messages)
            .childByAutoId()
        
        let status = self.isReciverActive ? 1 : 0
        let user = Auth_User.UserInfo
        let  message = Message(Message: text,
                               SenderUid: senderId,
                               Sendername: user.name,
                               SenderImage:  user.image,
                               ReceiverUid: self.reciverInfo.UserId,
                               Receivername: self.reciverInfo.FullName,
                               ReceiverImage: self.reciverInfo.UserImage,
                               Timestamp: Date().seconds ,
                               GroupId: chatRoomId,
                               mediaType: "text",
                               status : status,
                               mediaUrl: "")
        
        messageRefSender.setValue(message.toAnyObject()) { (error, ref) in
            if error == nil
            {
                
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                Constant.DBRefrence.child(Constant.chatRoomsNode)
                    .child(self.chatRoomId)
                    .child(Constant.TypeIndicator)
                    .child(senderId)
                    .child(Constant.TypeStatus)
                    .setValue(false)
                
                Constant.DBRefrence.child(Constant.RecentNode)
                    .child(self.chatRoomId)
                    .setValue(message.toAnyObject())
                
                ////
                if !self.isReciverActive
                {
                    let unSeenMessage = Constant.DBRefrence.child(Constant.Seen)
                        .child(self.chatRoomId)
                        .child(self.adminID)
                    
                    
                    unSeenMessage.child("counter").observeSingleEvent(of: .value, with: { (snapshot) in
                        var countNum : Int = 1
                        if snapshot.exists()
                        {
                            countNum = 1 + (snapshot.value as! Int)
                        }
                        unSeenMessage.setValue(["counter":countNum])
                    })
                    
                    self.sendChatNotification(message: text)
                    //                    MyApi.api.sendPushNotification(user_id: self.reciverInfo.UserId)
                }
            }
        }
    }
    
    private func sendChatNotification(message: String){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        var params : [String:Any] = [:]
        params["ticket_id"] = self.chatRoomId
        params["message"] = message
               MyApi.api.sendChatNotification(params: params) {message, status in
               if !status {self.showToast(message);return}
        //        self.postNotifcationCenter(.Chat, self.providerId)
               }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
        CameraHandler.shared.showActionSheet(self)
        CameraHandler.shared.imagePickedBlock = { img, name in
            self.saveMediaMessage(withImage: img, withVideo: nil)
        }
        CameraHandler.shared.filesPickedBlock = { url, name in
            self.saveFilesMessage(withFiles: url, name: name)
        }
    }
    
    private func saveFilesMessage(withFiles url: URL?, name: String?){
        
        if let urlPath = url {
            
            let filePath = "Files/\(name!)"
            let fileRef = Constant.StorageRef.child(filePath)
            let metadata = StorageMetadata()
            metadata.contentType = "file"
            
            //            let imageData = image.jpegData(compressionQuality: 0.8)
            self.showIndicator()
            
            fileRef.putFile(from: urlPath, metadata: metadata, completion: { (newMetaData, error) in
                
                guard error == nil else {
                    self.showToast("Error when upload file".localized)
                    self.hideIndicator()
                    return
                }
                
                fileRef.downloadURL(completion: { (url, error) in
                    
                    guard error == nil else {
                        self.showToast("Error when upload image".localized)
                        self.hideIndicator()
                        return
                    }
                    print("The url from fireBase \(url?.absoluteURL)")
                    self.func_saveImageToFirebase(url?.absoluteString ?? "", message: name!, mediaType: "file")
                })
                
            })
        }
    }
    
    private func saveMediaMessage(withImage image: UIImage?, withVideo: URL?){
        
        if let image = image {
            
            let imagePath = "messageWithMedia\(chatRoomId + UUID().uuidString)/photo.jpg"
            let imageRef = Constant.StorageRef.child(imagePath)
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            
            let imageData = image.jpegData(compressionQuality: 0.8)
            self.showIndicator()
            
            imageRef.putData(imageData!, metadata: metadata, completion: { (newMetaData, error) in
                
                guard error == nil else {
                    self.showToast("Error when upload image".localized)
                    self.hideIndicator()
                    return
                }
                
                imageRef.downloadURL(completion: { (url, error) in
                    
                    guard error == nil else {
                        self.showToast("Error when upload image".localized)
                        self.hideIndicator()
                        return
                    }
                    print(url?.absoluteURL ?? "")
                    self.func_saveImageToFirebase(url?.absoluteString ?? "", message: "image", mediaType: "image")
                })
                
            })
        }
    }
    
    private func func_saveImageToFirebase(_ urlStr:String,message: String ,mediaType:String) {
        let messageRefSender = Constant.DBRefrence.child(Constant.chatRoomsNode)
            .child(self.chatRoomId)
            .child(Constant.messages)
            .childByAutoId()
        let status = self.isReciverActive ? 1 : 0
        let user = Auth_User.UserInfo
        
        
        let  message = Message(Message: message,
                               SenderUid: self.senderId,
                               Sendername: user.name,
                               SenderImage:  user.image,
                               ReceiverUid: self.reciverInfo.UserId,
                               Receivername: self.reciverInfo.FullName,
                               ReceiverImage: self.reciverInfo.UserImage,
                               Timestamp: Date().seconds ,
                               GroupId: self.chatRoomId,
                               mediaType: mediaType,
                               status : status,
                               mediaUrl: urlStr)
        
        messageRefSender.setValue(message.toAnyObject()) { (error, ref) in
            self.hideIndicator()
            if error == nil
            {
                
                JSQSystemSoundPlayer.jsq_playMessageSentSound()
                Constant.DBRefrence.child(Constant.chatRoomsNode)
                    .child(self.chatRoomId)
                    .child(Constant.TypeIndicator)
                    .child(self.senderId)
                    .child(Constant.TypeStatus)
                    .setValue(false)
                
                Constant.DBRefrence.child(Constant.RecentNode)
                    .child(self.chatRoomId)
                    .setValue(message.toAnyObject())
                
                ////
                if !self.isReciverActive
                {
                    let unSeenMessage = Constant.DBRefrence.child(Constant.Seen)
                        .child(self.chatRoomId)
                        .child(self.reciverInfo.UserId)
                    
                    
                    unSeenMessage.child("counter").observeSingleEvent(of: .value, with: { (snapshot) in
                        var countNum : Int = 1
                        if snapshot.exists()
                        {
                            countNum = 1 + (snapshot.value as! Int)
                        }
                        unSeenMessage.setValue(["counter":countNum])
                    })
                    self.finishSendingMessage()
                }
            }
        }
    }
    
    private func hideUnReadText(){
        self.fisrt_unseen_messgaesId = ""
        collectionView.reloadData()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        return  message.senderId == senderId ? outgoingBubbleImageView : incomingBubbleImageView
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData!
    {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource!
    {
        let message = messages[indexPath.item]
        if message.senderId == senderId
        {
            return JSQMessagesAvatarImageFactory.avatarImage(with: senderAvatar, diameter: 18)
        }
        else
        {
            return JSQMessagesAvatarImageFactory.avatarImage(with: reciverAvatar, diameter: 18)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return kJSQMessagesCollectionViewCellLabelHeightDefault
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let message = messages[indexPath.item]
        print("All message is \(message)")
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.backgroundColor = .clear
        
        //                let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(downloadFile))
        //                   singleTapGestureRecognizer.numberOfTapsRequired = 1
        //                   singleTapGestureRecognizer.delegate = self
        //                   urlForFile = message.urlFile ?? ""
        //                   print(message.text)
        //                   cell.textView?.addGestureRecognizer(singleTapGestureRecognizer)
        
        
        switch message.isMediaMessage {
        case true:
            //cell.messageBubbleImageView.fetchImage(message.text, "chat_placeholder".toImage)
            if !message.text.isEmptyStr {
                if let url = URL(string: message.text) {
                    MyTools.getDataFromUrl(url: url) { (data, response, error)  in
                        guard let data = data, error == nil else { return }
                        guard let img = UIImage(data: data) else { return }
                        DispatchQueue.main.async {
                            let photo = JSQPhotoMediaItem(image: img)
                            let msg = JSQMessage(senderId: message.senderId,
                                                 senderDisplayName: message.senderDisplayName,
                                                 date: message.date,
                                                 media: photo,
                                                 url: "")
                            
                            self.messages[indexPath.item] = msg!
                            
                            self.collectionView.reloadData()
                        }
                        
                    }
                }
            }
        default:
            
            if message.senderId == senderId {
                cell.textView.textColor = UIColor.white
                cell.cellBottomLabel.textAlignment = .right
                
            }else{
                cell.textView.textColor = "363636".color
                cell.cellBottomLabel.textAlignment = .left
            }
        }
        
        
        if message.senderDisplayName == self.fisrt_unseen_messgaesId {
            cell.lbl_unseen.isHidden = false
            cell.lbl_unseen.text = "UnRead".localized
            cell.view_unseen.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hideUnReadText()
            }
            
        } else{
            cell.lbl_unseen.isHidden = true
            cell.view_unseen.isHidden = true
        }
        
        if self.isResolved == 1 {
            cell.lbl_unseen.isHidden = true
            cell.view_unseen.isHidden = true
        }
        
        if message.urlFile != nil {
            cell.textView.font = UIFont(name:"Muli-Bold", size: 14.0)
        }
        
        cell.messageBubbleTopLabel.text = "hhhhhhh"
        cell.messageBubbleTopLabel.textColor = UIColor.clear
        cell.cellBottomLabel.text = MyTools.getTimeOnly(message.date)
        
        return cell
    }
    
    func downloadAvatrImage(url: String){
        
        if let url2 = URL(string: url) {
            MyTools.getDataFromUrl(url: url2) { (data, response, error)  in
                guard let data = data, error == nil else { return }
                guard let img = UIImage(data: data) else { return }
                DispatchQueue.main.async() { () -> Void in
                    self.reciverAvatar = img
                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    func downloadAvatrImageSender(url: String) {
        
        let url2 = NSURL(string: url)! as URL
        MyTools.getDataFromUrl(url: url2)
        { (data, response, error)  in
            guard let data = data, error == nil else { return }
            guard let img = UIImage(data: data) else { return }
            DispatchQueue.main.async() { () -> Void in
                self.senderAvatar = img
                self.collectionView.reloadData()
            }
        }
    }
    
    func getMyAvatar(){
        self.downloadAvatrImageSender(url:Auth_User.UserInfo.image)
    }
    
    @objc func downloadFile(){
        print("Hiiii man \(urlForFile)")
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        
        self.pageSize = self.pageSize + 20
        self.LoadMore(Size: self.pageSize)
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        if self.fromPervVC {
            self.popVC(true)
        } else if self.fromRedAlertHome {
            
            let storyboard = UIStoryboard(name: "AppSB", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
            vc.selectedIndex = 2
            vc.setLineCenter(2)
            navigationController?.pushViewController(vc, animated: true)

        } else {
//            Route.goHome()
            let storyboard = UIStoryboard(name: "AppSB", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MainTabbarVC") as! MainTabbarVC
            vc.selectedIndex = 2
            vc.setLineCenter(2)
            navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}


extension JSQMessagesInputToolbar {
    override open func didMoveToWindow() {
        super.didMoveToWindow()
        guard let window = window else { return }
        if #available(iOS 11.0, *) {
            let anchor = window.safeAreaLayoutGuide.bottomAnchor
            bottomAnchor.constraint(lessThanOrEqualToSystemSpacingBelow: anchor, multiplier: 1.0).isActive = true
        }
    }
}
