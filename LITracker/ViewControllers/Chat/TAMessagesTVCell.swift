//
//  TAMessagesTVCell.swift


import UIKit

class TAMessagesTVCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!    
    @IBOutlet weak var notficationStatus: UIImageView!
    @IBOutlet weak var txt_seenCounter: UILabel!
    @IBOutlet weak var view_counter: UIView!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    
    
}
