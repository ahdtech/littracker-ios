//
//  Recent.swift



import UIKit
import Foundation
import Firebase

class Recent {
    
    var RecentId     : String
    var GroupId      : String
    var Message      : String
    var ReceiverImage: String
    var ReceiverName : String
    var ReceiverUid  : String
    var SenderImage  : String
    var SenderName   : String
    var SenderUid    : String
    var Timestamp    : Int
    var mediaType    : String
    var mediaUrl     : String
    
    init(RecentId:String, GroupId:String, Message:String, ReceiverImage: String, ReceiverName: String , ReceiverUid: String , SenderImage :String , SenderName :String ,SenderUid:String,Timestamp:Int , mediaType: String , mediaUrl: String) {
        self.RecentId = RecentId
        self.GroupId = GroupId
        self.Message = Message
        self.ReceiverImage = ReceiverImage
        self.ReceiverName = ReceiverName
        self.ReceiverUid = ReceiverUid
        self.SenderImage = SenderImage
        self.SenderName = SenderName
        self.SenderUid = SenderUid
        self.Timestamp  = Timestamp
        self.mediaType  = mediaType
        self.mediaUrl  = mediaUrl
    }
    
    init(snapshot: DataSnapshot){
        RecentId = snapshot.key
        let valueData = snapshot.value as! NSDictionary
        GroupId = valueData.value(forKey: "groupId") as! String
        Message = valueData.value(forKey: "message") as! String
        ReceiverImage = valueData.value(forKey: "receiverImage") as? String ?? ""
        ReceiverName = valueData.value(forKey: "receiverName") as? String ?? ""
        ReceiverUid = valueData.value(forKey: "receiverUid") as! String
        SenderImage = valueData.value(forKey: "senderImage") as? String ?? ""
        SenderName = valueData.value(forKey: "senderName") as? String ?? ""
        SenderUid = valueData.value(forKey: "senderUid") as! String
        Timestamp = valueData.value(forKey: "timestamp") as! Int
        mediaType = valueData.value(forKey: "type") as? String ?? ""
        mediaUrl = valueData.value(forKey: "mediaUrl") as? String ?? ""
    }
    
    func update(item: Recent){
        self.RecentId       = item.RecentId
        self.GroupId        = item.GroupId
        self.Message        = item.Message
        self.ReceiverImage  = item.ReceiverImage
        self.ReceiverName   = item.ReceiverName
        self.ReceiverUid    = item.ReceiverUid
        self.SenderImage    = item.SenderImage
        self.SenderName     = item.SenderName
        self.SenderUid      = item.SenderUid
        self.Timestamp      = item.Timestamp
        self.mediaType      = item.mediaType
        self.mediaUrl       = item.mediaUrl
    }
    
    //    func toAnyObject() -> Any
    //    {
    //        return
    //            [
    //                "debateId":self.DebateId,
    //                "userId" :self.UserId ,
    //                "postType" : self.PostType,
    //                "filePath":self.FilePath,
    //                "postStatus":self.PostStatus,
    //                "postCreatedAt":self.PostCreatedAt ,
    //                "userName":self.UserName ,
    //                "userImage" :self.UserImage,
    //                "thumbnail" :self.Thumbnail
    //        ]
    //        
    //    }
    
}

