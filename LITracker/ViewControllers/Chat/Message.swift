//
//  Message.swift


import Foundation
import FirebaseDatabase

struct Message
{
    
    var Message       : String
    var SenderUid     : String
    var Sendername    : String
    var SenderImage   : String
    var ReceiverUid   : String
    var Receivername  : String
    var ReceiverImage : String
    var Timestamp     : Int
    var GroupId       : String
    var key           : String = ""
    var mediaType     : String
    var mediaUrl      : String
    var status        : Int
    
    
    init(snapshot: DataSnapshot)
    {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        Message       = snapshotValue["message"] as? String ?? ""
        SenderUid     = snapshotValue["senderUid"] as! String
        Sendername    = snapshotValue["senderName"] as? String ?? ""
        SenderImage   = snapshotValue["senderImage"] as? String ?? ""
        ReceiverUid   = snapshotValue["receiverUid"] as! String
        Receivername  = snapshotValue["receiverName"] as? String ?? ""
        ReceiverImage = snapshotValue["receiverImage"] as? String ?? ""
        Timestamp     = snapshotValue["timestamp"] as! Int
        GroupId       = snapshotValue["groupId"] as? String ?? ""
        mediaType     = snapshotValue["type"] as? String ?? ""
        mediaUrl      = snapshotValue["mediaUrl"] as? String ?? ""
        status        = snapshotValue["status"] as? Int ?? 0
        
        self.key = snapshot.key
    }
    
    
    init(Message       : String ,
         SenderUid     : String,
         Sendername    : String,
         SenderImage   : String,
         ReceiverUid   : String,
         Receivername  : String,
         ReceiverImage : String,
         Timestamp     : Int,
         GroupId       : String,
         mediaType     : String,
         status        : Int,
         mediaUrl      : String)
    {
        self.Message       = Message
        self.SenderUid     = SenderUid
        self.Sendername    = Sendername
        self.SenderImage   = SenderImage
        self.ReceiverImage = ReceiverImage
        self.Receivername  = Receivername
        self.ReceiverUid   = ReceiverUid
        self.Timestamp     = Timestamp
        self.GroupId       = GroupId
        self.key           = GroupId
        self.mediaType     = mediaType
        self.mediaUrl      = mediaUrl
        self.status        = status
    }
    
    
    func toAnyObject() -> Any
    {
        return ["message"       :self.Message,
                "senderUid"     :self.SenderUid,
                "senderName"    :self.Sendername,
                "senderImage"   :self.SenderImage,
                "receiverUid"   :self.ReceiverUid,
                "receiverName"  :self.Receivername,
                "receiverImage" :self.ReceiverImage,
                "timestamp"     :self.Timestamp,
                "groupId"       :self.GroupId,
                "type"          :self.mediaType,
                "status"        :self.status,
                "mediaUrl"      :self.mediaUrl]
    }
}

struct ChatCounterSt{
    var counter : Int
    private var my_id = Auth_User.User_Id.toString
    init(snapshot: Any){
        let admin_chatId = MyTools.createChatGroupId(userId: my_id, userId2: Constant.Admin_Id.toString)
        let snap = (snapshot as! DataSnapshot)
        guard snap.key != admin_chatId else {counter = 0;return}
        let valueData = snap.childSnapshot(forPath: my_id)
        counter = valueData.childSnapshot(forPath:"counter").value as? Int ?? 0
    }
}
