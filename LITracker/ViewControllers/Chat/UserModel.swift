//
//  UserModel.swift
//
//

import UIKit

//struct UserModel {
//    var ID : Int
//    var FullName : String
//    var Email  : String
//    var Mobile : String
//    var InsertDate : Int64
//    var LastLogin:Int64
//    var LastUpdateData:Int64
//    var DeviceType:String
//    var DeviceToken : String
//    var UserType:Int
//    var UserActive : Bool
//    var UserCountry : UserCountryModel
//   // var UserOfficeTypeEn  : String
//    var UserOfficeTypeString : String
//    var UserOfficeType : Int
//    var CompanyNumber:String
//    var CompanyLic:String
//    var UserOrginalImage:String
//    var UserAvatar : String
//    var UserNickName:String
//    var IsDelete : Bool
//    var SocialMediaType:String
//    var ActiveCode:String
//    var FullAddress : String
//    var FcmToken:String
//    var Property:String
//    init(_ item: [String:Any]){
//        ID           = item["ID"] as? Int ?? 0
//        FullName     = item["CustomerName"] as? String ?? ""
//        Email        = item["Email"] as? String ?? ""
//        Mobile       = item["Mobile"] as? String ?? ""
//        InsertDate   = item["InsertDate"] as? Int64 ?? 0
//        LastLogin    = item["LastLogin"] as? Int64 ?? 0
//        LastUpdateData    = item["LastUpdateData"] as? Int64 ?? 0
//        DeviceType   = item["DeviceType"] as? String ?? ""
//        DeviceToken  = item["DeviceToken"] as? String ?? ""
//        UserType     = item["UserType"] as? Int ?? 0
//        UserActive   = item["UserActive"] as? Bool ?? false
//        UserOfficeType    = item["UserOfficeType"] as? Int ?? 0
//        CompanyNumber     = item["CompanyNumber"] as? String ?? ""
//        CompanyLic        = item["CompanyLic"] as? String ?? ""
//        UserOrginalImage  = item["UserOrginalImage"] as? String ?? ""
//        UserAvatar        = item["UserAvatar"] as? String ?? ""
//        UserNickName      = item["UserNickName"] as? String ?? ""
//        IsDelete          = item["IsDelete"] as? Bool ?? false
//        SocialMediaType   = item["SocialMediaType"] as? String ?? ""
//        ActiveCode        = item["ActiveCode"] as? String ?? ""
//        FullAddress       = item["FullAddress"] as? String ?? ""
//        FcmToken          = item["FcmToken"] as? String ?? ""
//        Property          = item["Property"] as? String ?? ""
//        let userCountry   = item["UserCountry"] as? [String:Any] ?? [:]
//        UserCountry       = UserCountryModel(userCountry)
//        if(Language.currentLanguage.contains("ar"))
//        {
//            UserOfficeTypeString  = item["UserOfficeTypeAR"] as? String ?? ""
//        }
//        else
//        {
//            UserOfficeTypeString  = item["UserOfficeTypeEn"] as? String ?? ""
//        }
//    }
//}
//
//struct UserCountryModel {
//    var CountryId : Int
//    var CityId : Int
//    var CountryCode : String
//    var CountryImage : String
//   // var CountryAR : String
//    var Country : String
//    var City : String
//    var Latitude:String
//    var Longitude:String
//    //var CityAR : String
//
//    init(_ item: [String:Any]){
//        CountryId     = item["CountryId"] as? Int ?? 0
//        CityId        = item["CityId"] as? Int ?? 0
//        CountryCode   = item["CountryCode"] as? String ?? ""
//        CountryImage  = item["CountryImage"] as? String ?? ""
//        Latitude      = item["Latitude"] as? String ?? ""
//        Longitude     = item["Longitude"] as? String ?? ""
//        if(Language.currentLanguage.contains("ar"))
//        {
//            City        = item["CityAR"] as? String ?? ""
//            Country     = item["CountryAR"] as? String ?? ""
//        }
//        else
//        {
//            City        = item["CityEn"] as? String ?? ""
//            Country     = item["CountryEn"] as? String ?? ""
//        }
//
//    }
//}
//
//struct AdminOffer {
//    var Id : Int
//    var Title : String
//    var Address : String
//    var Price : String
//    var desc : String
//    var Area : String
//    var IsDelete : Bool
//    var InsertedDate : Int64
//    var Phone :String
//    var Image:String
//    var Active:Bool
//
//    init(_ item: [String:Any]){
//        Id            = item["Id"] as? Int ?? 0
//        Title         = item["Title"] as? String ?? ""
//        Address       = item["Address"] as? String ?? ""
//        Price         = item["Price"] as? String ?? ""
//        desc          = item["desc"] as? String ?? ""
//        Area          = item["Area"] as? String ?? ""
//        IsDelete      = item["IsDelete"] as? Bool ?? false
//        InsertedDate  = item["InsertedDate"] as? Int64 ?? 0
//        Phone         = item["Phone"] as? String ?? ""
//        Image         = item["Image"] as? String ?? ""
//        Active        = item["Active"] as? Bool ?? false
//
//    }
//}
//
//struct CountryModel{
//    var CountryId : Int
//    var CountryCode : String
//    var iso : String
//    var CountryName : String
//    //var CountryArName : String
//    var iso3 : String
//    var numcode : String
//    var VisibleOnList : Bool
//    var Latitude:String
//    var Longitude:String
//    var IsDelete : Bool
//
//    init(_ item: [String:Any]){
//        CountryId     = item["CountryId"] as? Int ?? 0
//        CountryCode   = item["CountryCode"] as? String ?? ""
//        iso           = item["iso"] as? String ?? ""
//        iso3          = item["iso3"] as? String ?? ""
//        numcode       = item["numcode"] as? String ?? ""
//        Latitude      = item["Latitude"] as? String ?? ""
//        Longitude     = item["Longitude"] as? String ?? ""
//        VisibleOnList = item["VisibleOnList"] as? Bool ?? false
//        IsDelete      = item["IsDelete"] as? Bool ?? false
//        if(Language.currentLanguage.contains("ar"))
//        {
//            CountryName = item["CountryArName"] as? String ?? ""
//        }
//        else
//        {
//            CountryName = item["CountryEnName"] as? String ?? ""
//        }
//    }
//}
//
//struct CityModel{
//    var id : Int
//    var CityName : String
//    var CountryId :Int
//   // var CityArName: String
//
//    init(_ item: [String:Any]){
//        id                  = item["id"] as? Int ?? 0
//        CountryId           = item["CountryId"] as? Int ?? 0
//        if(Language.currentLanguage.contains("ar"))
//        {
//            CityName          = item["CityArName"] as? String ?? ""
//        }
//        else
//        {
//            CityName          = item["CityEnName"] as? String ?? ""
//        }
//
//    }
//
//}
//
//struct StaticModel{
//    var Id : Int
//    var StaticEnCode:String
//    var StaticCode:String
//    var PropertyDetId:Int
//    var isSelect = false
//
//    init(_ item: [String:Any]){
//        Id                = item["Id"] as? Int ?? 0
//        PropertyDetId     = item["PropertyDetId"] as? Int ?? 0
//
//        if(Language.currentLanguage.contains("ar"))
//        {
//            StaticCode        = item["StaticArCode"] as? String ?? ""
//        }
//        else
//        {
//            StaticCode        = item["StaticEnCode"] as? String ?? ""
//        }
//        StaticEnCode        = item["StaticEnCode"] as? String ?? ""
//
//    }
//}
//
//struct NotificationModel{
//    var Id : Int
//    var CustomerId : Int
//    var Title : String
//    var NotificationText : String
//    var Action : String
//    var ImageLink : String
//    var InsertDate : Int64
//    var IsDelete : Bool
//    var IsRead : Bool
//    var ActionId:Int
//
//    init(_ item: [String:Any]){
//        Id               = item["Id"] as? Int ?? 0
//        CustomerId       = item["CustomerId"] as? Int ?? 0
//        Title            = item["Title"] as? String ?? ""
//        NotificationText = item["NotificationText"] as? String ?? ""
//        Action           = item["Action"] as? String ?? ""
//        ImageLink        = item["ImageLink"] as? String ?? ""
//        InsertDate       = item["InsertDate"] as? Int64 ?? 0
//        IsRead           = item["IsRead"] as? Bool ?? false
//        IsDelete         = item["IsDelete"] as? Bool ?? false
//        ActionId         = item["ActionId"] as? Int ?? 0
//
//    }
//}
//struct InquireModel{
//    var Id : Int
//    var CustomerId : Int
//    var AddressLatitude:Double
//    var AddressLongitude:Double
//    var QueryTitle : String
//    var CustomerName : String
//    var CustomerImage : String
//    var QueryDetails : String
//    var InsertDate : Int64
//    var QueryDoneDate : Int64
//    var QueryDone : Bool
//    var IsDelete : Bool
//    var price:String
//    var _type:String
//    var _typeId:String
//    var area:String
//    var address:String
//    var Mobile:String
//    var Code:String
//
//    init(_ item: [String:Any]){
//        Id               = item["Id"] as? Int ?? 0
//        CustomerId       = item["CustomerId"] as? Int ?? 0
//        AddressLatitude     = item["AddressLatitude"] as? Double ?? 0.0
//        AddressLongitude    = item["AddressLongitude"] as? Double ?? 0.0
//        QueryTitle          = item["QueryTitle"] as? String ?? ""
//        CustomerName        = item["CustomerName"] as? String ?? ""
//        CustomerImage       = item["CustomerImage"] as? String ?? ""
//        QueryDetails       = item["QueryDetails"] as? String ?? ""
//        price              = item["RoundPrice"] as? String ?? ""
//        _typeId              = item["PropertyType"] as? String ?? ""
//        area               = item["Area"] as? String ?? ""
//        address             = item["Address"] as? String ?? ""
//        InsertDate       = item["InsertDate"] as? Int64 ?? 0
//        QueryDoneDate     = item["QueryDoneDate"] as? Int64 ?? 0
//        IsDelete         = item["IsDelete"] as? Bool ?? false
//        QueryDone         = item["QueryDone"] as? Bool ?? false
//        Mobile            = item["Mobile"] as? String ?? ""
//        Code             = item["Code"] as? String ?? ""
//
//        if(Language.currentLanguage.contains("ar"))
//        {
//            _type        = item["propertyTypeAR"] as? String ?? ""
//        }
//        else
//        {
//            _type        = item["propertyTypeEn"] as? String ?? ""
//        }
//
//    }
//}
