//
//  HomeVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/22/21.
//

import UIKit

class HomeVC: UIViewController {
    
//    @IBOutlet weak var btn_notification: BadgeBarButtonItem!
    private var _DataHome : HomeSt!
    var payments_Array : [PaymentScheduleST] = []
    var pre_auth_schedule_Array : [PreAuthScheduleST] = []

    @IBOutlet weak var imgIcon          : UIImageView!
    @IBOutlet weak var lblText          : UILabel!
    @IBOutlet weak var lblDateStatus    : UILabel!
    @IBOutlet weak var lblAmountStatus  : UILabel!
    @IBOutlet weak var lblDateNext      : UILabel!
    @IBOutlet weak var lblAmountNext    : UILabel!
    @IBOutlet weak var lblExpectedDate  : UILabel!
    @IBOutlet weak var btnFAQs          : UIButton!
    
    private let _user = Auth_User.UserInfo
    
    override func viewWillAppear(_ animated: Bool) {
//        if Auth_User.isFirstLogin {
//            btnFAQs.isHidden = false
////            Auth_User.isFirstLogin.toggle()
//            let vc : ChangePasswordVC = AppDelegate.MainSB.instanceVC()
//            self.pushNavVC(vc)
//        } else {
//            btnFAQs.isHidden = true
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Auth_User.isFirstLogin {
//            btnFAQs.isHidden = false
            let changePasswordVC : ChangePasswordVC = AppDelegate.MainSB.instanceVC()
            changePasswordVC.hideBackWord()
            let faqFirstVC : FaqFirstVC = AppDelegate.AppSB.instanceVC()
            faqFirstVC.hideBackWord()
            self.pushManyVCs([faqFirstVC,changePasswordVC])
        }
//        else {
////            btnFAQs.isHidden = true
//        }
        
        openNotifi()
        getData()
        configure()
//        getBadgeCountRequest()
    }

    
    override func viewDidDisappear(_ animated: Bool) {
//        if Auth_User.isFirstLogin {
//            btnFAQs.isHidden = false
//        }
//        else {
//            btnFAQs.isHidden = true
//        }
//        Auth_User.isFirstLogin.toggle()
    }
    
    func configure(){
        navigationItem.title = "Hello, \(Auth_User.UserInfo.name)"
        hideBackWord()
    }
    
    private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getHome { (items,paymentSchedule,preAuthSchedule, message, status) in
            self.hideIndicator()
            self._DataHome = items
            self.payments_Array = paymentSchedule
            self.pre_auth_schedule_Array = preAuthSchedule
            print("The count is ")
            self.setData()
        }
    }
    
    private func setData(){
        
        if _DataHome.alert?.id != 0 {
            let contactUsVC : ContactUsAsapVC = AppDelegate.AppSB.instanceVC()
            print("The manger id is \(_DataHome.alert!.managerId) \(_DataHome.alert!.id)")
            Auth_User.Sender_Id = _DataHome.alert!.managerId
            contactUsVC.message = _DataHome.alert!.ticket_data.message
            contactUsVC.Action_Id = _DataHome.alert!.id
            contactUsVC.fromRedAlertHome = true
            did_home_load = false
            self.pushNavVC(contactUsVC)
        } else {
            
            let flag                    = _DataHome.payment_status.flag
            switch flag {
            case .complete:
                self.imgIcon.image = "ic_green".toImage
            case .not_complete:
                self.imgIcon.image = "ic_red".toImage
            case .None:
                break
            }
            self.lblText.text          = _DataHome.payment_status.text
            
            let amount = Double(_DataHome.payment_status.amount)
            self.lblAmountStatus.text  = "$\(amount!.styleFormat2)"
            
            if !_DataHome.next_payment.date.isEmpty {
                let nextDate               = _DataHome.next_payment.date.toDate("yyyy-MM-dd")
                self.lblDateNext.text      = nextDate.toString("MMM d")
            }
            
            let amountNextPayment = Double(_DataHome.next_payment.amount)
            self.lblAmountNext.text    = "$\(amountNextPayment.styleFormat2)"
            
            if !_DataHome.expected_date_completion.date.isEmpty {
                let expectedDate           = _DataHome.expected_date_completion.date.toDate("yyyy-MM-dd")
                self.lblExpectedDate.text  = expectedDate.toString("MMMM d, yyyy")
            }
        }
    }
    
    
    
    @IBAction func btnPayment(_ sender: UIButton) {
        let vc : PaymentVC = AppDelegate.AppSB.instanceVC()
        vc.payments_Array = payments_Array
        if !payments_Array.isEmpty{
            presentModal(vc)
        }else{
            showToast(EMPTY_PAYMENT_SCHEDULE)
        }
    }
    
    @IBAction func btnPreAuthSchedule(_ sender: UIButton) {
        let vc : PreAuthScheduleVC = AppDelegate.AppSB.instanceVC()
        vc.preAuthSchedule_Array = self.pre_auth_schedule_Array
        if !pre_auth_schedule_Array.isEmpty{
            presentModal(vc)
        }else{
            showToast(EMPTY_PREAUTH_SCHEDULE)
        }
    }
    
    
    //MARK:notifications
    @IBAction func notificationsAction(_ sender:UIBarButtonItem){
        guard Auth_User.IsUserLogin else {showLoginAlert();return}
        //
        let vc : NotificationHomeVC = AppDelegate.AppSB.instanceVC()
        pushNavVC(vc)
    }
    
    @IBAction func FAQsAction(_ sender: Any) {
        let vc : FAQViewController = AppDelegate.AppSB.instanceVC()
        pushNavVC(vc)
    }
    
    
    private func getBadgeCountRequest(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        MyApi.api.unseenNotifications { (items, message, status) in
            let count = items.count_unseen_notification
//            self.btn_notification.badgeNumber = count
            
        }
    }
    
}

extension HomeVC {
    @objc func openNotifi() {
        if temp_notifi != nil {
            Route.openPressedScreen(self)
        }
        did_home_load = true
    }
}
