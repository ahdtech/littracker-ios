//
//  MessagesVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/22/21.
//

import UIKit
import Firebase

class MessagesVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var prev_index = -1
    private var messagesArray : [DataST] = []
    
    private var currentPage = 1
    private var pageSize: Int = 0
    private var total_pages: Int = 1
    private var isLoading = true
    private var isResolved: Int = 0
    let refresh = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresh.initRefresh()
        refresh.addTarget(self, action: #selector(sendLogsRequest), for: .valueChanged)
        tableView.refreshControl = refresh
        
        configure()
        sendLogsRequest()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.hidesBottomBarWhenPushed = false
    }
    
    private func configure(){
        navigationItem.title = MESSAGES
        hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = BACKGROUND_EFF2F9
        tableView.registerCell(id: "MessagesCell")
        addNotifcaionObserver(.UpdateMessages, #selector(sendLogsRequest))
    }
    
    @objc private func sendLogsRequest(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        self.pageSize = 10
        
        let params : [String:Any] = ["page_size": self.pageSize,
                                     "page_number": self.currentPage]
        
        MyApi.api.postLog(params){ (items, message, status, total) in
            self.hideIndicator()
            if !status {self.showMessageAlert(message);return}
            self.total_pages = total
            if self.currentPage == 1 {
                self.messagesArray = items
            }else{
                self.messagesArray.append(contentsOf:items)
            }
            self.isLoading = false
            self.refresh.endRefreshing()
            self.tableView.reloadData()
            
            //reset app ion badge
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let scroll_height = (scrollView.contentOffset.y + scrollView.frame.size.height)
    if (scroll_height >= (scrollView.contentSize.height - 70)){
        if !self.isLoading {
            if self.currentPage < total_pages {
                self.isLoading = true
                self.currentPage += 1
                sendLogsRequest()
            }
        }
    }
}
    
}

extension MessagesVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MessagesCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        
        let item = messagesArray[indexPath.row]
//        self.func_startChat("100","1")
        cell.idMember       = item.assigned_manager.id
        cell.roomId         = item.id.toString
        cell.isAssigned     = item.is_assigned
        cell.is_resolved    = item.is_resolved
        cell.didPressChat   = func_startChat
//        self.isResolved     = item.is_resolved
        
        
        Constant.DBRefrence.child(Constant.Seen).child(item.id.toString).child(Auth_User.UserInfo.id.toString).child("counter").observe(.value, with: { (snapshot) in
            
            if snapshot.exists(){
                let counterValue = snapshot.value
                if counterValue as? Int == 0 {
                    cell.lbl_Title.text    = item.ticket.title
                } else {
                    let titleText =  NSMutableAttributedString(string: item.ticket.title)
                    
                    titleText.append(NSAttributedString(string: "  New", attributes: [NSAttributedString.Key.font: UIFont(name: "Muli-SemiBold", size: 12), NSAttributedString.Key.foregroundColor: PRIMARY_COLOR]))
                    
                    cell.lbl_Title.attributedText = titleText
                    print("the id is \(item.is_assigned)")
                    if item.is_assigned == 0{
                        print("the id is \(item.id.toString)")
                        Constant.RecentNodes.child(item.id.toString).child("senderUid").observe(.value, with: { snapshot in
                            if snapshot.exists(){
                                let senderUid = snapshot.value
                                item.assigned_manager.id = senderUid as! String
                                item.is_assigned = 1
                                
                            }                            
                        })
                    }
                }
            } else {
                cell.lbl_Title.text    = item.ticket.title
//                cell.lbl_New.isHidden = true
            }
            
        }, withCancel: nil)
        
//        cell.lbl_Title.text    = item.ticket.title
        let created_at         = item.created_at.toDate("yyyy-MM-dd hh-mm-ss")
        cell.lbl_Time.text     = created_at.toString("hh:mm a")
        cell.lbl_Date.text     = created_at.toString("yyyy-MM-dd")
        
        let type = item.ticket.type
        switch type {
        
        case .changed_phone:
            cell.lbl_PhoneNumber.text   = item.ticket_data.phone
            cell.lbl_Email.text         = item.ticket_data.email
            cell.lbl_Address.text       = item.ticket_data.address
            cell.lbl_EffectiveDate.text = item.ticket_data.effective_date
            
        case .creditor_calling:
            cell.lbl_Name.text          = item.ticket_data.name
            cell.lbl_AccountNumber.text = item.ticket_data.account_number
            
        case .counselling_session:
            cell.lbl_Method.text          = item.ticket_data.method
            cell.lbl_Day.text             = item.ticket_data.day
            cell.lbl_TimeCounselling.text = item.ticket_data.time
            
        case .stop_payment:
            cell.lbl_ProcessedDate.text = item.ticket_data.processed_date
            
        case .request_document:
//            let arr = item.ticket_data.types[0].map { (text) -> String in
//                return String(text)
//            }.joined(separator: "\n")
//
//            cell.lbl_RequestedDocument.text = arr
            cell.lbl_RequestedDocument.text = item.ticket_data.types.joined(separator: "\n")
            
        case .extra_payment:
            cell.lbl_TakenDate.text = item.ticket_data.taken_date
            cell.lbl_Amount.text    = item.ticket_data.amount
            
            
        case .talk_someone:
            cell.lbl_Summary.text = item.ticket_data.summery
            
        case .alert:
            cell.viewMessage.isHidden = item.ticket_data.message.isEmpty
            cell.viewFileName.isHidden = item.ticket_data.file_name.isEmpty
            cell.lbl_Message.text = item.ticket_data.message
            cell.lbl_FileName.text = item.ticket_data.file_name
            
        case .None:
            break
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = messagesArray[indexPath.row]
        
        if prev_index == indexPath.row {
            if let cell = get_cell(index: prev_index) {
                cell.reset_view = true
                prev_index = -1
                tableView.reloadData()
            }
            return
        }
        
        if prev_index != -1 {
            if let cell = get_cell(index: prev_index) {
                cell.reset_view = true
                tableView.reloadData()
            }
        }
        
        if let cell = get_cell(index: indexPath.row) {
            prev_index  = indexPath.row
            let item    = messagesArray[indexPath.row]
            let type    = item.ticket.type
            switch type {
            
            case .changed_phone:
                cell.mainStackInfo.isHidden     = false
                cell.viewChangedPhone.isHidden  = false
                tableView.reloadData()
                
            case .creditor_calling:
                cell.mainStackInfo.isHidden        = false
                cell.viewCreditorCalling.isHidden  = false
                tableView.reloadData()
                
            case .counselling_session:
                cell.mainStackInfo.isHidden          = false
                cell.viewCounsellingSession.isHidden = false
                tableView.reloadData()
                
            case .stop_payment:
                cell.mainStackInfo.isHidden    = false
                cell.viewStopPayment.isHidden  = false
                tableView.reloadData()
                
            case .request_document:
                cell.mainStackInfo.isHidden        = false
                cell.viewRequestDocument.isHidden  = false
                tableView.reloadData()
                
            case .extra_payment:
                cell.mainStackInfo.isHidden     = false
                cell.viewExtraPayment.isHidden  = false
                tableView.reloadData()
                
            case .talk_someone:
                cell.mainStackInfo.isHidden     = false
                cell.viewTalkSomeone.isHidden   = false
                tableView.reloadData()
                
            case .alert:
                cell.mainStackInfo.isHidden     = false
                cell.viewAlert.isHidden   = false
                tableView.reloadData()
            
            case .None:
                break
            }
            
        }
        
    }
    
    func func_startChat(_ idMember:String,_ roomId:String, _ isResolved:Int){
        Constant.UserNodes.child(idMember).observeSingleEvent(of: .value) { (snapshot) in
            self.hideIndicator()
            if snapshot.exists() {
                let user  = User(snapshot: snapshot)
                let chatVC : ChatViewController = AppDelegate.AppSB.instanceVC()
                chatVC.senderId             = "\(Auth_User.User_Id)" // change 1 to self.userId
                chatVC.senderDisplayName    = "\(Auth_User.UserInfo.name)"
                chatVC.chatRoomId           = roomId
                chatVC.reciverInfo          = user
                chatVC.isResolved           = isResolved
                chatVC.fromPervVC           = true
                self.pushNavVC(chatVC)
            }else {
                self.showOkAlert(title: "", message: Constant.UserNoAvailable)
            }
        }
    }
    
    private func get_cell(index: Int) -> MessagesCell? {
        if let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? MessagesCell {
            return cell
        }
        return nil
    }
}
