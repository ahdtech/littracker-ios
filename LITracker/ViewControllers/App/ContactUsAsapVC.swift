//
//  ContactUsAsapVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit

class ContactUsAsapVC: UIViewController {
    
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    var message : String?
    var Action_Id : Int?
    var fromRedAlert     =  false
    var fromRedAlertHome =  false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        temp_notifi = nil
        // Do any additional setup after loading the view.
        lbl_name.text = "Hello, \(Auth_User.UserInfo.name)"
        if !message!.isEmpty {
            lbl_message.text = message!
        }else {
            lbl_message.text = "Contact us ASAP"
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarHidden()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        temp_notifi = nil
//        self.popToRoot()
    }
    
    @IBAction func contactAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if Auth_User.Sender_Id.isEmpty {
            showToast("Something Error")
        } else {
            func_startChat()
        }
    }
    
    
    func func_startChat(){
        let top = Route.top_VC()

        Constant.UserNodes.child(Auth_User.Sender_Id).observe(.value) { (snapshot) in
            self.hideIndicator()
            if snapshot.exists() {
                let user  = User(snapshot: snapshot)
                let chatVC : ChatViewController = AppDelegate.AppSB.instanceVC()
                chatVC.senderId             = "\(Auth_User.User_Id)" // change 1 to self.userId
                chatVC.senderDisplayName    = "\(Auth_User.UserInfo.name)"
                chatVC.chatRoomId           =  self.Action_Id?.toString
                chatVC.reciverInfo          = user
                chatVC.fromPervVC           = false
                if self.fromRedAlert {
                   chatVC.fromRedAlert      = true
                }
                if self.fromRedAlertHome {
                   chatVC.fromRedAlertHome  = true
                }
                top.pushNavVC(chatVC)
            }else {
                self.showOkAlert(title: "", message: Constant.UserNoAvailable)
            }
        }
    }
    
}
