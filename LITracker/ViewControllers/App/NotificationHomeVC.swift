//
//  NotificationHomeVC.swift
//  LITtracker
//
//  Created by Mohammed on 5/3/21.
//

import UIKit

class NotificationHomeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_Main: UIView!
    //    @IBOutlet weak var layout_ContainerHeight: NSLayoutConstraint!
    
    private let cell_Height : CGFloat = 100
    private var NotfiArray : [LinksSt] = []
    private var currentPage = 1
    private var pageSize: Int = 0
    private var total_pages: Int = 1
    private var isLoading = true
    let refresh = UIRefreshControl()


    
    override func viewDidLoad() {
        super.viewDidLoad()

        refresh.initRefresh()
        refresh.addTarget(self, action: #selector(sendNotificationRequest), for: .valueChanged)
        tableView.refreshControl = refresh
//        self.calcHeight()
        configure()
        sendNotificationRequest()
        
    }
    
    
    func configure(){
        navigationItem.title = NOTIFICATIONS_HOME
        hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = BACKGROUND_EFF2F9
        tableView.registerCell(id: "LinksCell")
    }
    
    func calcHeight() {
        tableView.reloadData()
        view_Main.isHidden = NotfiArray.isEmpty
        //        layout_ContainerHeight.constant = (cell_Height * CGFloat(notifi_Array.count)) + 20
    }
    
    
    @objc private func sendNotificationRequest(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        self.pageSize = 10
        let params : [String:Any] = ["page_size": self.pageSize,
                                     "page_number": self.currentPage]
        MyApi.api.sendNotifications(params){ (items, message, status, total) in
            self.hideIndicator()
            if !status {self.showMessageAlert(message);return}
            self.total_pages = total
            if self.currentPage == 1 {
                self.NotfiArray = items
            }else{
                self.NotfiArray.append(contentsOf:items)
            }
            self.isLoading = false
            self.refresh.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scroll_height = (scrollView.contentOffset.y + scrollView.frame.size.height)
        if (scroll_height >= (scrollView.contentSize.height - 70)){
            if !self.isLoading {
                if self.currentPage < total_pages {
                    self.isLoading = true
                    self.currentPage += 1
                    sendNotificationRequest()
                }
            }
        }
    }
    
    private func deleteNotificationRequest(index: Int) {
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        let notificationId = NotfiArray[index].id
        let params : [String:Any] = ["notification_id": notificationId]
        MyApi.api.deleteNotifications(params) { (message, status) in
            self.hideIndicator()
            self.showToast(message)
            guard status else { return }
            
            self.NotfiArray.remove(at: index)
            self.tableView.reloadData()
        }
    }
    
}


extension NotificationHomeVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NotfiArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LinksCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let item = NotfiArray[indexPath.row]
        
        cell.lbl_Title.text = item.title
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let item = NotfiArray[indexPath.row]
//        MyTools.openUrlBasic(urlStr: item.url)
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cell_Height
    }
    
    private func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            self.deleteNotificationRequest(index: indexPath.item)
        }
    }

}
