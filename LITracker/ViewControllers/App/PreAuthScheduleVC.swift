//
//  PreAuthScheduleVC.swift
//  LITracker
//
//  Created by Mohammed on 12/20/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit

class PreAuthScheduleVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var preAuthSchedule_Array : [PreAuthScheduleST] = []
    private let cell_Height : CGFloat = 20
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: HeaderPreAuthScheduleCell.self)
        tableView.registerCell(id: RowPreAuthScheduleCell.self)
        
//        let refresh = UIRefreshControl()
//        refresh.initRefresh()
//        refresh.addTarget(self, action: #selector(getData), for: .valueChanged)
//        tableView.refreshControl = refresh
    }

    
    @IBAction func closeAction(_ sender: Any) {
        dismissVC()
    }
    
}


extension PreAuthScheduleVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return preAuthSchedule_Array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell : HeaderPreAuthScheduleCell = tableView.dequeueTVCell()
            cell.selectionStyle = .none
            
            cell.dueDate.text       = "DUE DATE"
            cell.dueType.text       = "# Type"
            cell.status.text        = "Status"
            cell.deposited.text     = "Deposited $"
            cell.osAmount.text      = "O/S AMOUNT"
            
            return cell
        default:
            let cell : RowPreAuthScheduleCell = tableView.dequeueTVCell()
            let item = preAuthSchedule_Array[indexPath.row]
            cell.selectionStyle = .none
            
            let due_Date                = item.due_date.toDate("yyyy-MM-dd")
            cell.dueDate.text           = due_Date.toString("yyyy-MM-dd")
           
            let type                    = item.type
            cell.type.text              = type
            
            let status                  = item.status
            cell.status.text            = status
            
            let deposited               = Float(item.deposited)
            cell.deposited.text         = "$ \(item.deposited.styleFormat2)"
            
//            let o_s_amount                = Float(item.o_s_amount)
            
            if item.is_complete == 0 {
                cell.osAmount.text     = "$ \(item.o_s_amount.styleFormat2)"
                cell.imgCheck.image    = "redCheck".toImage
            }else {
//                let complete_Date          = item.complete_date.toDate("yyyy-MM-dd")
//                cell.completeDate.text     = complete_Date.toString("yyyy-MM-dd")
                cell.osAmount.text         = "$ \(item.o_s_amount.styleFormat2)"
                cell.imgCheck.image        = "ic_check".toImage
            }
            
            return cell
        }
                
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 40
        default:
            return cell_Height
        }
        
    }
    
}
