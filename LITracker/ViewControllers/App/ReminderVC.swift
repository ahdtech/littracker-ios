//
//  ReminderVC.swift
//  LITtracker
//
//  Created by Mohammed on 5/9/21.
//

import UIKit

class ReminderVC: UIViewController {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    var message : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        lbl_name.text = "Hello, \(Auth_User.UserInfo.name)"
        lbl_message.text = message!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarHidden()
    }
    
    
    @IBAction func contactAction(_ sender: UIButton) {
        temp_notifi = nil
        Route.goHome()
    }

}
