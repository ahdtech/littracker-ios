//
//  FAQViewController.swift
//  LITracker
//
//  Created by Mohammed on 11/24/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//
import UIKit

class FAQViewController: BasicVC {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var btnSearch:UIButton!
        
    @IBOutlet weak var viewError: UIView!
    private var searchController: UISearchController!
    private var searchActive : Bool = false
    private var filtered: [FAQST] = []
    private var data: [FAQST] = []
    private var currentPage = 1
    private var pageSize: Int = 0
    private var total_pages: Int = 1
    private var isLoading = true
    var fromHome : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        getData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if fromHome {
            Route.goHome()
        }
    }
    
    private func configure(){
        title = FAQ
        self.hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let searchText = txtSearch.text ?? ""
        if searchText.isEmptyStr {
            filtered = data
            tableView.reloadData()
            searchActive = false
            return
        }
        
        filtered = data.filter {
            return ($0.question.lowercased().contains(searchText)) ||
                ($0.answer.lowercased().contains(searchText))
        }
        searchActive = true
        tableView.reloadData()
    }
    
    private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        self.pageSize = 30
        let params : [String:Any] = ["page_size": self.pageSize,
                                     "page_number": self.currentPage]
        print("currentPage \(currentPage)")
        MyApi.api.getFAQ(params: params){ (items, message, status, total) in
            self.hideIndicator()
            if !status {self.viewError.isHidden = false;return}
            self.total_pages = total
            
            if self.currentPage == 1 {
                self.data = items
            }else{
                self.data.append(contentsOf: items)
            }
            
            self.filtered = self.data
            self.isLoading = false
            self.tableView.reloadData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scroll_height = (scrollView.contentOffset.y + scrollView.frame.size.height)
        if (scroll_height >= (scrollView.contentSize.height - 193)){
            if !self.isLoading {
                if self.currentPage < total_pages {
                    self.isLoading = true
                    self.currentPage += 1
                    self.getData()
                }
            }
        }
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview() // This will remove image from full screen
    }
    
}

extension FAQViewController : UITableViewDelegate , UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FAQTableViewCell = tableView.dequeueTVCell()
        cell.item = filtered[indexPath.row]
        
        cell.didPress_ShowDImageAction = { img in
            let newImageView = UIImageView(image: img)
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
            self.tabBarController?.tabBar.isHidden = true // tabBarController exists
            self.navigationController?.isNavigationBarHidden = true // default  navigationController
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        filtered[indexPath.row].isOpen.toggle()// = false
        tableView.reloadData()
    }
    
}

