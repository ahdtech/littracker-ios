//
//  FAQCollectionViewCell.swift
//  LITracker
//
//  Created by Mohammed on 1/3/22.
//  Copyright © 2022 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit

class FAQCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    var didPress_ShowDImageAction : ((UIImage)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    @IBAction func imageTab(_ sender: Any) {
        didPress_ShowDImageAction?(img.image!)
    }
    
}
