//
//  FAQTableViewCell.swift
//  LITracker
//
//  Created by Mohammed on 11/24/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//
import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQestion     : UILabel!
    @IBOutlet weak var txt_answer     : UITextView!
    @IBOutlet weak var imgArrow       : UIImageView!
//    @IBOutlet weak var imageAnswer    : UIImageView!
    //@IBOutlet weak var viewHeader: UIView!
    @IBOutlet private weak var collectionView    : UICollectionView!
    @IBOutlet weak var layout_cvHeight   : NSLayoutConstraint!
    
    var item : FAQST!{didSet{setup()}}
    var didPress_ShowDImageAction : ((UIImage)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //txt_answer.contentInset = .zero
        txt_answer.textContainerInset = .zero
        config()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setup() {
        lblQestion.text = item.question
        txt_answer.text = item.answer.htmlToString
//        if item.image.isEmpty {
//            imageAnswer.isHidden = !item.isOpen
//        }else {
//            imageAnswer.fetchImage(item.image)
//        }
        txt_answer.isHidden     = !item.isOpen
        collectionView.isHidden = !item.isOpen
        
    }
    
    private func config() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: FAQCollectionViewCell.self)
        collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
    }
    
    //MARK: check collectionView && TableView content size
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard keyPath == "contentSize" else {return}
        if let obj = object as? UICollectionView, obj == collectionView {
            layout_cvHeight.constant = obj.contentSize.height
        }
    }
    
}

extension FAQTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return item.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : FAQCollectionViewCell = collectionView.dequeueCVCell(indexPath: indexPath)
        let item = self.item.images[indexPath.item]
        cell.img.fetchImage(item)
        cell.didPress_ShowDImageAction = { img in
            self.didPress_ShowDImageAction?(img)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 285, height: 500)
    }
    
}
