//
//  FaqFirstVC.swift
//  LITracker
//
//  Created by Mohammed on 12/29/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit

class FaqFirstVC: UIViewController {
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_message: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_name.text = "Hello, \(Auth_User.UserInfo.name)"
//        lbl_message.text = message!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNavigationBarHidden()
    }
    override func viewWillDisappear(_ animated: Bool) {
        Auth_User.isFirstLogin = false
        self.setNavigationBarHidden(false)
    }
    
    @IBAction func btn_FAQsAction(_ sender: Any) {
        let vc : FAQViewController = AppDelegate.AppSB.instanceVC()
        vc.fromHome = true
        pushNavVC(vc)
    }
    
    @IBAction func btn_CloseAction(_ sender: Any) {
        Route.goHome()
    }
    
}
