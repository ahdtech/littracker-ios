//
//  LinksVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/22/21.
//

import UIKit

class LinksVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_Main: UIView!
    @IBOutlet weak var layout_ContainerHeight: NSLayoutConstraint!

    private let cell_Height : CGFloat = 85
    private var linksArray : [LinksSt] = []
    let refresh = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        refresh.initRefresh()
        refresh.addTarget(self, action: #selector(getData), for: .valueChanged)
        tableView.refreshControl = refresh
        self.calcHeight()
        configure()
        getData()
        
    }
    
    func configure(){
        navigationItem.title = HELPFUL_LINKS
        hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = BACKGROUND_EFF2F9
        tableView.registerCell(id: "LinksCell")
    }
    
    func calcHeight() {
        tableView.reloadData()
        layout_ContainerHeight.constant = (cell_Height * CGFloat(linksArray.count)) + 20
    }
    
    @objc private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getLookup { (tickets,links,faqsData, message, status) in
            self.hideIndicator()
            self.linksArray = links
            self.refresh.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
}


extension LinksVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return linksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : LinksCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let item = linksArray[indexPath.row]
        
        cell.lbl_Title.text = item.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = linksArray[indexPath.row]
        MyTools.openUrlBasic(urlStr: item.url)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cell_Height
    }
}
