//
//  MainTabbarVC.swift
//  LITracker
//
//  Created by Mohammed Alefrangy on 12/22/20.
//

import UIKit
import Firebase

class MainTabbarVC: UITabBarController {
    
    private var line : UIView!
    var fromChat : Bool = false
    var counter : Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        changeItemPositions()
        getAllUnReadMessages()
        addTopNotchView()
        setUpBarStyle()
        addShadow()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if UIDevice.current.has_notch{
            tabBar.frame.size.height = 100
            tabBar.frame.origin.y = view.frame.height - 100
        }else{
            tabBar.frame.size.height = 70
            tabBar.frame.origin.y = view.frame.height - 70
        }
    }
    
    private func changeItemPositions(){
        guard let items = tabBar.items else {return}
        items.forEach { (item) in
            item.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 2, right: 0)
        }
    }
    
    private func addTopNotchView(){
        line = UIView(frame: .init(x: 0, y: -2, width: 34, height: 3))
        line.setRounded(radius: 1.5)
        line.backgroundColor = PRIMARY_COLOR
        tabBar.addSubview(line)
        setLineCenter(0, false)
    }
    
    private func getAllUnReadMessages(){

        Constant.DBRefrence.child(Constant.Seen).observe(.value, with: { (snapshot) in
            if snapshot.exists(){
                // For loop to main tree in firebase
                for child in snapshot.children.allObjects as! [DataSnapshot] {

                    for data in child.children.allObjects as! [DataSnapshot] {
                        if data.key == Auth_User.UserInfo.id.toString {
                            let dict = data.value as? [String : AnyObject] ?? [:]
                            
                            if dict["counter"]! as! Int > 0 {
                                //for 3 tab
                                (self.tabBar.items![2] as! UITabBarItem).badgeValue = "●"
                                (self.tabBar.items![2] as! UITabBarItem).badgeColor = .clear
                                (self.tabBar.items![2] as! UITabBarItem).setBadgeTextAttributes([NSAttributedString.Key.foregroundColor: PRIMARY_COLOR], for: .normal)
                                self.counter += 1
                            } else {
                                (self.tabBar.items![2] as! UITabBarItem).badgeValue = nil
                            }
                        }
                    }
                }
            }
            }, withCancel: nil)
    }
    
}


extension MainTabbarVC: UITabBarControllerDelegate {
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        guard let index = tabBar.items?.firstIndex(of: item) else {return}
        setLineCenter(index)
    }
    
    func setLineCenter(_ tag:Int,_ animation:Bool=true){
        let item_width = tabBar.frame.width / 5
        let itemCenter = item_width/2
        let newX = (item_width * CGFloat(tag)) + itemCenter
        var center = self.line.center
        center.x = newX
        if !animation{
            self.line.center = center
            return
        }
        
        UIView.animate(withDuration: 0.2) {
            self.line.center = center
            self.view.layoutIfNeeded()
        }
    }
    
    
    private func addShadow(){
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowOpacity = 0.06
        tabBar.layer.shadowOffset = CGSize.zero
        tabBar.layer.shadowRadius = 5
        self.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBar.layer.borderWidth = 0
        self.tabBar.clipsToBounds = false
        self.tabBar.backgroundColor = UIColor.white
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
        
    }
    
    func setUpBarStyle(){
        //        would like to add corner radius
        
        let layer = CAShapeLayer()
        
        layer.path = UIBezierPath(roundedRect: CGRect(x: self.tabBar.bounds.minX, y: self.tabBar.bounds.minY, width: self.tabBar.bounds.width, height: self.tabBar.bounds.height), cornerRadius: 2).cgPath
        
        layer.fillColor = UIColor.white.cgColor
        
        self.tabBar.layer.insertSublayer(layer, at: 0)
        if let items = self.tabBar.items {
            items.forEach { item in item.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) }
        }
        
        self.tabBar.itemWidth = 30.0
        self.tabBar.itemPositioning = .fill
    }
}
