//
//  ProfileVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/22/21.
//

import UIKit

class ProfileVC: BasicVC {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var switch_status: UISwitch!
    
    private var _user : UserSt!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        
        getData()
    }
    func configure(){
        navigationItem.title = MY_PROFILE
        hideBackWord()
    }
        
    @IBAction func btn_changePasswordAction(_ sender: Any) {
        let vc : ChangePasswordVC = AppDelegate.MainSB.instanceVC()
        pushNavVC(vc)
    }
    
    @IBAction func btn_editAction(_ sender: Any) {
        let vc : EditProfileVC = AppDelegate.MainSB.instanceVC()
        pushNavVC(vc)
    }
    
    @IBAction func btn_myFormsAction(_ sender: Any) {
        let vc : FormsVC = AppDelegate.FormsSB.instanceVC()
        pushNavVC(vc)
    }
    
    @IBAction func SwitchStatus(_ sender: UISwitch) {
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.EnableTwoFactorAuth{ (message, status, status_code) in
            self.hideIndicator()
            Auth_User.Notifi_On = status
            if status {
                self.showToast(message)
            }
        }
    }
    
    func changeStatus() {
        if _user.has_two_factor_auth == 1 {
            switch_status.isOn = true
        } else {
            switch_status.isOn = false
        }
    }
    
    @IBAction func DeactivateMyAccountAction(_ sender: Any) {
        self.showCustomAlert(title: ATTENTION, message: DEACTIVATE_MESSAGE, okTitle: OK_TITLE, cancelTitle: CANCEL) { (action) in
            if action {
                guard self.isConnectedToNetwork() else {self.showToastNoInternt();return}
                self.showIndicator()
                MyApi.api.DeactivateMyAccount{ (message, status, status_code) in
                    if status {
                        self.showToast(message)
//                        Auth_User.removeUserValues()
//                        Auth_User.removeNotificationInfo()
//                        Route.goSignIn()
                    } else {
                        self.showToast(message)
                    }
                }
            }
        }
    }
    
    @IBAction func FAQsAction(_ sender: Any) {
        let vc : FAQViewController = AppDelegate.AppSB.instanceVC()
        pushNavVC(vc)
    }
    

    
    @IBAction func logOutAction(_ sender:UIButton){
        Auth_User.LogoutAction(self)
    }
    
    private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getProfile { (items, message, status) in
            self.hideIndicator()
            self._user = items
            self.setData()
            self.changeStatus()
        }
    }
    
    private func setData(){
        self.lblName.text = _user.name
//        self.lblLocation.text = _user.type
        self.lblEmail.text = _user.email
        self.lblPhone.text = _user.phone

        //        let phoneNo = "+" + _user.country.country_code + _user.mobile
        //        self.lblPhoneNo.text = phoneNo
    }
    
    
}
