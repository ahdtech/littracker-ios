//
//  CallingMeVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit

class CallingMeVC: UIViewController {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAccountNumber: UITextField!
    @IBOutlet weak var lineViewName: UIView!
    @IBOutlet weak var lineViewAccountNumber: UIView!
    
    var type :String?
    private var params : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtName.delegate = self
        txtAccountNumber.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        guard let name = txtName.text, !name.isEmptyStr else {
            showToast(ENTER_NAME)
            return
        }
        
//        guard let accountNumber = txtAccountNumber.text, !accountNumber.isEmptyStr else {
//            showToast(ENTER_ACCOUNTNUMBER)
//            return
//        }
        
        var data = [String : String] ()
        data["name"] = name
        data["account_number"] = txtAccountNumber.text

        let encoder = JSONEncoder()
        if let json = try? encoder.encode(data) {
            print(String(data: json, encoding: .utf8)!)
            params = ["ticket_type":type!,
                      "data": String(data: json, encoding: .utf8)!] as [String : Any]
        }
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
}

extension CallingMeVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtName {
            self.lineViewName.backgroundColor = PRIMARY_COLOR
        } else {
            self.lineViewAccountNumber.backgroundColor = PRIMARY_COLOR
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtName {
            self.lineViewName.backgroundColor = GRAY_616670
        } else {
            self.lineViewAccountNumber.backgroundColor = GRAY_616670
        }
    }
    
    // MARK: Limit number for Phone 
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //        if textField == txtPhone {
    //            let txt = textField.text?.replacingCharacter(newStr: string, range: range) ?? ""
    //            return txt.count <= 9
    //        }else {
    //            return true
    //        }
    //    }
    
}
