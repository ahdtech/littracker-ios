//
//  CounsellingSessionVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit
import SwiftyPickerPopover

class CounsellingSessionVC: UIViewController {
    
    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet weak var txtTime: UITextField!
    private var selected_Date : Date?
    private var selected_Time : Date?
    
    @IBOutlet weak var daysTableView: UITableView!
    @IBOutlet weak var timeTableView: UITableView!
    @IBOutlet weak var layout_tvHeight   : NSLayoutConstraint!

    
    var titleArray : [String] = ["Method", "What day of the week is the best for you?", "Best time of day?"]
    
    var methodArray  : [MethodSt] = []
    private var selectedMehtod  : MethodSt?
    var daysArray  : [DaysSt] = []
    private var selectedDay  : DaysSt?
    var timeArray  : [TimeSt] = []
    private var selectedTime  : TimeSt?
    
    var type :String?
    private var params : [String:Any]?
    
    override func viewWillAppear(_ animated: Bool) {
        self.daysTableView.setContentOffset( CGPoint(x: 0, y: 0) , animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
        setData()
        // Do any additional setup after loading the view.
    }
    
    func setData() {
        methodArray.append(MethodSt(id: 0, name: "In-Persone"))
        methodArray.append(MethodSt(id: 1, name: "Video Conference"))
        
        daysArray.append(DaysSt(id: 0, name: "Monday"))
        daysArray.append(DaysSt(id: 1, name: "Tuesday"))
        daysArray.append(DaysSt(id: 2, name: "Wednesday"))
        daysArray.append(DaysSt(id: 3, name: "Thursday"))
        daysArray.append(DaysSt(id: 4, name: "Friday"))
        daysArray.append(DaysSt(id: 5, name: "Any day"))
        
        timeArray.append(TimeSt(id: 0, name: "Morning"))
        timeArray.append(TimeSt(id: 1, name: "Afternoon"))
        timeArray.append(TimeSt(id: 2, name: "Any time"))
        
    }
    
    func configure(){
        daysTableView.delegate = self
        daysTableView.dataSource = self
        daysTableView.registerCell(id: "DaysTVC")
        self.daysTableView.isPagingEnabled = true
        
        daysTableView.addContentSizeObserver(self)
        
        timeTableView.delegate = self
        timeTableView.dataSource = self
        timeTableView.registerCell(id: "DaysTVC")
    }
    
    //MARK: check TableView content size
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard keyPath == "contentSize" else {return}
        if let obj = object as? UITableView, obj == daysTableView {
            layout_tvHeight.constant = obj.contentSize.height
            view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        guard let day = selectedDay?.id.toString, !day.isEmptyStr else {
            showToast(CHOOSE_DAY)
            return
        }
        
        guard let time = selectedTime?.id.toString, !time.isEmptyStr else {
            showToast(CHOOSE_TIME)
            return
        }
        
        var data = [String : String] ()
        data["method"]  = selectedMehtod!.name
        data["day"]     = selectedDay!.name
        data["time"]    = selectedTime!.name

        let encoder = JSONEncoder()
        if let json = try? encoder.encode(data) {
            print(String(data: json, encoding: .utf8)!)
            params = ["ticket_type":type!,
                      "data": String(data: json, encoding: .utf8)!] as [String : Any]
        }
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
    @IBAction func PickDateAction(_ sender : UIButton) {
        DatePickerPopover(title: "Date")
            .setMinimumDate(Date())
            .setDateMode(.date)
            .setPreferredDatePickerStyle(.inline)
            .setDoneButton(action: { _, selectedDate in
                self.selected_Date = selectedDate
                self.txtDate.text = selectedDate.toString("yyyy-MM-dd")
                
            })
            .appear(originView: sender, baseViewController: self)
    }
    
    @IBAction func PickTimeAction(_ sender : UIButton) {
        guard selected_Date != nil else { return }
        let min = selected_Date! > Date() ? Date().previousDay : Date().addMin(60)
        DatePickerPopover(title: "Time")
            .setMinimumDate(min)
            .setDateMode(.time)
            .setPreferredDatePickerStyle(.inline)
            .setDoneButton(action: { _, selectedDate in
                self.selected_Time = selectedDate
                self.txtTime.text = selectedDate.toString("hh:mm a")
            })
            .appear(originView: sender, baseViewController: self)
    }
    
}



extension CounsellingSessionVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 18
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 38
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view : TitleHeaderView = .fromNib()
        view.lbl_title.text = titleArray[section]
        return view
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 2:
            return UIView()
        default:
            let view : FooterView = .fromNib()
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return methodArray.count
        case 1:
            return daysArray.count
        case 2:
            return timeArray.count
        default:
            return timeArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : DaysTVC = tableView.dequeueTVCell()
        cell.selectionStyle = .none

        switch indexPath.section {
        case 0:
            let item = methodArray[indexPath.row]
            cell.lblText.text = item.name
            cell.selectImage.image = (selectedMehtod?.id == item.id) ? "ic_red".toImage : "ic_unCheck".toImage
            
            return cell
            
        case 1:
            let item = daysArray[indexPath.row]
            cell.lblText.text = item.name
            cell.selectImage.image = (selectedDay?.id == item.id) ? "ic_red".toImage : "ic_unCheck".toImage
            
            return cell
            
        case 2:
            let item = timeArray[indexPath.row]
            cell.lblText.text = item.name
            cell.selectImage.image = (selectedTime?.id == item.id) ? "ic_red".toImage : "ic_unCheck".toImage
            
            return cell
            
        default:
            let cell : DaysTVC = tableView.dequeueTVCell()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            selectedMehtod = methodArray[indexPath.item]
            daysTableView.reloadData()
        case 1:
            selectedDay = daysArray[indexPath.item]
            daysTableView.reloadData()
        case 2:
            selectedTime = timeArray[indexPath.item]
            daysTableView.reloadData()
        default:
            break
        }
    }
    
}
