//
//  RequestDocumentVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit

class RequestDocumentVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtDiscription: UITextField!
    @IBOutlet weak var lineViewDiscription: UIView!
    
    @IBOutlet weak var stackDescription: UIStackView!
    private var isStackShow = true
    private var requestDocument_Array : [RequestSt] = []
    private var selectDocument_Array : [RequestSt] = []
    
    var type :String?
    private var params : [String:Any]?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtDiscription.delegate = self
        
        requestDocument_Array.append(RequestSt(id: 0, title: "INITIAL FILING DOCUMENTS"))
        requestDocument_Array.append(RequestSt(id: 1, title: "DISCHARGE/COMPLETION DOCUMENT"))
        requestDocument_Array.append(RequestSt(id: 2, title: "OTHER (SPECIFY)"))
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: "RequestDocumentTVC")
        tableView.allowsMultipleSelection = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        

        
        guard !selectDocument_Array.isEmpty else {
            showToast(CHOOSE_DOCUMENT)
            return
        }
        
        for arr in selectDocument_Array {
            
            if selectDocument_Array.contains(where: { _ in 2 == arr.id }) {
                
                guard let discription = txtDiscription.text, !discription.isEmptyStr else {
                    showToast(FILL_ALL_DATA)
                    return
                }
            }
        }
        
        if let row = self.selectDocument_Array.firstIndex(where: {$0.id == 2}) {
            selectDocument_Array[row].title = selectDocument_Array[row].title + " " + txtDiscription.text!
        }
        
        
        let dic_array = selectDocument_Array.map{$0.title}
        let jsonData = MyTools.jsonObject(dic_array)
        
//        let data_array : [String] = [jsonData]
        
        params = ["ticket_type":type!,
                  "data": jsonData] as [String : Any]
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
}

extension RequestDocumentVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requestDocument_Array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RequestDocumentTVC = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let item = requestDocument_Array[indexPath.row]
        cell.lblTitle.text = item.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var cell = tableView.cellForRow(at: indexPath) as? RequestDocumentTVC
        cell?.bgView.backgroundColor = BLUE_2C3758
        cell?.lblTitle.textColor = .white
        
        selectDocument_Array.append(requestDocument_Array[indexPath.item])
        
        for arr in selectDocument_Array {
            print(arr.title)
        }
        
        if indexPath.row == 2 {
            stackDescription.isHidden = false
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        var cell = tableView.cellForRow(at: indexPath) as? RequestDocumentTVC
        cell?.bgView.backgroundColor = GRAY_EEEEEE
        cell?.lblTitle.textColor = BLUE_2C3758
        
        let index = selectDocument_Array.index(where: { $0.id == requestDocument_Array[indexPath.item].id })
    
        selectDocument_Array.remove(at: index!)
        
        for arr in selectDocument_Array {
            print(arr.title)
        }
        
        if indexPath.row == 2 {
            stackDescription.isHidden = true
        }
        
    }
    
    
    
}

extension RequestDocumentVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.lineViewDiscription.backgroundColor = PRIMARY_COLOR
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.lineViewDiscription.backgroundColor = GRAY_616670
    }
}
