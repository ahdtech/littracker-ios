//
//  MakePaymentVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit
import SwiftyPickerPopover

class MakePaymentVC: UIViewController {
    
    @IBOutlet weak var txtDate: UITextField!
    private var selected_Date : Date?
    
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var lineView: UIView!
    
    var type :String?
    private var params : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtAmount.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        guard let date = txtDate.text, !date.isEmptyStr else {
            showToast(ENTER_DATE)
            return
        }
        
        guard let amount = txtAmount.text, !amount.isEmptyStr else {
            showToast(ENTER_AMOUNT)
            return
        }
        
        var data = [String : String] ()
        data["taken_date"] = date
        data["amount"] = amount

        let encoder = JSONEncoder()
        if let json = try? encoder.encode(data) {
            print(String(data: json, encoding: .utf8)!)
            params = ["ticket_type":type!,
                      "data": String(data: json, encoding: .utf8)!] as [String : Any]
        }
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
    
    @IBAction func PickDateAction(_ sender : UIButton) {
        
        DatePickerPopover(title: "DatePicker")
            .setDateMode(.date)
            .setPreferredDatePickerStyle(.compact)
            .setSelectedDate(Date())
            .setDoneButton(action: { _, selectedDate in
                self.selected_Date = selectedDate
                self.txtDate.text = selectedDate.toString("yyyy-MM-dd")
                
            })
            .setCancelButton(action: { _, _ in print("cancel")})
            .appear(originView: sender, baseViewController: self)
    }
}

extension MakePaymentVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.lineView.backgroundColor = PRIMARY_COLOR
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.lineView.backgroundColor = GRAY_616670
    }
    
    
}
