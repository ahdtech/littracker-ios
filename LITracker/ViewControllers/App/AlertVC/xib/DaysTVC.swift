//
//  DaysTVC.swift
//  LITtracker
//
//  Created by Mohammed on 5/2/21.
//

import UIKit

class DaysTVC: UITableViewCell {

    @IBOutlet weak var selectImage: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
