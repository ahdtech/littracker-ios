//
//  MovedOrChangedVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit
import SwiftyPickerPopover


class MovedOrChangedVC: UIViewController {
    
    @IBOutlet weak var txtPhone     : UITextField!
    @IBOutlet weak var txtEmail     : UITextField!
    @IBOutlet weak var txtAdress    : UITextField!
    @IBOutlet weak var txtDate      : UITextField!
    @IBOutlet weak var lineViewPhone: UIView!
    @IBOutlet weak var lineViewEmail: UIView!
    
    private var selected_Date : Date?
    private var latitude      : Double?
    private var longitude     : Double?
    private var addressStr    : String?
    
    var type :String?
    
    private var data : [ChangedPhoneSt] = []
    private var temp_data : ChangedPhoneSt?
    private var params : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhone.delegate = self
        txtEmail.delegate = self
    }
    
    
    @IBAction func btnAdressAction(_ sender: Any) {
        let shared = AddressHandle.shared
        shared.showPlacePicker(self, latitude, longitude)
        shared.didPickShortAddress =  didPickAddress
    }
    
    private func didPickAddress(_ addres:String,_ lat:Double,_ lon:Double) {
        txtAdress.text = addres
        addressStr = addres
        latitude = lat
        longitude = lon
    }
    
    @IBAction func btnDateAction(_ sender: UIButton) {
        
        DatePickerPopover(title: "DatePicker")
            .setMinimumDate(Date())
            .setDateMode(.date)
            .setPreferredDatePickerStyle(.inline)
            .setDoneButton(action: { _, selectedDate in
                self.selected_Date = selectedDate
                self.txtDate.text = selectedDate.toString("yyyy-MM-dd")
                
            })
            .appear(originView: sender, baseViewController: self)
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        guard let phoneNumber = txtPhone.text, !phoneNumber.isEmptyStr else {
            showToast(ENTER_PHONE)
            return
        }
        
        guard let email = txtEmail.text, !email.isEmptyStr else {
            showToast(ENTER_EMAIL)
            return
        }
        guard let address = txtAdress.text, !address.isEmptyStr else {
            showToast(ENTER_ADDRESS)
            return
        }
        guard let date = txtDate.text, !date.isEmptyStr else {
            showToast(ENTER_DATE)
            return
        }
        
        guard email.trimmed.isEmailValid else {
            showToast(ENTER_VALID_EMAIL)
            return
        }
        
        let mobileQAValid = phoneNumber.isMobileQAValid
        guard mobileQAValid.status else {
            showToast(mobileQAValid.message)
            return
        }
        
        //        let temp_dataa = ChangedPhoneSt(phone: phoneNumber, email: email, address: address, date: date)
        
        var data = [String : String] ()
        data["phone"] = phoneNumber
        data["email"] = email
        data["address"] = address
        data["effective_date"] = date
        
        let encoder = JSONEncoder()
        if let json = try? encoder.encode(data) {
            print(String(data: json, encoding: .utf8)!)
            params = ["ticket_type":type!,
                      "data": String(data: json, encoding: .utf8)!] as [String : Any]
        }
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
}

extension MovedOrChangedVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtPhone {
            self.lineViewPhone.backgroundColor = PRIMARY_COLOR
        } else {
            self.lineViewEmail.backgroundColor = PRIMARY_COLOR
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtPhone {
            self.lineViewPhone.backgroundColor = GRAY_616670
        } else {
            self.lineViewEmail.backgroundColor = GRAY_616670
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtPhone {
            let txt = textField.text?.replacingCharacter(newStr: string, range: range) ?? ""
            return txt.count <= 10
        }else {
            return true
        }
    }
    
}
