//
//  StopPaymentVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit
import SwiftyPickerPopover


class StopPaymentVC: UIViewController {
    
    @IBOutlet weak var txt_date: UITextField!
    private var selected_Date : Date?
    
    var type :String?
    private var params : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
//        guard let date = txt_date.text, !date.isEmptyStr else {
//            showToast(ENTER_DATE)
//            return
//        }
        
        var data = [String : String] ()
        data["processed_date"] = txt_date.text

        let encoder = JSONEncoder()
        if let json = try? encoder.encode(data) {
            print(String(data: json, encoding: .utf8)!)
            params = ["ticket_type":type!,
                      "data": String(data: json, encoding: .utf8)!] as [String : Any]
        }
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
    @IBAction func PickDateAction(_ sender : UIButton) {
        DatePickerPopover(title: "DatePicker")
            .setPreferredDatePickerStyle(.compact)
            .setDateMode(.date)
            .setDoneButton(action: { _, selectedDate in
                self.selected_Date = selectedDate
                self.txt_date.text = selectedDate.toString("yyyy-MM-dd")
                
            })
            .appear(originView: sender, baseViewController: self)
    }
}
