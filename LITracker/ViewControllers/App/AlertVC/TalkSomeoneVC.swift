//
//  TalkSomeoneVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/21/21.
//

import UIKit

class TalkSomeoneVC: UIViewController {
    
    @IBOutlet weak var txtDiscription: UITextField!
    @IBOutlet weak var lineViewDiscription: UIView!
    
    var type :String?
    private var params : [String:Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtDiscription.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func closeAction(_ sender : UIButton) {
        dismissVC()
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        guard let discription = txtDiscription.text, !discription.isEmptyStr else {
            showToast(ENTER_DISCRIPTION)
            return
        }
        
        var data = [String : String] ()
        data["summery"] = discription

        let encoder = JSONEncoder()
        if let json = try? encoder.encode(data) {
            print(String(data: json, encoding: .utf8)!)
            params = ["ticket_type":type!,
                      "data": String(data: json, encoding: .utf8)!] as [String : Any]
        }
        
        
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.addTicket(params!) { (message, status) in
            self.hideIndicator()
            self.showOkAlertWithComp(message: message) { (_) in
                guard status else {return}
                self.showToast(message)
                self.dismissVC()
            }
        }
        
    }
    
}

extension TalkSomeoneVC :UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.lineViewDiscription.backgroundColor = PRIMARY_COLOR
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.lineViewDiscription.backgroundColor = GRAY_616670
    }
}
