//
//  NotificationVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/22/21.
//

import UIKit

class NotificationVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_Main: UIView!
    @IBOutlet weak var layout_ContainerHeight: NSLayoutConstraint!
    
    private let cell_Height : CGFloat = 85
    private var notifications : [TicketsST] = []
    let refresh = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        refresh.initRefresh()
        refresh.addTarget(self, action: #selector(getData), for: .valueChanged)
        tableView.refreshControl = refresh
        self.calcHeight()
        
        configure()
        getData()
        
    }
    
    func configure(){
        navigationItem.title = NOTIFY_MY_LIT
        hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = BACKGROUND_EFF2F9
        tableView.registerCell(id: "NotifiCell")
    }
    
    
    func calcHeight() {
        tableView.reloadData()
        layout_ContainerHeight.constant = (cell_Height * CGFloat(notifications.count)) + 20
    }
    
    @objc private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getLookup { (tickets,links,faqsData, message, status) in
            self.hideIndicator()
            self.notifications = tickets
            self.refresh.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
}

extension NotificationVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : NotifiCell = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let item = notifications[indexPath.row]
        cell.lbl_Title.text = item.title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = notifications[indexPath.row]
        
        switch item.type {
      
        case .changed_phone:
            let vc : MovedOrChangedVC = AppDelegate.AlertSB.instanceVC()
            vc.type = item.type.value
            presentModal(vc, true)
        case .creditor_calling:
            let vc : CallingMeVC = AppDelegate.AlertSB.instanceVC()
            vc.type = item.type.value
            presentModal(vc, true)
        case .counselling_session:
            let vc : CounsellingSessionVC = AppDelegate.AlertSB.instanceVC()
            vc.type = item.type.value
            presentModal(vc, true)
        case .stop_payment:
            
            self.showCustomAlert(title: ATTENTION, message: item.message, okTitle: OK_TITLE, cancelTitle: CANCEL) { (action) in
                if action {
                    let vc : StopPaymentVC = AppDelegate.AlertSB.instanceVC()
                    vc.type = item.type.value
                    self.presentModal(vc, true)
                }
            }
            
            
            
//            let vc : StopPaymentVC = AppDelegate.AlertSB.instanceVC()
//            vc.type = item.type.value
//            presentModal(vc, true)
        case .request_document:
            let vc : RequestDocumentVC = AppDelegate.AlertSB.instanceVC()
            vc.type = item.type.value
            presentModal(vc, true)
        case .extra_payment:
            let vc : MakePaymentVC = AppDelegate.AlertSB.instanceVC()
            vc.type = item.type.value
            presentModal(vc, true)
        case .talk_someone:
            let vc : TalkSomeoneVC = AppDelegate.AlertSB.instanceVC()
            vc.type = item.type.value
            presentModal(vc, true)
        case .alert:
        break
        case .None:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cell_Height
    }
   
}
