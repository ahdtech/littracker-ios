//
//  FormsVC.swift
//  LITracker
//
//  Created by Mohammed on 10/20/21.
//  Copyright © 2021 Snownet. All rights reserved.
//

import UIKit
import SwiftyPickerPopover
import MonthYearPicker

class FormsVC: BasicVC {
    
    var month : String = ""
    var year : String = ""
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layout_ContainerHeight: NSLayoutConstraint!
    
    private let cell_Height : CGFloat = 75
    private var formssArray : [FormsSt] = []
    let refresh = UIRefreshControl()
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationItem
        setNavigationBarHidden(false)
        self.navigationController?.navigationBar.backgroundColor =  .red
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refresh.initRefresh()
        //        refresh.addTarget(self, action: #selector(getData), for: .valueChanged)
        //        tableView.refreshControl = refresh
        self.calcHeight()
        configure()
        getData()
        
    }
    
    func configure(){
        navigationItem.title = "My I&E"
        hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        //        tableView.backgroundColor = BACKGROUND_EFF2F9
        tableView.registerCell(id: "FormsTitleTVC")
    }
    
    
    func calcHeight() {
        tableView.reloadData()
        layout_ContainerHeight.constant = (cell_Height * CGFloat(formssArray.count)) + 20
    }
    
    @objc private func getData(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        MyApi.api.getForms { (items, message, status) in
            self.hideIndicator()
            self.formssArray = items
            self.tableView.reloadData()
        }
    }
    
    @IBAction func btn_addFormsAction(_ sender: UIButton) {
//        DatePickerPopover(title: "DatePicker")
//            .setDateMode(.date)
//            .setMaximumDate(Date())
//            .setPreferredDatePickerStyle(.inline)
//            .setDoneButton(action: { _, selectedDate in
//                self.month = selectedDate.toString("MM")
//                self.year = selectedDate.toString("yyyy")
//
//                let vc : AddFormsVC = AppDelegate.FormsSB.instanceVC()
//                vc.hideBackWord()
//                vc.month = Int(self.month)!
//                vc.year  = Int(self.year)!
//                vc.delegate = {
//                    self.getData()
//                }
//                self.pushNavVC(vc)
//
//            })
//            .appear(originView: sender, baseViewController: self)
        
        let vc : ChooseDateVC = AppDelegate.AlertSB.instanceVC()
        vc.delegate = {  (month, year) in
            let vc : AddFormsVC = AppDelegate.FormsSB.instanceVC()
            vc.hideBackWord()
            vc.month = Int(month)!
            vc.year  = Int(year)!
            vc.delegate = {
                self.getData()
            }
            self.pushNavVC(vc)
        }
        presentModal(vc, true)
        
    }
    
    
}

extension FormsVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formssArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : FormsTitleTVC = tableView.dequeueTVCell()
        cell.selectionStyle = .none
        let item = formssArray[indexPath.row]
        cell.lblTitle.text = "FORM FOR \(item.month)/\(item.year)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = formssArray[indexPath.row]
        let vc : AddFormsVC = AppDelegate.FormsSB.instanceVC()
        vc.hideBackWord()
        vc.month = Int(item.month)!
        vc.year  = Int(item.year)!
        vc.delegate = {
            self.getData()
        }
        self.pushNavVC(vc)
    }
    
}
