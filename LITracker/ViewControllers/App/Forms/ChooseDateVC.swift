//
//  ChooseDateVC.swift
//  LITracker
//
//  Created by Mohammed on 10/25/21.
//  Copyright © 2021 Snownet. All rights reserved.
//

import UIKit
import MonthYearPicker

class ChooseDateVC: UIViewController {
    
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var picker: MonthYearPickerView!
    
    var month : String = ""
    var year : String = ""
    var delegate : ((String, String)->Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.maximumDate = Date()
        picker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        dateChanged(picker)
        // Do any additional setup after loading the view.
    }
    
    @objc func dateChanged(_ picker: MonthYearPickerView) {
        self.month = picker.date.toString("MM")
        self.year = picker.date.toString("yyyy")
        print("date changed: \(month) \(year)")
        lbl_Date.text = "\(picker.date.toString("MMM")) \(year)"
    }
    
    @IBAction func btn_okAction(_ sender: Any) {
        self.delegate?(self.month, self.year)
        self.dismissVC()
    }
    
    @IBAction func btn_CancelAction(_ sender: Any) {
        dismissVC()
    }
    
}
