//
//  FormsVC.swift
//  LITracker
//
//  Created by Mohammed on 10/14/21.
//  Copyright © 2021 Mohammed Alefrangy. All rights reserved.
//

import UIKit

class AddFormsVC: BasicVC {
    
    @IBOutlet weak var titleFormsCV         : UICollectionView!
    @IBOutlet weak var mainCV               : UICollectionView!
    @IBOutlet weak var layout_cvHeight      : NSLayoutConstraint!
    //    @IBOutlet weak var layout_maincvHeight  : NSLayoutConstraint!
    
    @IBOutlet weak var btnAddAttachment     : UIButton!
    @IBOutlet weak var btnPreview           : UIButton!
    @IBOutlet weak var btnNext              : UIButton!
    @IBOutlet weak var btnPervious          : UIButton!
    @IBOutlet weak var btn_Incomes          : UIButton!
    @IBOutlet weak var btn_Expenses         : UIButton!
    @IBOutlet weak var view_Incomes_seg     : UIView!
    @IBOutlet weak var view_Expenses_seg    : UIView!
    
    @IBOutlet weak var view_Total_Incomes        : UIStackView!
    @IBOutlet weak var lbl_TotalNameIncom        : UILabel!
    @IBOutlet weak var lbl_debtor                : UILabel!
    @IBOutlet weak var lbl_spouse                : UILabel!
    @IBOutlet weak var lbl_other                 : UILabel!
    var totalDebtor : Double = 0.0
    var totalSpouse : Double = 0.0
    var totalOther  : Double = 0.0
    
    @IBOutlet weak var lbl_Total_debtor          : UILabel!
    @IBOutlet weak var lbl_Total_spouse          : UILabel!
    @IBOutlet weak var lbl_Total_other           : UILabel!
    @IBOutlet weak var view_Total_For_Individual : UIStackView!
    
    @IBOutlet weak var view_Total_Expenses       : UIStackView!
    @IBOutlet weak var lbl_totalExpenses         : UILabel!
    @IBOutlet weak var lbl_TotalNameExpenses     : UILabel!
    var totalValue  : Double = 0.0
    
    private var currentIndex            : Int = 0
    private var req_type                : IncomType   = .Incomes
    private var incomeArray             : [IncomeSt]  = []
    private var expensesArray           : [ExpenseSt] = []
    private var incomeObject            : IncomeSt?
    private var expensesObject          : ExpenseSt?
    private var attachmentsArray        : [AttachmentsSt]?
    
    var delegate : (()->Void)?
    
    var month : Int = 0
    var year  : Int = 0
    private var catergoryID : Int = 0
    var nameOfCatergory : String = ""
    private var font = MyTools.appFont(.Regular, size: 13)
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hideBackWord()
    }
    
    override func viewDidLoad() {
        getIncomeExpenseform()
        configure()
    }
    
    private func configure() {
        titleFormsCV.delegate = self
        titleFormsCV.dataSource = self
        titleFormsCV.registerCell(id: FormsTitleCVC.self)
        titleFormsCV.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        mainCV.delegate = self
        mainCV.dataSource = self
        mainCV.registerCell(id: FormsMainCVC.self)
        mainCV.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        //        titleFormsCV.isScrollEnabled = false
        mainCV.isScrollEnabled = false
        
        selected_color(true, false)
        show_hide_seg(sender: btn_Incomes)
        hideButton()
    }
    
    //MARK: check collectionView && TableView content size
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard keyPath == "contentSize" else {return}
        if let obj = object as? UICollectionView, obj == titleFormsCV {
            layout_cvHeight.constant = obj.contentSize.height
        }
        //        else if let obj = object as? UICollectionView, obj == mainCV {
        //            layout_maincvHeight.constant = obj.contentSize.height
        //        }
    }
    
    private func hideButton(){
        if currentIndex == 0 {
            btnPervious.isHidden = true
            btnNext.isHidden = false
            btnPreview.isHidden = true
        } else {
            btnPervious.isHidden = false
            btnNext.isHidden = false
            btnPreview.isHidden = true
        }
        
        if req_type == .Incomes {
            if currentIndex == incomeArray.count - 1 {
                btnPervious.isHidden = false
                btnPreview.isHidden = false
                btnNext.isHidden = true
            }
        } else if req_type == .Expenses {
            if currentIndex == expensesArray.count - 1 {
                btnPervious.isHidden = false
                btnPreview.isHidden = false
                btnNext.isHidden = true
            }
        }
        
        btnAddAttachment.isHidden = req_type == .Incomes ? false : true
        titleFormsCV.reloadData()
        titleFormsCV.setNeedsLayout()
        titleFormsCV.setNeedsFocusUpdate()
    }
    
    @IBAction func addAttachmentAction(_ sender: Any) {
        let vc : AttachmentVC = AppDelegate.FormsSB.instanceVC()
        vc.attachmentsArray    = self.attachmentsArray
        vc.month = self.month
        vc.year = self.year
        vc.catergoryID = self.catergoryID
        vc.delegate = { (array) in
            
//            if let row = self.incomeArray.firstIndex(where: {$0.id == self.catergoryID}) {
//                self.incomeArray[row].attachments = array
//                self.mainCV.reloadData()
//            }
            self.selected_color(true, false)
            self.show_hide_seg(sender: self.btn_Incomes)
            self.hideButton()
            self.getIncomeExpenseform()
        }
        pushNavVC(vc)
    }
    
    
    @IBAction func nextAction(_ sender:UIButton){
        nextCell(titleFormsCV)
        nextCell(mainCV)
    }
    
    @IBAction func previousAction(_ sender:UIButton){
        previousCell(titleFormsCV)
        previousCell(mainCV)
    }
    
    func nextCell(_ collectionView: UICollectionView) {
        guard req_type == .Incomes ? !incomeArray.isEmpty : !expensesArray.isEmpty else {return}
        guard let indexPath = collectionView.currentIndex else {return}
        if req_type == .Incomes {
            guard indexPath.item < incomeArray.count + 1 else {return}
        } else {
            guard indexPath.item < expensesArray.count + 1 else {return}
        }
        let index_path = IndexPath(row: indexPath.item + 1, section: 0)
        
        collectionView.scrollToItem(at: index_path, at: .centeredHorizontally, animated: true)
        currentIndex = indexPath.item + 1
        collectionView.reloadData()
        collectionView.setNeedsLayout()
        hideButton()
    }
    
    func previousCell(_ collectionView: UICollectionView) {
        guard req_type == .Incomes ? !incomeArray.isEmpty : !expensesArray.isEmpty else {return}
        guard let indexPath = collectionView.currentIndex else {return}
        let index_path = IndexPath(row: indexPath.item - 1, section: 0)
        collectionView.scrollToItem(at: index_path, at: .centeredHorizontally, animated: true)
        currentIndex = indexPath.item - 1
        collectionView.reloadData()
        collectionView.setNeedsLayout()
        hideButton()
    }
    
    @IBAction func btm_saveAction(_ sender: Any) {
//        guard incomeObject   != nil else {return}
//        guard expensesObject != nil else {return}
        
        var params : [String:Any]
        params = ["month" : month,
                  "year"  : year]
        
        showIndicator()
        for item in incomeArray {
            for itemFields in item.fields {
                let para:NSMutableDictionary = NSMutableDictionary()
                para.setValue(Double(itemFields.data.debtor), forKey: "debtor")
                para.setValue(Double(itemFields.data.spouse), forKey: "spouse")
                para.setValue(Double(itemFields.data.other), forKey: "other")
                let jsonData = try! JSONSerialization.data(withJSONObject: para, options:[])
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
                params["data[\(itemFields.id)]"] = jsonString
            }
        }
        
        for item in expensesArray {
            for itemFields in item.fields {
                let para:NSMutableDictionary = NSMutableDictionary()
                para.setValue(Double(itemFields.data.value), forKey: "value")
                let jsonData = try! JSONSerialization.data(withJSONObject: para, options:[])
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
                params["data[\(itemFields.id)]"] = jsonString
            }
        }
        
        MyApi.api.incomeExpenseDataUpdate(params){ (message, status) in
            self.hideIndicator()
            self.showToast(message)
            self.delegate?()
            self.popVC()
        }
        
    }
    
    @IBAction func btn_PreviewAction(_ sender: Any) {
        let vc : PreviewFormVC = AppDelegate.FormsSB.instanceVC()
        vc.incomeArray    = self.incomeArray
        vc.expensesArray  = self.expensesArray
        vc.month = self.month
        vc.year  = self.year
        vc.delegate = {
            self.delegate?()
            self.popVC()
        }
        self.pushNavVC(vc)
    }
    
}

extension AddFormsVC {
    
    @objc private func getIncomeExpenseform(){
        guard isConnectedToNetwork() else {showToastNoInternt();return}
        showIndicator()
        let params : [String:Any] = ["month": self.month,
                                     "year": self.year]
        
        MyApi.api.getIncomeExpenseform(params){ (incomeArray,expenseArray, message, status) in
            self.hideIndicator()
            if !status {self.showMessageAlert(message);return}
            self.incomeArray = incomeArray
            self.expensesArray = expenseArray
            
            self.titleFormsCV.reloadData()
            self.mainCV.reloadData()
        }
    }
}

extension AddFormsVC {
    
    // MARK: Incomes && Expenses Button action
    @IBAction func TypeAction(_ sender : UIButton) {
        show_hide_seg(sender: sender)
        req_type            = (sender.tag == 0) ? .Incomes : .Expenses
        let isIncomes       = (btn_Incomes == sender)
        let isExpenses      = (btn_Expenses == sender)
        selected_color(isIncomes, isExpenses)
        
        let index_path = IndexPath(row: 0, section: 0)
        titleFormsCV.scrollToItem(at: index_path, at: .centeredHorizontally, animated: true)
        mainCV.scrollToItem(at: index_path, at: .centeredHorizontally, animated: true)
        currentIndex = 0
        hideButton()
        titleFormsCV.reloadData()
        mainCV.reloadData()
    }
    
    // MARK: set color for button select ( Incomes || Expenses )
    func selected_color(_ isIncomes: (Bool), _ isExpenses: (Bool)) {
        btn_Incomes.setTitleColor(isIncomes ? PRIMARY_COLOR : .black, for: .normal)
        view_Incomes_seg.backgroundColor     = isIncomes ? PRIMARY_COLOR : .white
        
        view_Expenses_seg.backgroundColor   = isExpenses ? PRIMARY_COLOR : .white
        btn_Expenses.setTitleColor(isExpenses ? PRIMARY_COLOR : .black, for: .normal)
    }
    
    // MARK: show or hide when select button ( Incomes || Expenses )
    func show_hide_seg(sender: UIButton) {
        view_Incomes_seg.isHidden = !(sender == btn_Incomes)
        view_Expenses_seg.isHidden = !(sender == btn_Expenses)
        
        view_Total_Incomes.isHidden = !(sender == btn_Incomes)
        view_Total_For_Individual.isHidden = true
        view_Total_Expenses.isHidden = !(sender == btn_Expenses)
    }
    
}


// MARK: CollectionView
extension AddFormsVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case titleFormsCV:
            if req_type == .Incomes {
                return incomeArray.count
            } else {
                return expensesArray.count
            }
            
        case mainCV:
            
            if req_type == .Incomes {
                return incomeArray.count
            } else {
                return expensesArray.count
            }
            
        default: return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        
        case titleFormsCV :
            let cell : FormsTitleCVC = collectionView.dequeueCVCell(indexPath: indexPath)
            cell.lbl_Number.text = "\(indexPath.row + 1)"
            
            if req_type == .Incomes {
                let item = incomeArray[indexPath.item]
                cell.lbl_Title.text = item.name
                
                if indexPath.row == currentIndex {
                    self.lbl_TotalNameIncom.text = "Total \(item.name)"
                }
                
                if currentIndex == 1 {
                    view_Total_For_Individual.isHidden = false
                } else {
                    view_Total_For_Individual.isHidden = true
                }
                
                if indexPath.row == 1 {
                    cell.lineView.isHidden = true
                }else {
                    cell.lineView.isHidden = false
                }
                
            } else {
                let item = expensesArray[indexPath.item]
                cell.lbl_Title.text = item.name
                nameOfCatergory = item.name
                
                if indexPath.row == currentIndex {
                    self.lbl_TotalNameExpenses.text = "Total \(item.name)"
                }
                
                if indexPath.row == 6 {
                    cell.lineView.isHidden = true
                }else {
                    cell.lineView.isHidden = false
                }
                
                
            }
            
            if indexPath.row == currentIndex {
                cell.viewNumber.isHidden = false
                cell.imageView_Done.isHidden = true
                cell.lbl_Title.font = UIFont.boldSystemFont(ofSize: 15)
                cell.lbl_Title.textColor = .black
                cell.imageViewBorder.tintColor = .red
            } else if indexPath.row < currentIndex {
                cell.viewNumber.isHidden = true
                cell.imageView_Done.isHidden = false
                cell.lbl_Title.textColor = .black
            } else if indexPath.row > currentIndex {
                cell.viewNumber.isHidden = false
                cell.imageView_Done.isHidden = true
                cell.lbl_Title.textColor = .gray
                cell.lbl_Title.font = UIFont.systemFont(ofSize: 15)
                cell.imageViewBorder.tintColor = .gray
            }
            
            return cell
            
        case mainCV :
            
            let cell : FormsMainCVC = collectionView.dequeueCVCell(indexPath: indexPath)
            cell.req_type = req_type
            
            if req_type == .Incomes {
                let item = incomeArray[indexPath.row]
                self.incomeObject = item
                
                if currentIndex == indexPath.row {
                    self.catergoryID = item.id
                    self.attachmentsArray = item.attachments
                }
                
                cell.income = item
                cell.pass_obj_Income = { obj in
                    self.incomeObject = obj
                    self.setTotal()
                }
                self.setTotal()
                
            } else {
                let item = expensesArray[indexPath.row]
                self.expensesObject = item
                cell.expense = item
                cell.pass_obj_Expense = { obj in
                    self.expensesObject = obj
                    self.setTotal()
                }
                self.setTotal()
            }
            return cell
        default:
            let cell : FormsTitleCVC = collectionView.dequeueCVCell(indexPath: indexPath)
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        
        if collectionView == titleFormsCV {
            titleFormsCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            mainCV.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            currentIndex = indexPath.item
            
            titleFormsCV.reloadData()
            mainCV.reloadData()
            hideButton()
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == mainCV {
            let visibleRect = CGRect(origin: mainCV.contentOffset, size: mainCV.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = mainCV.indexPathForItem(at: visiblePoint) else { return }
            currentIndex = indexPath.item
        }else {
            let visibleRect = CGRect(origin: titleFormsCV.contentOffset, size: titleFormsCV.bounds.size)
            let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
            guard let indexPath = titleFormsCV.indexPathForItem(at: visiblePoint) else { return }
            currentIndex = indexPath.item
        }
    }
    
    func setTotal() {
        self.totalDebtor = 0.0
        self.totalSpouse = 0.0
        self.totalOther  = 0.0
        self.totalValue  = 0.0
        
        if req_type == .Incomes {
            
            for item in self.incomeArray[currentIndex].fields {
                totalDebtor += item.data.debtor
                totalSpouse += item.data.spouse
                totalOther  += item.data.other
                lbl_debtor.text = "$\(totalDebtor.styleFormat2)"
                lbl_spouse.text = "$\(totalSpouse.styleFormat2)"
                lbl_other.text  = "$\(totalOther.styleFormat2)"
            }
            
            if incomeObject?.total_income_type == "sub" {
                view_Total_For_Individual.isHidden = false
                var totalDebtor  = 0.0
                var totalSpouse  = 0.0
                var totalOther   = 0.0
                for item in self.incomeArray[0].fields {
                    totalDebtor      += item.data.debtor
                    totalSpouse      += item.data.spouse
                    totalOther       += item.data.other
                    
                    let total_debtor = totalDebtor - self.totalDebtor
                    let total_spouse = totalDebtor - self.totalSpouse
                    let total_other  = totalDebtor - self.totalOther
                    
                    lbl_Total_debtor.text = "$\(total_debtor.styleFormat2)"
                    lbl_Total_spouse.text = "$\(total_spouse.styleFormat2)"
                    lbl_Total_other.text  = "$\(total_other.styleFormat2)"
                }
            } else {
                view_Total_For_Individual.isHidden = true
            }
            
        } else {
            
            for item in self.expensesArray[currentIndex].fields {
                totalValue += item.data.value
                lbl_totalExpenses.text = "$\(totalValue.styleFormat2)"
            }
        }
    }
    
}

extension AddFormsVC : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow: CGFloat = 2
        let sectionInsets = UIEdgeInsets(top: 0,
                                         left: 0,
                                         bottom: 0,
                                         right: 0)
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = titleFormsCV.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        switch collectionView {
        case titleFormsCV:
            if req_type == .Incomes {
                let item = incomeArray[indexPath.row]
                let width = item.name.width(height: 17, font: font)
                return CGSize(width: widthPerItem + 10, height: 50)
//                return CGSize(width: width + 40, height: 50)
                
            } else {
                let item = expensesArray[indexPath.row]
                let width = item.name.width(height: 17, font: font)
                return .init(width: widthPerItem + width, height: 50)
                //                let widtht = (titleFormsCV.frame.size.width)
                //                return CGSize(width: width + widtht + 40, height: 50)
            }
            
           
        case mainCV:
            return self.mainCV.frame.size
        default: return .zero
        }
    }
    
}
