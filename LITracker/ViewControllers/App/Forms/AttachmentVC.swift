//
//  AttachmentVC.swift
//  LITracker
//
//  Created by Mohammed on 11/18/21.
//  Copyright © 2021 Mohammed Alefrangy & Snownet . All rights reserved.
//

import UIKit
import Alamofire

class AttachmentVC: BasicVC, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet weak var collectionView   : UICollectionView!
    @IBOutlet weak var layout_cvHeight  : NSLayoutConstraint!
    //    var incomeArray   : [IncomeSt]!
    
    var attachmentsArray : [AttachmentsSt]!
    var documentInteractionController: UIDocumentInteractionController!
    
    private let itemsPerRow: CGFloat = 3
    private let sectionInsets = UIEdgeInsets(
        top: 0,
        left: 0,
        bottom: 0,
        right: 0)
    
    private var imageID  = 4000
    private var fileID   = 5000
    var month : Int = 0
    var year  : Int = 0
    var catergoryID : Int = 0
    var delegate   : (([AttachmentsSt])->Void)?
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hideBackWord()
        attachmentsArray.insert(AttachmentsSt(["id":-1,
                                               "file":"",
                                               "created_at":"",
                                               "stringType":StringType.None.value,
                                               "isLocalFile":false]), at: 0)
        
        for item in attachmentsArray {
            if item.id != -1 {
                if item.file.contains("jpg") || item.file.contains("png") || item.file.contains("jpeg") || item.file.contains("gif")  {
                    item.stringType = StringType.Image
                } else {
                    item.stringType = StringType.PDF
                }
            }
        }
    }
    
    override func viewDidLoad() {
        configure()
    }
    
    private func configure() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(id: AttachmentCVC.self)
        collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    //MARK: check collectionView && TableView content size
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard keyPath == "contentSize" else {return}
        if let obj = object as? UICollectionView, obj == collectionView {
            layout_cvHeight.constant = obj.contentSize.height
        }
    }
    
    func downloadFile(urlFile : String,fileExten : String, completion: @escaping (String?, Error?) -> Void) {
        self.showIndicator()
        let destination: DownloadRequest.Destination = { _, _ in
            var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            documentsURL.appendPathComponent("file.\(fileExten)")
            return (documentsURL, [.removePreviousFile])
        }
        let file = urlFile.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        AF.download(file, to: destination).responseData { response in
            self.hideIndicator()
            
            if let destinationUrl = response.fileURL {
                completion("\(destinationUrl)", nil)
            }
        }
    }
    
    
    @IBAction func btn_saveAttachmentsActon(_ sender: Any) {
        self.showIndicator()
        
        var arrayImage  : [UIImage] = []
        var arrayOfInt  : [Int] = []
        var arrayOfAny  : [Any] = []
        var params : [String:Any]
        
        params = ["month" : month,
                  "year"  : year,
                  "category_id": catergoryID]
        
        for item in attachmentsArray {
            if item.id != -1 {
                if item.isLocalFile {
                    if item.file.contains("jpg") || item.file.contains("png") || item.file.contains("jpeg") || item.file.contains("gif")  {
                        arrayOfAny.append(item.file.stringToImage()!)
                    } else {
                        let url  = URL(string: item.file)
                        arrayOfAny.append(url)
                    }
                    params["new_data[]"] = arrayOfAny
                } else {
                    arrayOfInt.append(item.id)
                    params["old_data[]"] = arrayOfInt
                }
            }
        }
        
        print("\(params)")
        MyApi.uploadImagesWithParam(.attachments_form, Parameters: params, _images: arrayImage) { (response,array, err, msg, status) in
            self.hideIndicator()
            self.showToast(msg)
            self.delegate?(array)
            self.popVC()
        }
    }
    
}


// MARK: CollectionView
extension AttachmentVC : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachmentsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : AttachmentCVC = collectionView.dequeueCVCell(indexPath: indexPath)
        let item = attachmentsArray![indexPath.item]
        
        switch item.stringType {
        case .Image:
            cell.view_add.isHidden   = true
            cell.view_file.isHidden  = true
            cell.view_image.isHidden = false
            if item.isLocalFile {
                cell.image.image = item.file.stringToImage()
            }else {
                cell.image.fetchImage(item.file)
            }
            
            cell.btn_delete.isHidden = false
        case .PDF:
            cell.view_add.isHidden   = true
            cell.view_file.isHidden  = false
            cell.view_image.isHidden = true
            cell.btn_delete.isHidden = false
            if item.isLocalFile {
                let url = URL(string: item.file)
                cell.lblName.text = url?.pathExtension.uppercased() ?? ""
            }else {
                cell.lblName.text = item.file.fileExtension().uppercased()
            }
            
        case .None:
            cell.view_add.isHidden   = false
            cell.view_file.isHidden  = true
            cell.view_image.isHidden = true
            cell.btn_delete.isHidden = true
        default:
            cell.view_add.isHidden   = false
            cell.view_file.isHidden  = true
            cell.view_image.isHidden = true
            cell.btn_delete.isHidden = false
        }
        
        cell.item = item
        
        cell.delegateDelete = { (id) in
            
            if let row = self.attachmentsArray.firstIndex(where: {$0.id == id}) {
                self.attachmentsArray.remove(at: row)
                self.collectionView.reloadData()
            }
        }
        
        cell.delegateAddPhoto = {
            CameraHandler.shared.showActionSheet(self)
            
            CameraHandler.shared.imagePickedBlock = { img, name in
                
                self.attachmentsArray.append(AttachmentsSt(["id":self.imageID,
                                                            "file":img.toPngString(),
                                                            "created_at":Date(),
                                                            "stringType":StringType.Image.value,
                                                            "isLocalFile":true]))
                self.imageID += 1
                self.collectionView.reloadData()
                
            }
            
            CameraHandler.shared.filesPickedBlock = { url, name in
                
                let file = "\(url)"
                self.attachmentsArray.append(AttachmentsSt(["id": self.fileID,
                                                            "file": file,
                                                            "created_at": Date(),
                                                            "stringType": StringType.PDF.value,
                                                            "isLocalFile": true]))
                self.fileID += 1
                self.collectionView.reloadData()
            }
        }
        
        cell.delegateShowPhoto = {
            var newImageView = UIImageView()
            if item.isLocalFile {
                newImageView.image = item.file.stringToImage()
            }else {
                newImageView.fetchImage(item.file)
            }
            
            newImageView.frame = UIScreen.main.bounds
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleAspectFit
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
            self.tabBarController?.tabBar.isHidden = true // tabBarController exists
            self.navigationController?.isNavigationBarHidden = true // default  navigationController
            
        }
        
        cell.delegateShowFile = {
            
            if item.isLocalFile {
                print("The path is \(URL(string: item.file))")
                
                
            } else {
                self.downloadFile(urlFile: item.file , fileExten: item.file.fileExtension() ) { (fileURL, error) in
                    if error != nil {
                        self.hideIndicator()
                        return
                    }else{
                        if let url = URL(string: fileURL!) {
                            self.documentInteractionController = UIDocumentInteractionController(url: url)
                            self.documentInteractionController.delegate = self
                            self.documentInteractionController.presentOptionsMenu(from: self.view.frame, in: self.view, animated: true)
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = false
        sender.view?.removeFromSuperview() // This will remove image from full screen
    }
    
}

extension AttachmentVC : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        print("New Width \(widthPerItem)")
        return .init(width: widthPerItem - 10, height: widthPerItem - 10)
        
    }
}
