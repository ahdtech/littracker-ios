//
//  PreviewFormVC.swift
//  LITracker
//
//  Created by Mohammed on 10/21/21.
//  Copyright © 2021 Snownet. All rights reserved.
//

import UIKit

class PreviewFormVC: BasicVC {
    
    
    @IBOutlet weak var tableView                : UITableView!
    @IBOutlet weak var layout_ContainerHeight   : NSLayoutConstraint!
    
    @IBOutlet weak var lbl_TotalIncomeForIndividual : UILabel!
    @IBOutlet weak var lbl_TotalMonthlyExpenes      : UILabel!
    @IBOutlet weak var lbl_ExcessDeficit            : UILabel!
    
    var incomeArray             : [IncomeSt]!
    var expensesArray           : [ExpenseSt]!
    
    
    private let cell_Height : CGFloat = 85
    
    var month : Int = 0
    var year  : Int = 0
    var delegate : (()->Void)?
    
    private var totalIncomForFamily    : Double = 0.0
    private var totalNonDiscretionary  : Double = 0.0
    private var totalExpenses          : Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    func configure(){
        
        navigationItem.title = "My I&E"
        hideBackWord()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        //        tableView.backgroundColor = BACKGROUND_EFF2F9
        
        tableView.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        tableView.registerCell(id: "IncomeTVC")
        tableView.registerCell(id: "ExpensesTVC")
        self.setTotal()
        tableView.reloadData()
    }
    
    
    @IBAction func btm_saveAction(_ sender: Any) {
//        guard incomeObject   != nil else {return}
//        guard expensesObject != nil else {return}
        
        var params : [String:Any]
        params = ["month" : month,
                  "year"  : year]
        
        showIndicator()
        for item in incomeArray {
            for itemFields in item.fields {
                let para:NSMutableDictionary = NSMutableDictionary()
                para.setValue(Double(itemFields.data.debtor), forKey: "debtor")
                para.setValue(Double(itemFields.data.spouse), forKey: "spouse")
                para.setValue(Double(itemFields.data.other), forKey: "other")
                let jsonData = try! JSONSerialization.data(withJSONObject: para, options:[])
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
                params["data[\(itemFields.id)]"] = jsonString
            }
        }
        
        for item in expensesArray {
            for itemFields in item.fields {
                let para:NSMutableDictionary = NSMutableDictionary()
                para.setValue(Double(itemFields.data.value), forKey: "value")
                let jsonData = try! JSONSerialization.data(withJSONObject: para, options:[])
                let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) as! String
                params["data[\(itemFields.id)]"] = jsonString
            }
        }
        
        MyApi.api.incomeExpenseDataUpdate(params){ (message, status) in
            self.hideIndicator()
            self.showToast(message)
            self.delegate?() 
            self.popVC()
        }
        
    }
    
}

extension PreviewFormVC : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        
        switch section {
        case 0:
            headerView.lblTitleHeaderView.text = "Income"
        case 1:
            headerView.lblTitleHeaderView.text = "Expenses"
        default:
            break
        }
        
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return incomeArray.count
        case 1:
            return expensesArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell : IncomeTVC = tableView.dequeueTVCell()
            var totalDebtor : Double = 0
            var totalSpouse : Double = 0
            var totalOther  : Double = 0
            
            let item = incomeArray[indexPath.row]
            cell.selectionStyle = .none
            cell.lbl_Title.text = "Total \(item.name)"
            cell.lbl_TotalValue.isHidden = false
            
            for fieldsItem in item.fields {
                totalDebtor += fieldsItem.data.debtor
                totalSpouse += fieldsItem.data.spouse
                totalOther  += fieldsItem.data.other
            }
            
            cell.txt_Debtor.text = "$\(totalDebtor.styleFormat2)"
            cell.txt_Spouse.text = "$\(totalSpouse.styleFormat2)"
            cell.txt_Other.text = "$\(totalOther.styleFormat2)"
            
            let total = totalDebtor + totalSpouse + totalOther
            cell.lbl_TotalValue.text = "$\(total.styleFormat2)"
            cell.txt_Debtor.isEnabled = false
            cell.txt_Spouse.isEnabled = false
            cell.txt_Other.isEnabled = false
            
            return cell
        case 1:
            let cell : ExpensesTVC = tableView.dequeueTVCell()
            cell.selectionStyle = .none
            var total : Double = 0
            let item = expensesArray[indexPath.row]
            cell.lbl_Title.text = "Total \(item.name)"
            
            for fieldsItem in item.fields {
                total += fieldsItem.data.value
            }
            
            cell.txt_Value.text = "$\(total.styleFormat2)"
            cell.txt_Value.isEnabled = false
            
            return cell
        default:
            let cell : IncomeTVC = tableView.dequeueTVCell()
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    
    func setTotal() {
        self.totalIncomForFamily = 0.0
        self.totalNonDiscretionary = 0.0
        
        for item in incomeArray {
            var total = 0.0
            
            for itemFileds in item.fields {
                total +=  itemFileds.data.debtor + itemFileds.data.spouse + itemFileds.data.other
            }
            
            if item.total_income_type == "main" {
                totalIncomForFamily = total
            } else {
                totalNonDiscretionary = total
            }
        }
        
        for item in expensesArray {
            for itemFileds in item.fields {
                self.totalExpenses += itemFileds.data.value
            }
        }
        
        let totalIncomeForIndividual = totalIncomForFamily - totalNonDiscretionary
        self.lbl_TotalIncomeForIndividual.text = "$\(totalIncomeForIndividual.styleFormat2)"
        
        self.lbl_TotalMonthlyExpenes.text = "$\(totalExpenses.styleFormat2)"
        
        let excessDeficit = (totalIncomForFamily - totalNonDiscretionary) - totalExpenses
        self.lbl_ExcessDeficit.text = "$\(excessDeficit.styleFormat2)"
    }
}
