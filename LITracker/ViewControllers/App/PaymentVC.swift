//
//  PaymentVC.swift
//  LITtracker
//
//  Created by Mohammed on 4/25/21.
//

import UIKit

class PaymentVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var payments_Array : [PaymentScheduleST] = []
    private let cell_Height : CGFloat = 20
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.registerCell(id: HeaderCellTVC.self)
        tableView.registerCell(id: RowCellTVC.self)
        
//        let refresh = UIRefreshControl()
//        refresh.initRefresh()
//        refresh.addTarget(self, action: #selector(getData), for: .valueChanged)
//        tableView.refreshControl = refresh
    }

    
    @IBAction func closeAction(_ sender: Any) {
        dismissVC()
    }
    
}


extension PaymentVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return payments_Array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell : HeaderCellTVC = tableView.dequeueTVCell()
            cell.selectionStyle = .none
            
            cell.dueDate.text           = "DUE DATE"
            cell.dueAmount.text         = "DUE AMOUNT"
            cell.depositedAmount.text   = "DEPOSITED AMOUNT"
            cell.osAmount.text          = "O/S AMOUNT"
            cell.completeDate.text      = "COMPLETE DATE"
            
            return cell
        default:
            let cell : RowCellTVC = tableView.dequeueTVCell()
            let item = payments_Array[indexPath.row]
            cell.selectionStyle = .none
            
            let due_Date                = item.due_date.toDate("yyyy-MM-dd")
            cell.dueDate.text           = due_Date.toString("yyyy-MM-dd")
           
            let dueAmount               = Float(item.due_amount)
            cell.dueAmount.text         = "$\(item.due_amount.styleFormat2)"
            
            let depositedAmount         = Float(item.deposited_amount)
            cell.depositedAmount.text   = "$\(item.deposited_amount.styleFormat2)"
            
            let osAmount                = Float(item.o_s_amount)
            cell.osAmount.text          = "$\(item.o_s_amount.styleFormat2)"
            
            if item.is_complete == 0 {
                cell.completeDate.text  = ""
                cell.imgCheck.image     = "redCheck".toImage
            }else {
                let complete_Date          = item.complete_date.toDate("yyyy-MM-dd")
                cell.completeDate.text     = complete_Date.toString("yyyy-MM-dd")
                cell.imgCheck.image        = "ic_check".toImage
            }
            
            return cell
        }
                
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 40
        default:
            return cell_Height
        }
        
    }
    
}

