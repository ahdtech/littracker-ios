//
//  AppDelegate.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit
import Firebase
import UserNotifications
import SwiftyJSON

var temp_notifi : NotifiSt?
var did_home_load = false


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    static let MainSB     = UIStoryboard(name: "Main"  , bundle: nil)
    static let AppSB      = UIStoryboard(name: "AppSB" , bundle: nil)
    static let AlertSB    = UIStoryboard(name: "AlertSB" , bundle: nil)
    static let FormsSB    = UIStoryboard(name: "FormsSB" , bundle: nil)
    
    override init() {
        super.init()
        FirebaseApp.configure()
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Route.publicStyleAppearance()
        
        //
        configureNotifications(application)
//        UIApplication.shared.applicationIconBadgeNumber = 0

        
        // Override point for customization after application launch.
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print(#function)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        print(#function)
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print(#function)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        print(#function)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        print(#function)
    }
    
    func configureNotifications(_ application:UIApplication){
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {(granted, error) in })
        application.registerForRemoteNotifications()
    }

}

extension AppDelegate : UNUserNotificationCenterDelegate , MessagingDelegate {
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) { }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        print("Device Token \(deviceToken.base64EncodedString())")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        guard let fcmToken = fcmToken else {return}
        Auth_User.FcmToken = fcmToken
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        if Auth_User.IsUserLogin {
            MyApi.api.updateFCMRequest(fcmToken)
        }
        Auth_User.FcmToken = fcmToken
        connectToFcm()
    }
    
    func connectToFcm() {
        Messaging.messaging().isAutoInitEnabled = true
    }
    
    
    //MARK: Receive displayed notifications for iOS 10 devices When the user open app.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        temp_notifi = nil
        let userInfo = notification.request.content.userInfo
        let top = Route.top_VC()
        
        if let data = userInfo["data"] as? String {
            if let dic = Route.convertToDictionary(text: data) {
                
                let obj = NotifiSt(JSON(dic))
                temp_notifi = obj
                guard let _id = Int(obj.action_id.toString) else {return}
                
                Auth_User.Action_Name = NotificationType(obj.action.value)
                Auth_User.Action_Id = _id
                Auth_User.Sender_Id = obj.sender_id
                
                let type = obj.action
                switch type {
                
                case .user_delete, .user_deactivate:
                    Auth_User.sessionExpired()
                    return
                    
                case .new_alert:
                    
                    top.postNotifcationCenter(.UpdateMessages)
                    let contactUsVC : ContactUsAsapVC = AppDelegate.AppSB.instanceVC()
                    Auth_User.Sender_Id = obj.sender_id
                    contactUsVC.message = temp_notifi?.message_text
                    contactUsVC.Action_Id = _id
                    did_home_load = false
                    top.pushNavVC(contactUsVC)
                    
                case .payment_reminder_2_days, .payment_reminder_10_days:
                    let reminderVC : ReminderVC = AppDelegate.AppSB.instanceVC()
                    reminderVC.message = temp_notifi?.message
                    top.pushNavVC(reminderVC)
                    
                case .ticket_resolved:
                    top.postNotifcationCenter(.UpdateMessages)
                    top.postNotifcationCenter(.UpdateChatViewController)
                    
                default:
                    temp_notifi = nil
                    if let top = Route.topVC(), top.isKind(of: HomeVC.self){
                        //                NotificationCenter.default.post(name: .UpdateNotificationBadge, object: nil)
                    }
                }
            }
        }
        
        completionHandler([.banner,.sound])
    }
    
    
    //MARK: When clicked on notification open specific VC
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        //MARK: When clicked on notification open specific VC
        let userInfo = response.notification.request.content.userInfo
        print("the user Info \(userInfo)")
        Route.openSpecificScreen(userInfo)
        completionHandler()
    }
}
