//
//  PopupMapViewController.swift
//  LITtracker
//
//  Created by Mohammed on 4/24/21.
//
import UIKit

class PopupMapViewController: UIViewController {

    @IBOutlet weak var txtAddress: UITextField!
    var address: String = ""
    var latitude  : Double = 0
    var longitude : Double = 0
    var selectLocation:((String,Double,Double) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    private func configure(){
        self.txtAddress.text = address
    }


    @IBAction func btnCancel(_ sender: Any) {
        self.dismissVC()
    }
    
    @IBAction func btnOK(_ sender: Any) {
        guard let address = txtAddress.text, !address.isEmptyStr else {
            showToast(ENTER_ADDRESS)
            return
        }
        self.dismissVC(true, {
            self.selectLocation?(address ,self.latitude,self.longitude)
        })
    }
}
