//
//  SelectMapLocationVC.swift
//  
//
//  Created by Tareq Safia on 10/20/19.
//  Copyright © 2019 Tareq Safia. All rights reserved.
//

import UIKit
import UIKit
import AVKit
import GoogleMaps
import IQKeyboardManagerSwift

class SelectMapLocationVC:  UIViewController ,GMSMapViewDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var view_showAddress: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btn_select: UIButton!

    var selectLocation:((String,Double,Double) -> Void)?
    
    
    private let locationManag = CLLocationManager()
    var latitude  : Double = 0
    var longitude : Double = 0
    private var searchAddress = "" {
        didSet {
            view_showAddress.isHidden = true
            setLayoutAnimation()
        }
    }
    
    private let address = AddressHandle.shared
    private var marker : GMSMarker?
    private var shared = LocationHandler.shared
    private var enableDidChange = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        title = "Map".localized
        btn_select.setTitle("Select".localized, for: .normal)
        shared.startLocation()
        shared.didLocateCurrentLocation = {lat,lon in
            if Int(self.latitude) == 0 {
                self.latitude  = lat
                self.longitude = lon
            }
            self.enableDidChange = true
            self.setMarker()
        }
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.settings.zoomGestures = true
        mapView.settings.scrollGestures = true
        mapView.delegate = self
        
        //
        definesPresentationContext = true
        
        showAutoComplete()
        
        //
        let color = PRIMARY_COLOR
        view_showAddress.backgroundColor = color
        btn_select.backgroundColor = color
        btn_select.font = MyTools.appFont(.SemiBold, size: 17)
        lbl_address.font = MyTools.appFont(size: 14)
    }
    
    func showAutoComplete(){
//        let img = getSearchImage()
//        let search = UIBarButtonItem(image: img, style: .plain,
//                                     target: self, action: #selector(openAutoComplete))
//        navigationItem.leftBarButtonItem = search
        
        let close = UIBarButtonItem(title: CANCEL_TITLE, style: .plain,
                                    target: self, action: #selector(closeScreen))
        navigationItem.rightBarButtonItem = close
        
    }
    
//    @objc func openAutoComplete(){
//        let acController = GMSAutocompleteViewController()
//        acController.extendedLayoutIncludesOpaqueBars = false
//        acController.delegate = self
//        self.presentVC(acController)
//    }
    
    //
    @objc func closeScreen(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setMarker(){
        
        let Location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let camera = GMSCameraPosition.camera(withLatitude:CLLocationDegrees(latitude) ,
                                              longitude:CLLocationDegrees(longitude) , zoom: 13)
        mapView.camera = camera
        mapView.animate(to: camera)
        updateLocationoordinates(coordinates: Location)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        guard enableDidChange else {return}
        latitude = position.target.latitude
        longitude = position.target.longitude
        updateLocationoordinates(coordinates: position.target)
    }
    
    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        if marker == nil {
            marker = GMSMarker()
            marker!.position = coordinates
            marker!.map = self.mapView
            marker!.appearAnimation = .pop
        } else{
            CATransaction.begin()
            CATransaction.setAnimationDuration(0.5)
            marker!.position =  coordinates
            CATransaction.commit()
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        guard enableDidChange else {return}
        let coordinate = position.target
        latitude = coordinate.latitude
        longitude = coordinate.longitude
        
        address.getAddressByCordinate(_lat: latitude, _lon: longitude) { (name, addrss, full) in
            self.searchAddress = addrss
            self.latitude = coordinate.latitude
            self.longitude = coordinate.longitude
            self.lbl_address.text = addrss
        }
    }
    
    @IBAction func didPressSelect(_ sender: UIButton) {
//        selectLocation?(self.searchAddress,self.latitude,self.longitude)
//        self.dismiss(animated: true, completion: nil)
        let vc : PopupMapViewController = AppDelegate.AlertSB.instanceVC()
        vc.address = self.lbl_address.text ?? ""
        vc.latitude = self.latitude
        vc.longitude = self.longitude
        vc.selectLocation = { (str, lat, lng) in
            self.selectLocation?(str, lat, lng)
            self.dismissVC()
        }
        self.presentModal(vc)
        
        
        
    }
}


//extension SelectMapLocationVC: GMSAutocompleteViewControllerDelegate {
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//        viewController.dismiss(animated: true, completion: nil)
//        let address = place.formattedAddress
//        searchAddress = address ?? ""
//        self.lbl_address.text = address
//        self.latitude = place.coordinate.latitude
//        self.longitude = place.coordinate.longitude
//        let camera = GMSCameraPosition.camera(withLatitude:CLLocationDegrees(latitude) ,
//                                              longitude:CLLocationDegrees(longitude) , zoom: 13)
//        mapView.camera = camera
//        mapView.animate(to: camera)
//        updateLocationoordinates(coordinates: place.coordinate)

//        let target = CLLocationCoordinate2D(latitude: self.latitude,
//                                            longitude: self.longitude)
//        let updatedCamera = GMSCameraUpdate.setTarget(target, zoom: 12.0)
//        self.mapView.animate(with: updatedCamera)
//    }
//
//    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
//
//    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
//        viewController.dismiss(animated: true, completion: nil)
//    }
        
//    func getSearchImage() -> UIImage?{
//        let image = "search-icon".toImage//UIImage(named: "search-icon2", in: Bundle(for: type(of: self)) , compatibleWith: nil)
//        return image
//    }
//}
