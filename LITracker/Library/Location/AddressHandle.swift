//
//  Address.swift
//  Routes
//
//  Created by Tareq Safia on 7/2/19.
//  Copyright © 2019 Tareq Safia. All rights reserved.
//

import UIKit
import GoogleMaps
//import GooglePlaces

class AddressHandle: NSObject {
    
    static let shared = AddressHandle()
    
    //private
    private var currentVC: UIViewController!
    private var _lat = 0.0
    private var _lon = 0.0
    
    var didPickAddress:((String,String,String,Double,Double) -> Void)?
    var didPickShortAddress:((String,Double,Double) -> Void)?
    
    func showPlacePicker(_ viewController: UIViewController,_ lat:Double?=nil,_ lon:Double?=nil) {
        currentVC = viewController
        
        let vc = SelectMapLocationVC()
        if let _lat = lat, let _lon = lon {
            vc.latitude = _lat
            vc.longitude = _lon
        }
        vc.selectLocation = didSelectLocation(_:_:_:)
        let nav = UINavigationController(rootViewController: vc)
        viewController.presentModal(nav)
    }
    
    func didSelectLocation(_ add:String,_ lat:Double,_ lon:Double) {
        currentVC.dismiss(animated: true, completion: nil)
        didPickShortAddress?(add,lat,lon)
    }

    func getAddressByCordinate(_lat:Double,_lon:Double,completion: @escaping (String,String,String) -> ()) {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: _lat, longitude: _lon)
        geoCoder.cancelGeocode()
        geoCoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            
            var addressString: String = ""
            
            let placemark = placemarks?.last!
            if placemark?.subThoroughfare != nil
            {
                addressString = (placemark?.subThoroughfare!)! + " "
            }
            if placemark?.thoroughfare != nil
            {
                addressString = addressString + (placemark?.thoroughfare!)! + ", "
            }
            if placemark?.postalCode != nil
            {
                addressString = addressString + (placemark?.postalCode!)! + " "
            }
            if placemark?.locality != nil
            {
                addressString = addressString + (placemark?.locality!)! + ", "
            }
            if placemark?.administrativeArea != nil
            {
                addressString = addressString + (placemark?.administrativeArea!)! + " "
            }
            if placemark?.country != nil
            {
                addressString = addressString + (placemark?.country!)!
            }
            let name = placemark?.name ?? ""
            
            var full_address = addressString
            if name != "" {
                full_address = name + ", " + addressString
            }
            completion(name,addressString,full_address)
        })
    }
}

