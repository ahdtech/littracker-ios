//
//  CurrentLocation.swift
//  JustClickAndEat
//
//  Created by iosMaher on 11/3/19.
//  Copyright © 2019 iosMaher. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class CurrentLocation: NSObject , CLLocationManagerDelegate {

    static let shared = CurrentLocation()
    
    private var _lat = 0.0
    private var _lon = 0.0
    private var locationManager = CLLocationManager()
    private var isStopLocation = true

    var delegate : ((CLLocationDegrees,CLLocationDegrees)->Void)?
    
    func startLocation(_ isStopLocation:Bool = true) {
        self.isStopLocation = isStopLocation
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse , .authorizedAlways:
            manager.startUpdatingLocation()
        case .denied , .restricted:
            manager.requestWhenInUseAuthorization()
            break
        @unknown default:
            fatalError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        _lat = manager.location?.coordinate.latitude ?? 0.0
        _lon = manager.location?.coordinate.longitude ?? 0.0
        delegate?(_lat,_lon)
        if isStopLocation { locationManager.stopUpdatingLocation() }
    }
    
}
