//
//  LocationHandler.swift
//  Dragon Mart Driver
//
//  Created by Tareq Safia on 7/29/19.
//  Copyright © 2019 Tareq Safia. All rights reserved.
//

import UIKit
import CoreLocation

class LocationHandler: NSObject , CLLocationManagerDelegate {
    
    static let shared = LocationHandler()
    
    //private
    private var _lat = 0.0
    private var _lon = 0.0
    private var locationManager = CLLocationManager()
    private var isStopLocation = true
    
    var didLocateCurrentLocation:((Double,Double) -> Void)?
    //MARK: Location
    func startLocation(_ isStopLocation:Bool = true){
        self.isStopLocation = isStopLocation
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse , .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .denied , .restricted:
            manager.requestWhenInUseAuthorization()
            break
        @unknown default:
            fatalError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        _lat = manager.location?.coordinate.latitude ?? 0.0
//        _lon = manager.location?.coordinate.longitude ?? 0.0
//        print("Inner => latitude : \(_lat) || longitude : \(_lon)" )
//        didLocateCurrentLocation?(_lat,_lon)
//        if isStopLocation {locationManager.stopUpdatingLocation()}
        
        guard let first = locations.first else { return }
        if isStopLocation {
            locationManager.stopUpdatingLocation()
            manager.delegate = nil
        }
        
        _lat = first.coordinate.latitude
        _lon = first.coordinate.longitude
        print("Inner => latitude : \(_lat) || longitude : \(_lon)" )
        didLocateCurrentLocation?(_lat,_lon)
    }
    
//    func calculateDistance(_ lat:Double,_ lon:Double,_ lat2:Double,_ lon2:Double) -> Double{
//        let coordinate1 = CLLocation(latitude: lat, longitude: lon)
//        let coordinate2 = CLLocation(latitude: lat2, longitude: lon2)
//        let disMeters = coordinate1.distance(from: coordinate2)
//        let distance = Double(disMeters/1000).roundTo(places: 2)
//        return distance
//    }
    
    
}
