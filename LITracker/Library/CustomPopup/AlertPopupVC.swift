//
//  AlertPopupVC.swift
//  TeamUp
//
//  Created by Tareq Safia on 1/6/20.
//  Copyright © 2020 Tareq Safia. All rights reserved.
//

import UIKit

class AlertPopupVC: UIViewController {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_message: UILabel!
    @IBOutlet weak var btn_cancel: UIButton!
    @IBOutlet weak var btn_ok: UIButton!
    
    var didPressOk:(() -> ())?
    var didPressCancel:(() -> ())?
    
    var _title:String?
    var _message:String?
    var _cancel:String?
    var _ok:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    func configure(){
        btn_ok.backgroundColor = PRIMARY_COLOR
        lbl_title.textColor = PRIMARY_COLOR
        
        lbl_title.isHidden = _title == nil
        lbl_message.isHidden = _message == nil
        btn_cancel.isHidden = _cancel == nil
        lbl_title.text = _title
        lbl_message.text = _message
        btn_cancel.setTitle(_cancel, for: .normal)
        btn_ok.setTitle(_ok, for: .normal)
        
        if _title == nil && _message != nil {
            lbl_message.textColor = .white
        }
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.didPressCancel?()
        self.dismissVC()
    }
    
    @IBAction func okAction(_ sender: UIButton) {
        self.didPressOk?()
        self.dismissVC()
    }
    
}
