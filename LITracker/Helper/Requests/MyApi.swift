//
//  MyApi.swift
//  LITtracker
//
//  Created by Mohammed on 4/29/21.
//

import Foundation
import Alamofire
import Firebase
import FirebaseAuth
import SwiftyJSON

//MARK: MainReguest and data header
class MyApi {
    
    static let api = MyApi()
    var page_size = 15
    
    func HeaderDefault(_ auth:Bool,_ override:MethodType = .NONE) -> [String:String]{
        var header_token   = ["Accept"       : "application/json",
                              "Content-Type" : "application/x-www-form-urlencoded"]
        
        if auth {
            header_token["Authorization"] = "Bearer \(Auth_User.Token)"
        }
        //        Authorization
        if override != .NONE {
            header_token["X-HTTP-Method-Override"] = override.path()
        }
        return header_token
    }
    
    
    func request(_ url:UrlPath,_ param : [String:Any]? ,_ method:HTTPMethod = .post, _ auth : Bool = true , _ methodType :MethodType = .NONE ,completion: @escaping ([String:Any]?,String,Bool,StatusCode) -> ()) {
        
        let _auth = HTTPHeaders(HeaderDefault(auth,methodType))
        print(url.path())
        print(param)
        
        AF.request(url.path() , method: method , parameters: param, encoding: URLEncoding.default, headers: _auth)
            .responseJSON { (response) in
                
                switch response.result {
                
                case .failure(let error):
                    
                    let error = error.localizedDescription ?? "Server Error".localized
                    completion(nil,error,false,.Success)
                    
                case .success(let req):
                    
                    guard let resp = req as? [String:Any] else {
                        completion(nil,"Server Error".localized,false,.Success)
                        return
                    }
                    
                    let status = resp["status"] as? Bool ?? false
                    var messageResp = resp["message"] as? String ?? ""
                    if messageResp == "" {
                        messageResp = resp["Message"] as? String ?? "Server Error".localized
                    }
                    let code = resp["statusCode"] as? Int ?? 0
                    let status_code =  StatusCode(code)
                    if !status && ( status_code == .Unauthorized) && auth {
                        if url.path() == UrlPath.RefreshToken.path() {
                            Auth_User.removeUserValues()
                            Route.goSignIn()
                            return
                        }
                        debugPrint("Unauthorized : Refresh Token")
                        self.RefreshTokenRequest(completion: { (message, status, _statusCode) in
                            if !status {
                                completion(resp,messageResp,status,status_code)
                                return
                            }
                            self.request(url, param,method,auth, methodType, completion: completion)
                        })
                    }else {
                        completion(resp,messageResp,status,status_code)
                    }
                    
                }
            }
    }
    
    
    func postRequest(_ url:UrlPath,_ param : [String:Any]? ,_ method:HTTPMethod = .post, _ auth : Bool = true , _ methodType :MethodType = .NONE ,completion: @escaping ([String:Any]?,String,Bool,StatusCode) -> ()) {
        
        let _auth = HTTPHeaders(HeaderDefault(auth,methodType))
        print(url.path())
        print(param)
        
        let urlP = URL(string: url.path())!
        var request = URLRequest(url: urlP)
        request.httpMethod = "POST"
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("Bearer \(Auth_User.Token)", forHTTPHeaderField: "Authorization")
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: param)
        
        
        
        AF.request(request)
            .responseJSON { (response) in
                
                switch response.result {
                
                case .failure(let error):
                    
                    let error = error.localizedDescription ?? "Server Error".localized
                    completion(nil,error,false,.Success)
                    
                case .success(let req):
                    
                    guard let resp = req as? [String:Any] else {
                        completion(nil,"Server Error".localized,false,.Success)
                        return
                    }
                    
                    let status = resp["status"] as? Bool ?? false
                    var messageResp = resp["message"] as? String ?? ""
                    if messageResp == "" {
                        messageResp = resp["Message"] as? String ?? "Server Error".localized
                    }
                    let code = resp["statusCode"] as? Int ?? 0
                    let status_code =  StatusCode(code)
                    if !status && ( status_code == .Unauthorized) && auth {
                        if url.path() == UrlPath.RefreshToken.path() {
                            Auth_User.removeUserValues()
                            Route.goSignIn()
                            return
                        }
                        debugPrint("Unauthorized : Refresh Token")
                        self.RefreshTokenRequest(completion: { (message, status, _statusCode) in
                            if !status {
                                completion(resp,messageResp,status,status_code)
                                return
                            }
                            self.request(url, param,method,auth, methodType, completion: completion)
                        })
                    }else {
                        completion(resp,messageResp,status,status_code)
                    }
                    
                }
            }
    }
    
}

// Upload Images
extension MyApi {
    
    static func uploadImagesWithParam(_ url:UrlPath,Parameters : [String:Any],_images: [UIImage],completion: @escaping (_ response:[String:Any], [AttachmentsSt] , _ errors: String,_ message: String,_ status: Bool) -> Void) {
        let _auth = HTTPHeaders(Constant.HeaderToken)
        
        AF.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in Parameters {
                if let arrValue = value as? [Int] {
                    for item in arrValue {
                        multipartFormData.append(("\(item)".data(using: String.Encoding.utf8))!, withName: key)
                    }
                }else if let arrValue = value as? [String]{
                    for item in arrValue {
                        multipartFormData.append(("\(item)".data(using: String.Encoding.utf8))!, withName: key)
                    }
                    
                }else if let arrValue = value as? [UIImage]{
                    for img in arrValue {
                        let data = img.jpegData(compressionQuality: 0.5)
                        let name = "\(Date().millisecondsSince1970).jpeg"
                        multipartFormData.append(data!, withName: key , fileName: name , mimeType: "image/jpeg")
                    }
                    
                }else if let url = value as? URL{
                    let name = "\(Date().millisecondsSince1970).mp4"
                    multipartFormData.append(url, withName: key, fileName: name, mimeType: "video/mp4")
                }else if let img = value as? UIImage{
                    let data = img.jpegData(compressionQuality: 0.5)
                    let name = "\(Date().millisecondsSince1970).jpeg"
                    multipartFormData.append(data!, withName: key , fileName: name , mimeType: "image/jpeg")
                    
                } else if let url = value as? NSArray {
                    url.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multipartFormData.append(string.data(using: .utf8)!, withName: keyObj)
                        } else if let num = element as? Int {
                                let value = "\(num)"
                                multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        } else if let num = element as? UIImage {
                            let value = "\(num)"
                            multipartFormData.append(value.data(using: .utf8)!, withName: keyObj)
                        } else if let url = element as? URL{
                            let name = "\(Date().millisecondsSince1970).\(url.pathExtension)"
                            print("The name is \(name)")
                            multipartFormData.append(url, withName: key)
//                            multipartFormData.append(url, withName: key, fileName: name, mimeType: "files")
                        }
                    })
                } else{
                    multipartFormData.append(("\(value)".data(using: String.Encoding.utf8))!, withName: key)
                }
            }
            
        }, to:url.path(), method: .post,headers: _auth).responseJSON(completionHandler: { (response) in
            
            switch response.result {
            case .failure(let error):
                let error = error.localizedDescription ?? "Server Error".localized
                completion(["":""],[AttachmentsSt(["":""])],error,error,false)
                break
                
            case .success(let value):
                let json = JSON(value)
                
                guard let res = json.dictionary else { return }
                
                let status  = res["status"]?.bool ?? false
                
                var messageResp = res["message"]?.string ?? ""
                if messageResp == "" {
                    messageResp = res["Message"]?.string ?? "Server Error".localized
                }
                
//                guard let items = res["items"]?.array else { return }
                var array : [AttachmentsSt] = [AttachmentsSt(["":""])]
                
                
                let items = res["items"]?.array ?? []
                print("The arrry count is \(items)")
//                guard let items = res["items"]?.dictionary else { return }
                
//                var tempArray : [AttachmentsSt] = [AttachmentsSt(["":""])]
                items.forEach({ (item) in
                    guard let itemarray = item.dictionary else { return }
                    array.append(AttachmentsSt(itemarray))
                })
//
//                let items = res["items"] as? [[String:Any]] ?? []
//                let arr = items.map { AttachmentsSt($0)}
                
                
//                let items = json["items"] as? [[String:Any]] ?? []
                print("The arrry count is \(array.count)")
                
                if status {
                    completion(res,array, "", messageResp, status)
                }else{
                    
                    completion(["":""],[AttachmentsSt(["":""])],messageResp,messageResp,status)
                }
            }
            
        })
    }
        
}

//MARK: get instruction
extension MyApi{
    
    //MARK: get instruction
    func getInstruction(completion: @escaping ([WalkthroughST],String,Bool) -> ()){
        request(.Instructions, nil, .get, false) { (resp, message, status,status_code) in
            let items = resp?["items"] as? [[String:Any]] ?? []
            let arr = items.map { WalkthroughST($0)}
            completion(arr,message, status)
        }
    }
    
    //MARK: Get FAQ
    func getFAQ(params:[String:Any],completion: @escaping ([FAQST],String,Bool,Int) -> ()){
        request(.FAQ, params, .post) { (resp, message, status,status_code) in
            if let res = resp, let items = res["items"] as? [String:Any] {
                let data = items["data"] as? [[String:Any]] ?? []
                let total_pages = items["total_pages"] as? Int ?? 0
                let notifications = data.map { FAQST($0)}
                completion(notifications,message, status,total_pages)
                return
            }
            completion([], message, status,0)
        }
    }
    
    
}

//MARK: TokenRequest && update FCM
extension MyApi{
    
    //MARK: update fcm notification
    func updateFCMRequest(_ fcm:String){
        let uid = UIDevice.current.identifierForVendor?.uuidString ?? ""
        let param = ["device_token":fcm,"device_id":uid]
        request(.RefreshFcmToken, param, .post) { (resp, message, status,status_code) in
            debugPrint("Update FCM " + message)
        }
    }
    
    //MARK: Refresh Token
    func RefreshTokenRequest(completion: @escaping (String,Bool,StatusCode) -> ()){
        let param = ["refresh_token": Auth_User.UserToken.refresh_token,
                     "grant_type":GrantType.RefreshToken.rawValue,
                     "client_id":Constant.client_id,
                     "client_secret":Constant.client_secret] as [String : Any]
        print("Refresh Token Param : \(param)")
        request(.RefreshToken, param, .post) { (resp, message, status,status_code) in
            if status {
                let _data  = resp!["items"] as? [String:Any] ?? [:]
                let _token = _data["token"] as? [String:Any] ?? [:]
                Auth_User.TokenData = _token
                Auth_User.Token = Auth_User.UserToken.access_token
            }
            completion(message, status,status_code)
        }
    }
    
}

//MARK: Login - Logout - Forgot Password
extension MyApi{
    
    //MARK: login
    func LoginRequest(_ email:String, _ password:String , completion: @escaping (String,Bool,StatusCode) -> ()){
        var FcmToken = Messaging.messaging().fcmToken ?? Auth_User.FcmToken
        if FcmToken.isEmpty {FcmToken = "FcmToken"}
        
        let param = ["email":email,"password":password,"device_token":FcmToken ,
                     "device_id":Constant.device_id,"device_type":"ios",
                     "grant_type":GrantType.Password.rawValue,"client_id":Constant.client_id,
                     "client_secret":Constant.client_secret] as [String : Any]
        
        request(.Login, param, .post , false) { (resp, message, status,status_code) in
            if let _data =  resp?["items"] as? [String:Any]{
                if let _User  = _data["user"] as? [String:Any] {
                    Auth_User.UserData = _User
                    self.updateUserInfoToFirebase()
                }
                if let _token = _data["token"] as? [String:Any]{
                    Auth_User.TokenData = _token
                    Auth_User.Token = Auth_User.UserToken.access_token
                }
            }
            
            completion(message, status,status_code)
        }
    }
    
    //MARK: Login Firebase
    func LoginFirebaseRequest(_ email:String, _ password:String , completion: @escaping (String,Bool) -> ()){
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
          if let error = error as? NSError {
            switch AuthErrorCode(rawValue: error.code) {
            case .operationNotAllowed:
              // Error: Indicates that email and password accounts are not enabled. Enable them in the Auth section of the Firebase console.
                completion(error.localizedDescription,false)
            case .userDisabled:
              // Error: The user account has been disabled by an administrator.
                completion(error.localizedDescription,false)
            case .wrongPassword:
              // Error: The password is invalid or the user does not have a password.
                completion(error.localizedDescription,false)
            case .invalidEmail:
                completion(error.localizedDescription,false)
              // Error: Indicates the email address is malformed.
            default:
                completion(error.localizedDescription,false)
                print("Error: \(error.localizedDescription)")
            }
          } else {
            print("User signs in successfully")
            let userInfo = Auth.auth().currentUser
            let email = userInfo?.email
            completion("successfully action",true)
          }
        }
    }
    
    //MARK: Forgot Password Request
    func ForgotPasswordRequest(email:String,completion: @escaping (String,Bool) -> ()){
        let params = ["email" : email]
        
        request(.ForgotPassword , params, .post,false) { (resp, message, status,status_code) in
            completion(message, status)
        }
    }
    
    //MARK:  Logout
    func LogoutRequest(completion: @escaping (String,Bool) -> ()){
        print("Constant.device_id \(Constant.device_id)")
        let param = ["device_id": Constant.device_id]
        request(.Logout, param, .post) { (resp, message, status,status_code) in
            completion(message, status)
        }
    }
    
    
    //MARK: Verify Code
    func VerifyCodeRequest(_ code:String,_ password:String , completion: @escaping (String,Bool,StatusCode) -> ()){
        
        let _user = Auth_User.UserInfo
        let param = ["user_id":_user.id, "code":code, "mobile":_user.phone,
                     "grant_type":GrantType.Password.rawValue,
                     "client_id":Constant.client_id, "client_secret":Constant.client_secret,
                     "password":password ] as [String : Any]
        
        request(.confirm_code, param, .post , false) { (resp, message, status,status_code) in
            if let _data =  resp?["items"] as? [String:Any]{
                if let _User  = _data["user"] as? [String:Any] {
                    Auth_User.UserData = _User
                    self.updateUserInfoToFirebase()
                }
                if let _token = _data["token"] as? [String:Any]{
                    Auth_User.TokenData = _token
                    Auth_User.Token = Auth_User.UserToken.access_token
                }
            }
            completion(message, status,status_code)
        }
    }
    
    //MARK: ReSend Code Request
    func ResendCodeRequest(_ phone:String,completion: @escaping (String,Bool) -> ()){
        
        let _user = Auth_User.UserInfo
        let param = ["user_id":_user.id,"mobile":phone] as [String : Any]
        
        request(.resend_code, param, .post, false) { (resp, message, status,status_code) in
            completion(message, status)
        }
    }
    
    //MARK: Enable Two Factor Auth
    func EnableTwoFactorAuth(completion: @escaping (String,Bool,StatusCode) -> ()){
        request(.activate_two_factor_auth, nil, .get, true) { (resp, message, status,status_code) in
            if let _data =  resp?["items"] as? [String:Any]{
                print("The item is \(_data)")
                
                if let _User  = _data["user"] as? [String:Any] {
                    Auth_User.UserData = _User
                    self.updateUserInfoToFirebase()
                }
            }
            completion(message, status,status_code)
        }
    }
    
    
    //MARK: Verify Code
    func DeactivateMyAccount(completion: @escaping (String,Bool,StatusCode) -> ()){
        request(.activate, nil, .get, true) { (resp, message, status,status_code) in
            let items = resp?["items"] as? [String:Any] ?? [:]
            completion(message, status, status_code)
        }
    }
    
}

//MARK: Home
extension MyApi{
    
    //MARK: get Home
    func getHome(completion: @escaping (HomeSt,[PaymentScheduleST],[PreAuthScheduleST],String,Bool) -> ()){
        request(.Home, nil, .get, true) { (resp, message, status,status_code) in
            let items = resp?["items"] as? [String:Any] ?? [:]
            let paymentSchedule = items["payment_schedule"] as? [[String:Any]] ?? []
            let pre_auth_schedule_Array = items["pre_auth_schedule"] as? [[String:Any]] ?? []
            
            let paymentScheduleData = paymentSchedule.map { PaymentScheduleST($0)}
            let preAuthScheduleData = pre_auth_schedule_Array.map { PreAuthScheduleST($0)}
            completion(HomeSt(items),paymentScheduleData,preAuthScheduleData,message, status)
        }
    }
    
    //MARK: Read Alert Request
    func ReadAlert(_ params:[String:Any], completion: @escaping (String,Bool) -> ()){
        request(.ReadAlert, params, .post) { (resp, message, status,status_code) in
            completion(message, status)
        }
    }
    
}

//MARK: Notifications
extension MyApi {
    
    //MARK: Send Notifications
    func sendNotifications(_ params:[String:Any], completion: @escaping ([LinksSt],String,Bool,Int) -> ()){
        request(.Notifications, params, .post) { (resp, message, status,status_code) in
            if let res = resp, let items = res["items"] as? [String:Any] {
                let data = items["data"] as? [[String:Any]] ?? []
                let total_pages = items["total_pages"] as? Int ?? 0
                let notifications = data.map { LinksSt($0)}
                completion(notifications,message, status,total_pages)
                return
            }
            completion([], message, status,0)
        }
    }
    
    //MARK: Delete Notifications
    func deleteNotifications(_ params:[String:Any], completion: @escaping (String,Bool) -> ()){
        request(.DeleteNotification, params, .delete) { (resp, message, status,status_code) in
            completion(message, status)
        }
    }
    
    //MARK: Unseen Notifications
    func unseenNotifications(completion: @escaping (UnseenNotification,String,Bool) -> ()){
        request(.UnseenNotification, nil, .get) { (resp, message, status,status_code) in
            let items = resp?["items"] as? [String:Any] ?? [:]
            completion(UnseenNotification(items),message, status)
        }
    }
    
    //MARK: Send Chat Notification Request
    func sendChatNotification(params:[String:Any], completion: @escaping (String,Bool) -> ()){
        request(.SendChatNotification ,params ,.post) { (resp, message, status,status_code) in
            completion(message, status)
        }
    }
    
}

//MARK: Lookup
extension MyApi {
    
    //MARK: get Lookup
    func getLookup(completion: @escaping ([TicketsST],[LinksSt],[FAQST],String,Bool) -> ()){
        request(.Lookup, nil, .get, true) { (resp, message, status,status_code) in
            let items    = resp?["items"] as? [String:Any] ?? [:]
            let tickets  = items["tickets"] as? [[String:Any]] ?? []
            let links    = items["links"] as? [[String:Any]] ?? []
            let faqs     = items["faqs"] as? [[String:Any]] ?? []
            
            let ticketsData = tickets.map { TicketsST($0)}
            let linksData   = links.map { LinksSt($0)}
            let faqsData    = faqs.map { FAQST($0)}
            
            completion(ticketsData,linksData,faqsData,message, status)
        }
    }
    
    //MARK: Update AddTicket
    func addTicket(_ params:[String:Any], completion: @escaping (String,Bool) -> ()){
        postRequest(.AddTicket, params, .post) { (resp, message, status,status_code) in
            if let res = resp, let items = res["items"] as? [String:Any] {
                completion(message, status)
                return
            }
            completion( message, status)
        }
    }
    
    //MARK: logs for tickets
    func postLog(_ params:[String:Any], completion: @escaping ([DataST],String,Bool,Int) -> ()){
        request(.Logs, params, .post) { (resp, message, status,status_code) in
            if let res = resp, let items = res["items"] as? [String:Any] {
                
                let data = items["data"] as? [[String:Any]] ?? []
                let total_pages = items["total_pages"] as? Int ?? 0
                
                let messageData = data.map { DataST($0)}
                
                print("The respons not is \(items) || \(message) || \(status) || \(total_pages)")
                completion(messageData, message, status, total_pages)
                return
            }
            completion([], message, status,0)
        }
    }
    
    //MARK: get Data Ticket
    func getDataTicket(id: Int, completion: @escaping (DataST,String,Bool) -> ()){
        request(.TicketData(id), nil, .get, true) { (resp, message, status,status_code) in
            let items = resp!["items"] as? [String:Any]
            completion(DataST(items!), message, status)
        }
    }
    
    
}

//MARK: GetProfile
extension MyApi{
    
    //MARK: get Profile
    func getProfile(completion: @escaping (UserSt,String,Bool) -> ()){
        request(.User, nil, .get, true) { (resp, message, status,status_code) in
            let items = resp?["items"] as? [String:Any] ?? [:]
            //         let arr = items.map { UserSt($0)}
            completion(UserSt(items),message, status)
        }
    }
    
    func changePassword(_ params:[String:Any], completion: @escaping (String, Bool) -> ()){
        request(.changePassword, params, .post, true) { (resp, message, status,status_code) in
            print("message \(message) \(status)")
            if status {
                completion(message, status)
            } else {
                completion(message, status)
            }
        }
    }
    
}

//MARK: UpdateUserInfoToFirebase
extension MyApi{
    
    func updateUserInfoToFirebase(){
        let user = Auth_User.UserInfo
        let id = "\(user.id)"
        let FcmToken = Messaging.messaging().fcmToken ?? Auth_User.FcmToken
        let _user  = User(UserId:id, UserImage: user.image ,
                          FullName: user.name, DeviceToken: FcmToken)
        Constant.UserNodes.child(id).setValue(_user.toAnyObject())
    }
    
}

//MARK: Get Income Expense form
extension MyApi {
    
    //MARK: get My Forms
    func getForms(completion: @escaping ([FormsSt],String,Bool) -> ()){
        request(.monthly_forms, nil, .get, true) { (resp, message, status,status_code) in
            let items = resp?["items"] as? [[String:Any]] ?? []
            let forms = items.map {FormsSt($0)}
            completion(forms,message, status)
        }
    }
    
    
    //MARK: get Income && Expense
    func getIncomeExpenseform(_ params:[String:Any], completion: @escaping ([IncomeSt],[ExpenseSt],String,Bool) -> ()){
        request(.incomeExpenseData, params, .post) { (resp, message, status,status_code) in
            if let res = resp, let items = res["items"] as? [String:Any] {
                let income = items["income"] as? [[String:Any]] ?? []
                let expense = items["expense"] as? [[String:Any]] ?? []
                let incomeArray = income.map { IncomeSt($0)}
                
                let expenseArray = expense.map { ExpenseSt($0)}
                completion(incomeArray,expenseArray,message, status)
                return
            }
            completion([],[], message, status)
        }
    }
    
    //MARK: Update income && Expense Data
    func incomeExpenseDataUpdate(_ params:[String:Any], completion: @escaping (String,Bool) -> ()){
        request(.incomeExpenseDataUpdate, params, .post) { (resp, message, status,status_code) in
            if let res = resp, let items = res["items"] as? [String:Any] {
                completion(message, status)
                return
            }
            completion( message, status)
        }
    }
    
    
}


#warning("nothing just test")
//#error("just test man ")
//FIXME: nothing
