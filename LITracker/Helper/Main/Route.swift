//
//  Route.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import SwiftyJSON

class Route: NSObject {
    
    
    //MARK: Public Style Appearance
    static func publicStyleAppearance() {
        
        //
        let GoogleApiService =  "AIzaSyA46jB1_NQSBbmRlkdLnbtJjqMptZ2J3Mk"
        
        enableDisableIQKeybaord(enable: true)
        GMSServices.provideAPIKey(GoogleApiService)
        
        
        //style
        let nav = UINavigationBar.appearance()
        nav.tintColor = BLUE_2C3758
        nav.barTintColor = .white
        nav.shadowImage = UIImage()
        nav.isTranslucent = false
        
        nav.titleTextAttributes = [.foregroundColor: BLUE_2C3758,
                                   .font: MyTools.appFont(.SemiBold, size: 17)]
        
        let backImage = "backArrow-icon".toImage.imageFlippedForRightToLeftLayoutDirection()
        nav.backIndicatorImage = backImage
        nav.backIndicatorTransitionMaskImage = backImage
        
    }
    
    static func basciStyleAppearance() {
        let nav = UINavigationBar.appearance()
        nav.barTintColor = .white
    }
    
    static func homeStyleAppearance() {
        let nav = UINavigationBar.appearance()
        nav.barTintColor = .white
    }
    
    static func enableDisableIQKeybaord(enable:Bool){
        IQKeyboardManager.shared.enable = true
    }
    
    static func startApp(){
        if Auth_User.FirstOpen {
            goWalkthough()
            return
        }
        
        if Auth_User.IsUserLogin {
            goHome()
            return
        }
        
        goSignIn()
    }
    
    
    static func setRoot(vc:UIViewController) {
        guard let window = UIApplication.shared.windows.first(where: {$0.isKeyWindow}) else {return}
        let nav = UINavigationController(rootViewController: vc)
        window.rootViewController = nav
        UIView.transition(with: window, duration: 0.3 , options: .transitionCrossDissolve,
                          animations: nil, completion: nil)
    }
    
    static func goWalkthough() {
        let vc : WalkthoughVC = AppDelegate.MainSB.instanceVC()
        setRoot(vc: vc)
    }
    
    static func goSignIn() {
        basciStyleAppearance()
        //
        let vc : SignInVC = AppDelegate.MainSB.instanceVC()
        setRoot(vc: vc)
    }
    
    static func goHome(){
        homeStyleAppearance()
        let vc : MainTabbarVC = AppDelegate.AppSB.instanceVC()
        setRoot(vc: vc)
    }
    
    static func goMyTikets(){
        homeStyleAppearance()
        let vc : MainTabbarVC = AppDelegate.AppSB.instanceVC()
        setRoot(vc: vc)
    }
    
    static func getNotificationType(_ values:[AnyHashable:Any]) -> NotificationType {
        var action : String = ""
        if let a = values["Action"] as? String {
            action = a
        }else if let a = values["gcm.notification.Action"] as? String {
            action = a
        }
        
        print("The type is \(action)")
        return NotificationType(action)
    }
    
    static func topVC() -> UIViewController?{
        return topViewController()
    }
    
    static func openSpecificScreen(_ values:[AnyHashable:Any])  {
        Auth_User.removeNotificationInfo()
        if let fullInfo = values as? [String:Any] {
            notifiObj(fullInfo: fullInfo)
        }

    }
    
    static func notifiObj(fullInfo: [String:Any]) {
        if let data = fullInfo["data"] as? String {
            
            if let dic = convertToDictionary(text: data) {
                let obj = NotifiSt(JSON(dic))
                temp_notifi = obj
                
                guard let _id = Int(obj.action_id.toString) else {return}
                Auth_User.Action_Name = NotificationType(obj.action.value)
                Auth_User.Action_Id = _id
                Auth_User.Sender_Id = obj.sender_id
                
                if did_home_load {
                    guard let root = topViewController() else {return}
                    if let topVC = topViewController() {
                        openPressedScreen(topVC)
                    }
                }
            }
        }
    }
    
    static func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func openPressedScreen(_ root:UIViewController) {
        
        let type = Auth_User.Action_Name
        let id = Auth_User.Action_Id
        let Sender_Id = Auth_User.Sender_Id
        print("The last data3 \(id)")
        guard type != .None else {return}
        switch type {
        
        case .chat:
            func_startChat(chatRoomId: id, Sender_Id: Sender_Id, isResolved: 0)
            
        case .ticket_resolved:
            if Sender_Id.isEmpty {
                func_startChat(chatRoomId: id, Sender_Id: "0", isResolved: 1)
            }else {
                func_startChat(chatRoomId: id, Sender_Id: Sender_Id, isResolved: 1)
            }
            
        case .new_alert:
            
            let contactUsVC : ContactUsAsapVC = AppDelegate.AppSB.instanceVC()
            contactUsVC.message = temp_notifi?.message_text
            contactUsVC.Action_Id = id
            print("The last data4 \(id)")
            root.pushNavVC(contactUsVC)
            
        case .payment_reminder_10_days, .payment_reminder_2_days:
            print("payment_reminder_10_days")
            let reminderVC : ReminderVC = AppDelegate.AppSB.instanceVC()
            reminderVC.message = temp_notifi?.message
            root.pushNavVC(reminderVC)

        case .user_delete, .user_deactivate:
            Auth_User.sessionExpired()
        //
        default:
            temp_notifi = nil
            break
        }
        
        Auth_User.removeNotificationInfo()
    }
    
    static func func_startChat(chatRoomId:Int, Sender_Id:String,  isResolved:Int){
        
        guard let root = topViewController() else {return}
        Constant.UserNodes.child(Sender_Id).observeSingleEvent(of: .value) { (snapshot) in
            root.hideIndicator()
            if snapshot.exists() {
                let user  = User(snapshot: snapshot)
                let chatVC : ChatViewController = AppDelegate.AppSB.instanceVC()
                chatVC.senderId             = "\(Auth_User.User_Id)" // change 1 to self.userId
                chatVC.senderDisplayName    = "\(Auth_User.UserInfo.name)"
                chatVC.chatRoomId           = chatRoomId.toString
                chatVC.reciverInfo          = user
                chatVC.isResolved           = isResolved
                root.pushNavVC(chatVC)
            }else {
                root.showOkAlert(title: "", message: Constant.UserNoAvailable)
            }
        }
        
    }
    
    static func topViewController() -> UIViewController? {
        var navigation : UINavigationController?
        if let window = UIApplication.shared.windows.first(where: {$0.isKeyWindow}) {
            if let root = window.rootViewController as? UINavigationController {
                navigation = root
            }
        }
        guard let nav = navigation else {return nil}
        ///
        if let vc  = nav.topViewController {
            if let p = vc.presentedViewController {
                return p
            }
            return vc
        }
        
        return nil
    }
    
    static func top_VC() -> UIViewController {
        if let top_vc = UIApplication.topViewController() {
            return top_vc
        }
        return UIViewController()
    }
    
}
