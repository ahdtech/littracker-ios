//
//  Auth_User.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import Foundation
import UIKit
import FirebaseAuth

struct Auth_User {
    
    //MARK: Saved Data For user
    static var IsUserLogin: Bool {
        let ud = UserDefaults.standard.value(forKey: "User_Id")
        return ud != nil ? true : false
    }
    
    static var User_Id: Int {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "User_Id") as? Int ?? 0
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "User_Id")
        }
    }
    
    static var Token: String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "Token") as? String ?? ""
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "Token")
        }
    }
    
    static var Notifi_On : Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "Notifi_On") as? Bool ?? true
        }
        set(status) {
            let ud = UserDefaults.standard
            ud.set(status, forKey: "Notifi_On")
            ud.synchronize()
        }
    }
    
    static var Password: String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "Password") as? String ?? ""
        }
        set(Password) {
            let ud = UserDefaults.standard
            ud.set(Password, forKey: "Password")
        }
    }
    
    static var FcmToken: String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "FcmToken") as? String ?? ""
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "FcmToken")
        }
    }
    
    static var FirstOpen : Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "FirstOpen") as? Bool ?? true
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "FirstOpen")
        }
    }
    
    static var isFirstLogin : Bool {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "isFirstLogin") as? Bool ?? true
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "isFirstLogin")
        }
    }
    
    
    //MARK: To User Model
    static var UserInfo : UserSt {
        get {
            return UserSt(Auth_User.UserData)
        }
    }
    
    static var UserToken : TokenSt {
        get {
            return TokenSt(Auth_User.TokenData)
        }
    }
    
    static var UserData: [String:Any] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "UserData") as? Data ?? Data()
            if let userData = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String:Any] {
                return userData
            }
            return [:]
        }
        set(token) {
            let ud = UserDefaults.standard
            let encodedData = try! NSKeyedArchiver.archivedData(withRootObject: token, requiringSecureCoding: true)
            ud.set(encodedData, forKey: "UserData")
        }
    }
    
    static var TokenData: [String:Any] {
        get {
            let ud = UserDefaults.standard
            let data = ud.value(forKey: "TokenData") as? Data ?? Data()
            
            if let userData = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [String:Any] {
                return userData
            }
            return [:]
        }
        set(token) {
            let ud = UserDefaults.standard
            let encodedData = try! NSKeyedArchiver.archivedData(withRootObject: token, requiringSecureCoding: true)
            ud.set(encodedData, forKey: "TokenData")
        }
    }
    
    static var _Language : String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "lang") as? String ?? "en"
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "lang")
            ud.synchronize()
        }
    }
    
    //MARK: logout action
    static func LogoutAction(_ vc:UIViewController){
        vc.showCustomAlert(title: LOGOUT, message: LOGOUT_MESSAGE, okTitle: LOGOUT, cancelTitle: CANCEL) { (action) in
            if action {
                guard vc.isConnectedToNetwork() else {vc.showAlertNoInternt(); return}
                vc.showIndicator()
                MyApi.api.LogoutRequest { (message, status) in
                    vc.hideIndicator()
                    if !status{vc.showToast(message);return}
                    Auth_User.removeUserValues()
                    Auth_User.removeNotificationInfo()
                    do {
                      try Auth.auth().signOut()
                    } catch {
                        vc.showToast("Sign out error \(AuthErrorCode.keychainError)")
                        return
                    }
                    Route.goSignIn()
                }
            }
        }
    }
    
    //MARK: remove data for user from userDefults
    static func removeUserValues() {
        let ud = UserDefaults.standard
        ud.removeObject(forKey: "User_Id")
        ud.removeObject(forKey: "Token")
        ud.removeObject(forKey: "UserData")
        ud.removeObject(forKey: "TokenData")
        ud.removeObject(forKey: "Notifi_On")
        ud.removeObject(forKey: "Password")
        ud.synchronize()
    }
    
    
    //MARK: notification
    static func removeNotificationInfo() {
        let ud = UserDefaults.standard
        ud.removeObject(forKey: "Action_Name")
        ud.removeObject(forKey: "Action_Id")
        ud.synchronize()
    }
    
    static func sessionExpired(){
        Auth_User.removeUserValues()
        Auth_User.removeNotificationInfo()
        Route.goWalkthough()
        disabledAccountAlert()
    }
    
    static func disabledAccountAlert() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            Route.topVC()?.showOkAlert(title: ATTENTION, message: DISABLED_ACCOUNT_MESSAGE)
        }
    }
    
    //MARK: save action name for notification
    static var Action_Name: NotificationType {
        get {
            let ud = UserDefaults.standard
            let v = ud.value(forKey: "Action_Name") as? String ??  ""
            return NotificationType(v)
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token.value, forKey: "Action_Name")
        }
    }
    
    //MARK: save action id for notification
    static var Action_Id: Int {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "Action_Id") as? Int ??  -1
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "Action_Id")
        }
    }
    
    
    //MARK: save senderID to use it in firebase to start chat
    static var Sender_Id: String {
        get {
            let ud = UserDefaults.standard
            return ud.value(forKey: "Sender_Id") as? String ??  ""
        }
        set(token) {
            let ud = UserDefaults.standard
            ud.set(token, forKey: "Sender_Id")
        }
    }
    
}
