//
//  Extension-UITextField.swift
//  LITracker
//
//  Created by Moahmmed Alefrangy on 11/16/20.
//

import Foundation
import UIKit
import TextFieldEffects

extension UITextField {
    
    //@Change placeholder color
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setLeftView(width:Int){
        let leftView = UIView()
        leftView.frame = CGRect(x: 0, y: 0, width: width, height: Int(self.frame.size.height))
        self.leftView = leftView
        self.leftViewMode = .always
    }
    
    func setRightView(width:Int) {
        let rightView = UIView()
        rightView.frame = CGRect(x: 0, y: 0, width: width, height: Int(self.frame.size.height))
        self.rightView = rightView
        self.rightViewMode = .always
    }
    
}

extension HoshiTextField {
    open override func awakeFromNib() {
        super.awakeFromNib()
        
        switch placeholderLabel.textAlignment {
        case .left, .natural:
            self.textAlignment = Language.isEnglishLang ? .left : .right
            
        case .right:
            self.textAlignment = Language.isEnglishLang ? .right : .left
        default:
            break
        }
    }
}
