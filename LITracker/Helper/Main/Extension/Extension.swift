//
//  Extension.swift
//
//
//  Created by Mohammed on 4/20/21.
//  Copyright © 2021 Mohammed. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import PopupDialog
import SDWebImage
import IQKeyboardManagerSwift


extension UINavigationBar {
    
    func setBottomBorderColor(color: UIColor, height: CGFloat) {
        let bottomBorderRect = CGRect(x: 0, y: frame.height, width: frame.width, height: height)
        let bottomBorderView = UIView(frame: bottomBorderRect)
        bottomBorderView.backgroundColor = color
        addSubview(bottomBorderView)
    }
    
    var shadow: Bool {
        get {
            return false
        }
        set {
            if newValue {
                self.layer.shadowOffset = CGSize(width: 0, height: 2)
                self.layer.shadowColor = UIColor.lightGray.cgColor
                self.layer.shadowRadius = 3
                self.layer.shadowOpacity = 0.5;
            }
        }
    }
}

extension UIColor {
    
    func colorToImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let ctx = UIGraphicsGetCurrentContext()
        self.setFill()
        ctx!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    //random color
    static func getRandomColor() -> UIColor{
        
        let randomRed:CGFloat = CGFloat(drand48())
        
        let randomGreen:CGFloat = CGFloat(drand48())
        
        let randomBlue:CGFloat = CGFloat(drand48())
        
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
}

extension UIView {
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func addDashedBorder(_ color : UIColor = UIColor.lightGray) {
        //Create a CAShapeLayer
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = 2
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [2,3]
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: 0, y: 0),
                                CGPoint(x: self.frame.width, y: 0)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }
    
    func setRounded() {
        let radius = self.frame.height / 2
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setRounded(radius:CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func roundCorners(cornerRadius: Double, corners:CACornerMask) {
        self.layer.cornerRadius = CGFloat(cornerRadius)
        self.clipsToBounds = true
        self.layer.maskedCorners = corners//[.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
    
    func removeCornerdMask() {
        DispatchQueue.main.async {
            self.layer.mask = nil
        }
    }
    
    func setBorderGray() {
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 1
    }
    
    func setBorder(width:CGFloat,color:UIColor) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], gradientOrientation orientation: GradientOrientation) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func setGradientBackground(colorTop: UIColor, colorBottom: UIColor) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorBottom.cgColor, colorTop.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradientLayer.locations = [0, 1]
        gradientLayer.frame = bounds
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    
    // OUTPUT 1
    func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 2
    }
    
    func dropShadowLigh() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: -5)
        self.layer.shadowRadius = 5
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        self.layer.borderWidth = 0.0
    }
    
    func addShadowView(color:UIColor,width:Int,height:Int,Opacity:Float,radius:CGFloat,cornerRadius:CGFloat) {
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowOpacity = Opacity
        self.layer.shadowRadius = radius
        self.layer.masksToBounds = false
        self.layer.cornerRadius = cornerRadius
    }
    
    // UIView Shadow + Radius
    func addShadowWithCornerRadius(color:UIColor,width:Int,height:Int,opacity:Float,
                                   radius:CGFloat,corner : CGFloat) {
        self.layer.cornerRadius = corner
        let shadowPath2 = UIBezierPath(rect: self.bounds)
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: width, height: height)
        self.layer.shadowOpacity = opacity
        self.layer.shadowPath = shadowPath2.cgPath
        self.layer.shadowRadius = radius
    }
    
    func setBlurView(){
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
    func toPngImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let pngImageData: Data? = image!.pngData()
        let finalImage = UIImage(data: pngImageData!)
        return finalImage!
    }
    
    func rotateView360(duration: Double = 0.4,completion: @escaping () -> ()) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
                        self.transform = self.transform.rotated(by: CGFloat.pi)
                        self.transform = self.transform.rotated(by: CGFloat.pi) })
            { finished in completion() }
    }
    
    func rotateViewDegrees(degree: CGFloat = 45  ,duration: Double = 0.3,completion: @escaping () -> ()) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
                        let radians: CGFloat = degree * (.pi / 180)
                        self.transform = self.transform.rotated(by: radians ) })
            { finished in completion() }
    }
    
    func resetRotation(duration: Double = 0.3){
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
                        self.transform = .identity }, completion: nil)
    }
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    func addGestureObserver(_ target:Any,_ selector:Selector){
        let tap = UITapGestureRecognizer(target: target, action: selector)
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
}

extension UIButton {
    
    func setUnderLine() {
        let text = self.titleLabel?.text ?? ""
        let textRange = NSMakeRange(0, text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle,
                                    value:NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        
        setAttributedTitle(attributedText, for: .normal)
    }
    
    
    var font: UIFont? {
        get {
            if let _font = titleLabel?.font {
                return _font
            }
            return nil
        }
        set {
            if let _font = newValue {
                titleLabel?.font = _font
            }
        }
    }
    
    var title : String? {
        get {
            return titleLabel?.text
        }
        set {
            setTitle(newValue, for: .normal)
        }
    }
    
    var imageNormal : UIImage? {
        get {
            return self.imageView?.image
        }
        set {
            setImage(newValue, for: .normal)
        }
    }
    
}

extension UIImageView {
    func fetchImage(_ imgURL : String,_ placeHolder:UIImage? = nil) {
        if let imgURL = URL.init(string: imgURL) {
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.sd_setImage(with: imgURL, placeholderImage: placeHolder)
        }else{
            self.image = placeHolder
        }
    }
}


extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
    
    
    func scrollToRight(animated:Bool=true) {
        let desiredOffset = CGPoint(x: contentSize.width / 1.408, y: 0)
        setContentOffset(desiredOffset, animated: animated)
    }
    
    func addContentSizeObserver(_ observer:NSObject){
        addObserver(observer, forKeyPath: CONTENT_SIZE, options: .new, context: nil)
    }
}

extension UIViewController {
    
    
    //MARK: close app
    func addCloseAppButton(){
        let backImage:UIImage = "leftArrow-icon".toImage.imageFlippedForRightToLeftLayoutDirection()
        let back = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(closeApp))
        navigationItem.leftBarButtonItem = back
    }
    
    @objc func closeApp(){
        UIApplication.shared.perform(#selector(NSXPCConnection.suspend))
        Thread.sleep(forTimeInterval: 0.5)
        exit(0)
    }
    
    
    //MARK: messages alerts / toasts
    func showToast(_ message:String ,_ duration:TTGSnackbarDuration = .semiMiddle, backgroundColor : UIColor = PRIMARY_COLOR) {
        let snackbar: TTGSnackbar = TTGSnackbar(message: message, duration: duration)
        
        // Change the content padding inset
        snackbar.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 8)
        
        // Change margin
        snackbar.leftMargin = 0
        snackbar.rightMargin = 0
        snackbar.bottomMargin = 0
        snackbar.radius = 0
        
        // Change message text font and color
        snackbar.backgroundColor = backgroundColor
        snackbar.messageTextColor = .white
        snackbar.messageTextFont = MyTools.appFont(size: 14)
        
        // Change animation duration
        snackbar.animationDuration = 0.5
        
        // Animation type
        snackbar.animationType = .slideFromBottomBackToBottom
        
        //snackbar.action
        snackbar.show()
    }
    
    func showMessageAlert(_ message:String) {
        showCustomAlert(message: message)
    }
    
    func showOkAlert(title:String, message:String){
        showCustomAlert(title: title, message: message, okTitle: OK_TITLE, cancelTitle: nil, completion: nil)
    }
    
    func showOkAlertWithComp(title:String?=nil, message:String,completion:((Bool) -> Void)? = nil) {
        showCustomAlert(title: title, message: message, okTitle: OK_TITLE, cancelTitle: nil, completion: completion)
    }
    
    func showCustomAlert(title:String?=nil,message:String?,okTitle:String=OK_TITLE,cancelTitle:String?=nil,completion:((Bool) -> Void)? = nil) {
        
        let width = self.view.frame.size.width
        let popup = PopupDialog(title: title, message: message ,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                preferredWidth: width,
                                tapGestureDismissal: false,
                                panGestureDismissal: false, hideStatusBar: false) {}
        
        let dialogAppearance = PopupDialogDefaultView.appearance()
        let align =  NSTextAlignment.center
        dialogAppearance.backgroundColor      = .white
        dialogAppearance.titleFont            = MyTools.appFont(size: 17)
        dialogAppearance.titleColor           = PRIMARY_COLOR
        dialogAppearance.titleTextAlignment   = align
        dialogAppearance.messageFont          = MyTools.appFont(size: 15)
        dialogAppearance.messageColor         = "9B9B9B".color
        dialogAppearance.messageTextAlignment = align
        
        
        let okButton = DefaultButton(title: okTitle) {
            completion?(true)
            popup.dismiss()
        }
        okButton.titleColor = PRIMARY_COLOR
        okButton.titleFont = MyTools.appFont(size: 16)
        
        if let cancel = cancelTitle {
            let cancelButton = CancelButton(title: cancel) {
                completion?(false)
                popup.dismiss()
            }
            cancelButton.titleColor = .red
            cancelButton.titleFont = MyTools.appFont(size: 16)
            
            popup.addButtons([okButton,cancelButton])
            
        }else {
            popup.addButton(okButton)
        }
        
        self.present(popup, animated: true, completion: nil)
    }
    
    func presentPopupVC(vc: UIViewController,tapDismissal: Bool = false,panDismissal: Bool = false) {
        let width = self.view.frame.size.width
        let popup = PopupDialog(viewController: vc,
                                buttonAlignment: .horizontal,
                                transitionStyle: .zoomIn,
                                preferredWidth: width,
                                tapGestureDismissal: tapDismissal,
                                panGestureDismissal: panDismissal,
                                hideStatusBar: false,
                                completion: nil)
        popup.view.backgroundColor = .clear
        self.present(popup, animated: true, completion: nil)
    }
    
    
    //simple alert
    func showAlertNoInternt() {
        showCustomAlert(message: NO_INTERNET_MESSAGE)
    }
    
    func showAlertNoInternt(completion:@escaping () -> Void) {
        showCustomAlert(title: nil, message: NO_INTERNET_MESSAGE, okTitle: RETRY, cancelTitle: BACK_TITLE) { (status) in
            if status {completion()}
        }
    }
    
    func showToastNoInternt() {
        //        showToast(NO_INTERNET_MESSAGE)
    }
    
    
    func showIndicator(message : String = PLEASE_WAIT) {
        SKActivityIndicator.statusLabelFont(MyTools.appFont(size: 14))
        SKActivityIndicator.spinnerStyle(.spinningCircle)//spinningFadeCircle)
        SKActivityIndicator.spinnerColor(PRIMARY_COLOR)
        SKActivityIndicator.show(message, userInteractionStatus: false)
    }
    
    func hideIndicator() {
        SKActivityIndicator.dismiss()
    }
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func hideBackWord()  {
        navigationItem.hideBackWord()
    }
    
    func popVC(_ animated:Bool = true) {
        self.navigationController?.pop(animated: animated)
    }
    
    func popToVC(_ vc:UIViewController ,_ animated:Bool = true) {
        self.navigationController!.popToViewController(vc, animated: animated)
    }
    
    func popToSpecificVC(vc:UIViewController.Type ,_ animated:Bool = true) {
        for _vc in self.navigationController!.viewControllers as Array {
            if _vc.isKind(of: vc) {
                self.navigationController!.popToViewController(_vc, animated: animated)
                break
            }
        }
    }
    
    func removeVCsFromNav(vc:[UIViewController.Type]) {
        if var vcs = navigationController?.viewControllers {
            for _vc in vc {
                vcs.removeAll(where: {$0.isKind(of: _vc)})
            }
            navigationController?.viewControllers = vcs
        }
    }
    
    func removeVCsAddOrder() {
        if let vcs = navigationController?.viewControllers {
            guard let first = vcs.first, let last = vcs.last else {return}
            navigationController?.viewControllers = [first, last]
        }
    }
    
    
    func replaceCurrentVC(_ vc:UIViewController){
        if let nav = navigationController {
            var vcs = nav.viewControllers
            vcs.removeLast();vcs.append(vc)
            nav.viewControllers = vcs
        }
    }
    
    func popToRoot(_ animated:Bool = true) {
        self.navigationController?.popToRoot(animated: animated)
    }
    
    func pushNavVC(_ vc:UIViewController,_ animated:Bool = true) {
        self.navigationController?.pushViewController(vc, animated: animated)
    }
    
    func setVCs(_ vcs:[UIViewController],_ animated:Bool = true) {
        self.navigationController?.setViewControllers(vcs, animated: animated)
    }
    
    func presentVC(_ vc:UIViewController,_ animated:Bool = true) {
        self.present(vc, animated: animated, completion: nil)
    }
    
    func presentModal(_ vc:UIViewController,_ animated:Bool = true) {
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .custom
        self.present(vc, animated: animated, completion: nil)
    }
    
    func presentCustom(_ vc:UIViewController,_ animated:Bool = true) {
        vc.modalPresentationStyle = .custom
        self.present(vc, animated: animated, completion: nil)
    }
    
    func  pushManyVCs(_ vcs:[UIViewController],_ animated:Bool = true) {
        if let nav = navigationController {
            var nvcs = nav.viewControllers
            nvcs.append(contentsOf: vcs)
            nav.setViewControllers(nvcs, animated: true)
        }
    }
    
    func dismissVC(_ animated:Bool = true,_ completion: (() -> Void)? = nil) {
        self.dismiss(animated: animated, completion: completion)
    }
    
    func dismissNav(_ animated:Bool = true,_ completion: (() -> Void)? = nil) {
        self.navigationController?.dismiss(animated: animated, completion: completion)
    }
    
    func dismissAllVC(_ animated:Bool = true,_ completion: (() -> Void)? = nil) {
        if let keyWindow = UIWindow.key {
            keyWindow.rootViewController?.dismiss(animated: animated, completion: completion)
        }
    }
    
    func setNavigationBarHidden(_ value:Bool=true) {
        self.navigationController?.setNavigationBarHidden(value, animated: true)
    }
    
    func enableLargeNavTitle(_ value:Bool = true) {
        self.navigationController?.navigationBar.prefersLargeTitles = value
    }
    
    func ShowHideIQKeyboardPlaceHolder(_ show:Bool = true) {
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = show
    }
    
    
    func setLayoutAnimation(_ duartion:TimeInterval = 0.3){
        UIView.animate(withDuration: duartion) {
            self.view.layoutIfNeeded()
        }
    }
    
    func setLayoutAnimationVibration(_ duartion:TimeInterval = 0.4){
        UIView.animate(withDuration: duartion, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    //MARK: show login alert
    func showLoginAlert(){
        self.showCustomAlert(title: ATTENTION, message: "YOU_MUST_LOGIN_CONTINUE", okTitle: LOGIN, cancelTitle: CANCEL_TITLE){ (action) in
            guard action else{return}
            //                Route.goIntro()
        }
    }
    
    func customBack(name: String) {
        let yourBackImage = name.toImage
        navigationController?.navigationBar.backIndicatorImage = yourBackImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
    }
    
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}


extension URL {
    
    var fileName : String {
        let strUrl = self.absoluteString
        return (strUrl as NSString).lastPathComponent
    }
}

//MARK: alert controller
extension UIAlertController {
    
    func addCustomAction(_ title:String ,_ completion: ((UIAlertAction) -> Void)? = nil){
        self.addAction(UIAlertAction(title: title, style: .default, handler: { (alert) in
            completion?(alert)
        }))
    }
    
    func addDestructiveAction(_ title:String ,_ completion: ((UIAlertAction) -> Void)? = nil){
        self.addAction(UIAlertAction(title: title, style: .destructive, handler: { (alert) in
            completion?(alert)
        }))
    }
    
    
    func addCancelAction(_ title:String,_ completion: ((UIAlertAction) -> Void)? = nil){
        self.addAction(UIAlertAction(title: title, style: .cancel, handler: { (alert) in
            completion?(alert)
        }))
    }
    
    
    func supportIpad(_ view:UIView){
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            // Ipad
            self.popoverPresentationController?.sourceView = view
            self.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        }
    }
    
    func addSourceView(_ source:UIView){
        let frame = source.frame.origin
        self.popoverPresentationController?.sourceRect = CGRect(x: frame.x, y: frame.y, width: 200, height: 100)
        self.popoverPresentationController?.sourceView = source
    }
}

extension UIStoryboard {
    func instanceVC<T: UIViewController>() -> T {
        guard let vc = instantiateViewController(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return vc
    }
}

extension UITableView {
    func dequeueTVCell<T: UITableViewCell>() -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: T.self)) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return cell
    }
    
    func registerCell(id: String) {
        self.register(UINib(nibName: id, bundle: nil), forCellReuseIdentifier: id)
    }
    
    func registerCell(id: UITableViewCell.Type) {
        let _id = String(describing: id)
        self.register(UINib(nibName: _id, bundle: nil), forCellReuseIdentifier: _id)
    }
    
}

extension UICollectionView {
    func dequeueCVCell<T: UICollectionViewCell>(indexPath:IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: String(describing: T.self), for: indexPath) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return cell
    }
    
    func dequeueCRView<T: UICollectionReusableView>(kind:CollectionKind,indexPath:IndexPath) -> T {
        let _id = String(describing: T.self)
        guard let cell = dequeueReusableSupplementaryView(ofKind: kind.value, withReuseIdentifier: _id, for: indexPath) as? T else {
            fatalError("Could not locate viewcontroller with with identifier \(String(describing: T.self)) in storyboard.")
        }
        return cell
    }
    
    
    func registerCell(id: String) {
        self.register(UINib(nibName: id, bundle: nil), forCellWithReuseIdentifier: id)
    }
    
    func registerCell(id: UICollectionViewCell.Type) {
        let _id = String(describing: id)
        self.register(UINib(nibName: _id, bundle: nil), forCellWithReuseIdentifier: _id)
    }
    
    func registerRView(id: UICollectionReusableView.Type,kind:CollectionKind) {
        let _id = String(describing: id)
        self.register(UINib(nibName: _id, bundle: nil), forSupplementaryViewOfKind: kind.value, withReuseIdentifier: _id)
    }
    
    func scrollToNearestVisibleCollectionViewCell(){
        self.decelerationRate = UIScrollView.DecelerationRate.fast
        let visibleCenterPositionOfScrollView = Float(self.contentOffset.x + (self.bounds.size.width / 2))
        var closestCellIndex = -1
        var closestDistance: Float = .greatestFiniteMagnitude
        for i in 0..<self.visibleCells.count {
            let cell = self.visibleCells[i]
            let cellWidth = cell.bounds.size.width
            let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)
            
            // Now calculate closest cell
            let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
            if distance < closestDistance {
                closestDistance = distance
                closestCellIndex = self.indexPath(for: cell)!.row
            }
        }
        if closestCellIndex != -1 {
            self.scrollToItem(at: IndexPath(row: closestCellIndex, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    var currentIndex: IndexPath? {
        let visibleRect = CGRect(origin: self.contentOffset, size: self.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        return indexPathForItem(at: visiblePoint)
    }
    
    func scrollToIndex(index:Int = 0){
        let indexPath = IndexPath(item: index, section: 0)
        scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}


enum CollectionKind {
    case Header
    case Footer
    var value : String {
        switch self {
        case .Header:
            return "UICollectionElementKindSectionHeader"
        default:
            return "UICollectionElementKindSectionFooter"
        }
    }
}

extension Int {
    
    var toString: String {
        return "\(self)"
    }
    
    var toTwoDigitString: String {
        return String.init(format: "%0.02d", self)
    }
    
    
    var toDouble: Double {
        return Double(self)
    }
    
    
    var isZero: Bool {
        return self == 0
    }
    
    var styleFormat2 : String {
        return  String(format: "%.3f", self).dropLast().description
    }
    
}

extension Double {
    
    func rounded(toPlaces places:Int) -> Double {
        return Double(Int((pow(10, Double(places)) * self).rounded(.towardZero))) / pow(10, Double(places))
    }
    
    var to100Percent:CGFloat {
        return CGFloat(self.roundTo(places: 2) / 5)
    }
    
    var to10PercentString: String {
        return "\((self.roundTo(places: 2) / 5) * 10)"
    }
    
    var toString: String {
        return "\(self.roundTo(places: 2))"
    }
    
    var toStringRound0: String {
        return "\(self.roundTo(places: 0))"
    }
    
    var styleFormat2 : String {
        return  String(format: "%.3f", self).dropLast().description
    }
}


extension String {
    
    func format(args: CVarArg...) -> String {
        return String(format: self, arguments: args)
    }
    
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    var containsWhitespace : Bool {
        return (self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
    
    var containtSpecialCharacter : Bool {
        let characterset = CharacterSet(charactersIn: "!@#$%^&*()=<>?/\';~`٫±§")
        //"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_")
        return self.rangeOfCharacter(from: characterset) != nil ?  true : false
    }
    
    var validUserName : Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_")
        return rangeOfCharacter(from: characterset) != nil
    }
    
    var validEmailChar : Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_@")
        return rangeOfCharacter(from: characterset) != nil
    }
    
    
    //cut first caracters from full names
    public func getAcronyms(separator: String = "") -> String {
        let acronyms = self.components(separatedBy: " ").map({ String($0.first!) }).joined(separator: separator);
        return acronyms;
    }
    
    //remove spaces from text
    var trimmed:String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var prepareFilterFirstString : String {
        return self.trimmed.first?.description ?? ""
    }
    
    var isEmailValid: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }
    
    var isMobileQAValid: (status:Bool, message:String) {
        guard self.isNumeric else {return (status:false, message: INVALID_MOBILENUMBER)}
        if hasPrefix("0") {return (status:false, message:PHONENUMBER_NOT_START_ZERO)}
        let count =  self.trimmed.count == 10 || self.trimmed.count == 9
        if count {return (status:true, message:"")}
        return (status:false, message:INVALID_MOBILENUMBER)
    }
    
    var isNumeric: Bool {
        return !(self.isEmpty) && self.allSatisfy { $0.isNumber }
    }
    
    var isMobileValid: Bool {
        do {
            let regStr2 =  "^(1)?[0-9]{3}?[0-9]{3}?[0-9]{4}$"
            let regex = try NSRegularExpression(pattern: regStr2, options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
        } catch {
            return false
        }
    }
    
    var isUrlValid : Bool {
        if let url = URL.init(string: self){
            return UIApplication.shared.canOpenURL(url)
        }
        return false
    }
    
    var isPdfFile : Bool {
        return lowercased().hasSuffix("pdf")
    }
    
    var isEmptyStr:Bool{
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    var color: UIColor {
        let hex = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        guard let int = Scanner(string: hex).scanInt32(representation: .hexadecimal) else { return #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) }
        
        let a, r, g, b: Int32
        switch hex.count {
        case 3:     (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)  // RGB (12-bit)
        case 6:     (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)                    // RGB (24-bit)
        case 8:     (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)       // ARGB (32-bit)
        default:    (a, r, g, b) = (255, 0, 0, 0)
        }
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(a) / 255.0)
    }
    
    var cgColor: CGColor {
        return self.color.cgColor
    }
    
    var TrimAllSpaces:String {
        return self.components(separatedBy: .whitespacesAndNewlines).joined()
    }
    
    var TrimWhiteSpaces:String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    
    var enhanceNewLine:String {
        var str = self.replacingOccurrences(of: "\\n", with: "\n")
        str  = str.replacingOccurrences(of: "\r", with: "")
        str  = str.replacingOccurrences(of: "&amp;", with: "and")
        str  = str.replacingOccurrences(of: "<p>", with: "")
        str  = str.replacingOccurrences(of: "</p>", with: "")
        return str.replacingOccurrences(of: "\\'", with: "'")
    }
    
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func width(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
    }
    
    func stringSize()-> CGSize {
        let size  = (self as NSString).size(withAttributes: nil)
        return size
    }
    
    var removeHtmlTags:String {
        //        return   self.replacingOccurrences(of: "<div*></div>", with: "")
        let regex:NSRegularExpression  = try! NSRegularExpression(  pattern: "<.*?>", options: .caseInsensitive)
        let range = NSMakeRange(0, self.count)
        let htmlLessString :String = regex.stringByReplacingMatches(in: self, options: NSRegularExpression.MatchingOptions(), range:range , withTemplate: "")
        
        return htmlLessString.replacingOccurrences(of: "&amp;", with: "")
    }
    
    var toImage: UIImage {
        if self == "" {
            return UIImage()
        }else{
            let img = UIImage(named: self) ?? UIImage()
            return img
        }
    }
    
    func stringToImage() -> UIImage? {
            if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters){
                return UIImage(data: data)
            }
            return nil
        }
    
    
    func func_ArNumberToEn() -> String {
        let Formatter: NumberFormatter = NumberFormatter()
        Formatter.locale = NSLocale(localeIdentifier: "EN") as Locale?
        let finaltxtNo = Formatter.number(from: self)
        return "00\(finaltxtNo!)"
    }
    
    func func_TrimCharacters() -> String {
        let result = self.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890.").inverted)
        return  "\(result)"
    }
    
    func replacePlusInCountryCode() -> String {
        let result = self.replacingOccurrences(of: "+", with: "00")
        return  "\(result)"
    }
    
    func replaceZeroInCountryCode() -> String {
        let result = self.replacingOccurrences(of: "00", with: "+")
        return  "\(result)"
    }
    
    var func_replaceZerosInCountruCode : String {
        let result = self.dropFirst(2).description
        return  "+\(result)"
    }
    
    func replacingCharacter(newStr:String,range:NSRange) -> String {
        let str = NSString(string: self).replacingCharacters(in: range, with : newStr) as NSString
        return String(str)
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func func_setUpFontawsomeIconOld() -> String {
        let icon = self.dropFirst(6).description.capitalizingFirstLetter()
        return icon
    }
    func func_setUpFontawsomeIcon() -> String {
        let icon = self.dropFirst(3).description
        return icon
    }
    
    func toDateUTC(_ defaultFormat: String = "yyyy-MM-dd HH:mm:ss") -> Date {
        
        let toDate = DateFormatter()
        toDate.dateFormat = defaultFormat
        toDate.locale = Locale(identifier: "en")
        toDate.timeZone = TimeZone(identifier: "UTC")
        let newDate = toDate.date(from: self)
        return newDate ?? Date()
    }
    

    func toDate(_ defaultFormat: String = "dd-MM-yyyy hh:mm:ss") -> Date {
        
        let toDate = DateFormatter()
        toDate.dateFormat = defaultFormat
        toDate.locale = Locale(identifier: "en")
        let newDate = toDate.date(from: self)
        return newDate ?? Date()
    }
    
    
    var addPercentageValue : String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    
    func fromJSON() -> [String: Any] {
        let data = self.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String: Any]
            {
                return jsonArray // use the json here
            } else {
                return [:]
            }
        } catch _ as NSError {
            return [:]
        }
    }
    
    var containUnnecessaryChar : Bool {
        let characterset = CharacterSet(charactersIn: "0123456789.").inverted
        return self.rangeOfCharacter(from: characterset) != nil ?  true : false
    }
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var fileUrlName : String {
        return (self as NSString).lastPathComponent
    }
    
    func fileExtension() -> String {
           return URL(fileURLWithPath: self).pathExtension
    }
    
    func isContain(_ str:String) -> Bool{
        let _self = self.lowercased()
        let other = str.lowercased()
        return _self.contains(other) || _self.localizedCaseInsensitiveContains(other)
    }
    
    var styleFormat2 : String {
        return  String(format: "%.3f", self).dropLast().description
    }
}

extension Bool {
    var intValue : Int {
        return self ? 1 : 0
    }
}

extension UINavigationController {
    func pop(animated: Bool) {
        _ = self.popViewController(animated: animated)
    }
    
    func popToRoot(animated: Bool) {
        _ = self.popToRootViewController(animated: animated)
    }
}

extension UINavigationItem
{
    func hideBackWord()  {
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.backBarButtonItem = backItem
    }
}

extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    var seconds:Int {
        return Int((self.timeIntervalSince1970).rounded())
    }
    
    var millisecondNo1000:Int {
        return Int((self.timeIntervalSince1970).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    
    init(milliNo1000:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliNo1000))
    }
    
    init(seconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(seconds))
    }
    
    var nextDay : Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    
    var startDate : Date {
        return  self.toString("yyyy-MM-dd").toDate("yyyy-MM-dd")
    }
    
    var previousDay:Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    
    func nextDays(_ day:Int = 1) -> Date {
        return Calendar.current.date(byAdding: .day, value: day, to: self)!
    }
    
    func previousDays(_ day:Int = -1) -> Date {
        let _day = day > 0 ? (day * -1) : day
        return Calendar.current.date(byAdding: .day, value: _day, to: self)!
    }
    
    func previousYaers(_ year:Int = -1) -> Date {
        let _year = year > 0 ? (year * -1) : year
        return Calendar.current.date(byAdding: .year, value: _year, to: self)!
    }
    
    func isBetween(_ start: Date,_ end: Date) -> Bool {
        return start.compare(self).rawValue * self.compare(end).rawValue >= 0
    }
    
    func addMin(_ value:Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: value, to: self)!
    }
    
    func addHour(_ value:Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: value, to: self)!
    }
    
    func monthAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MMM")
        return df.string(from: self)
    }
    
    func toString(_ formater:String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en")
        dateFormatter.dateFormat = formater
        return dateFormatter.string(from: self)
    }
    
    func toStringUTC(_ formater:String = "yyyy-MM-dd HH:mm:ss") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en")
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.dateFormat = formater
        return dateFormatter.string(from: self)
    }
    
    func resetTime() -> Date{
        return self.toString("yyyy-MM-dd 00:00:00").toDate()
    }
    
    func resetMinSeconds() -> Date{
        return self.toString("yyyy-MM-dd HH:00:00").toDate()
    }
    
    func resetSeconds() -> Date{
        return self.toString("yyyy-MM-dd HH:mm:00").toDate()
    }
    
}

extension UIActivityViewController {
    func supportIpad(_ view:UIView){
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad){
            // Ipad
            self.popoverPresentationController?.sourceView = view
            self.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection()
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
    class func topVC(base: UIViewController? = UIApplication.shared.windows.first(where: {$0.isKeyWindow})?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topVC(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topVC(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topVC(base: presented)
        }
        return base
    }
        
        class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            if let nav = base as? UINavigationController {
                return topViewController(base: nav.visibleViewController)
            }
            if let tab = base as? UITabBarController {
                if let selected = tab.selectedViewController {
                    return topViewController(base: selected)
                }
            }
            if let presented = base?.presentedViewController {
                return topViewController(base: presented)
            }
            return base
        }
    
}


extension UIWindow {
    
    func capture() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(self.frame.size, self.isOpaque, UIScreen.main.scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.windows.first { $0.isKeyWindow }
        } else {
            return UIApplication.shared.keyWindow
        }
    }
    
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func resizeImageWith(newSize: CGSize) -> UIImage {
        
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: image!.cgImage!)
    }
    
    var toBase64 : String {
        let data = self.jpegData(compressionQuality: 0.4)
        return data?.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0)) ?? ""
    }
    
    func func_updateImageOrientionUpSide() -> UIImage? {
        if self.imageOrientation == .up { return self }
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        }
        UIGraphicsEndImageContext()
        return nil
    }
    
    class func imageWithColor(color:UIColor, size:CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        if context == nil {
            return nil
        }
        color.set()
        context?.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    class func verticalAppendedTotalImageSizeFromImagesArray(imagesArray:[UIImage]) -> CGSize {
        var totalSize = CGSize.zero
        for im in imagesArray {
            let imSize = im.size
            totalSize.height += imSize.height
            totalSize.width = max(totalSize.width, imSize.width)
        }
        return totalSize
    }
    
    
    class func verticalImageFromArray(imagesArray:[UIImage]) -> UIImage? {
        
        var unifiedImage:UIImage?
        let totalImageSize = self.verticalAppendedTotalImageSizeFromImagesArray(imagesArray: imagesArray)
        
        UIGraphicsBeginImageContextWithOptions(totalImageSize,false, 0)
        
        var imageOffsetFactor:CGFloat = 0
        
        for img in imagesArray {
            img.draw(at: CGPoint(x: 0, y: imageOffsetFactor))
            imageOffsetFactor += img.size.height;
        }
        unifiedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return unifiedImage
    }
    
    func toPngString() -> String? {
            let data = self.pngData()
            return data?.base64EncodedString(options: .endLineWithLineFeed)
        }
      
        func toJpegString(compressionQuality cq: CGFloat) -> String? {
            let data = self.jpegData(compressionQuality: cq)
            return data?.base64EncodedString(options: .endLineWithLineFeed)
        }
    
}


// Set Gradient To Any View

typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        get { return points.startPoint }
    }
    
    var endPoint : CGPoint {
        get { return points.endPoint }
    }
    
    var points : GradientPoints {
        get {
            switch(self) {
            case .topRightBottomLeft:
                return (CGPoint.init(x: 0.0,y: 1.0), CGPoint.init(x: 1.0,y: 0.0))
            case .topLeftBottomRight:
                return (CGPoint.init(x: 0.0,y: 0.0), CGPoint.init(x: 1,y: 1))
            case .horizontal:
                return (CGPoint.init(x: 0.0,y: 0.5), CGPoint.init(x: 1.0,y: 0.5))
            case .vertical:
                return (CGPoint.init(x: 0.0,y: 0.0), CGPoint.init(x: 0.0,y: 1.0))
            }
        }
    }
}

extension CGFloat {
    
    func roundTo(places:Int) -> CGFloat {
        return CGFloat(Double(self).roundTo(places: places))
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    mutating func roundToPlaces(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
    
    func formatPoints() -> String {
        
        var thousandNum = self/1000
        var millionNum = self/1000000
        if self >= 1000 && self < 1000000{
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))k")
            }
            return("\(thousandNum.roundToPlaces(places: 1))k")
        }
        if self > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))k")
            }
            return ("\(millionNum.roundToPlaces(places: 1))M")
        }
        else{
            if(floor(self) == self){
                return ("\(Int(self))")
            }
            return ("\(self)")
        }
    }
}

extension UILabel{
    
    func setUnderLine() {
        let textRange = NSMakeRange(0, self.text!.count)
        let attributedText = NSMutableAttributedString(string: self.text!)
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle,
                                    value:NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        
        self.attributedText = attributedText
    }
    
    func setParagraphLine(_ text:String,_ space:CGFloat=8) {
        let parag = NSMutableParagraphStyle()
        parag.lineSpacing = space
        let attributedText = NSMutableAttributedString(string: text,attributes: [.paragraphStyle : parag])
        self.attributedText = attributedText
    }
    
    func getNumberOfLine() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font ?? UIFont.systemFont(ofSize: 14)], context: nil)
        let linesRoundedUp = Int(ceil(textSize.height/charSize))
        return linesRoundedUp
    }
    
    func colorString(text: String?, coloredText: String?, color: UIColor? = PRIMARY_COLOR) {
        let attributedString = NSMutableAttributedString(string: text!)
        let range = (text! as NSString).range(of: coloredText!)
        attributedString.setAttributes([NSAttributedString.Key.foregroundColor: color!],
                                       range: range)
        self.attributedText = attributedString
    }
    
    
}

    extension UIDevice {
        var has_notch: Bool {
            let bottom =  UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        }
    }

extension UIScreen {
    var sizeType: SizeType {
        switch nativeBounds.height {
        case 960:
            return .iPhones4
        case 1136:
            return .iPhones5_SE
        case 1334:
            return .iPhones678
        case 1792:
            return .iPhone_XR
        case 1920, 2208:
            return .iPhones_678Plus
        case 2436:
            return .iPhones_X_XS
        case 2688:
            return .iPhone_XSMax
        default:
            return .unknown
        }
    }
    
    enum SizeType: String {
        case iPhones4 = "iPhone 4 or iPhone 4S"
        case iPhones5_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones678 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_678Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhones_X_XS = "iPhone X or iPhone XS"
        case iPhone_XR = "iPhone XR"
        case iPhone_XSMax = "iPhone XS Max"
        case unknown
    }
}

extension UILabel {
    func setStrikethroughStyle(text:String) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:text)
        let range = NSMakeRange(0, attributeString.length)
        attributeString.addAttribute(.strikethroughStyle, value: 1, range: range)
        attributeString.addAttribute(.strikethroughColor, value: "505050".color, range: range)
        attributedText = attributeString
    }
    
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
}

extension UIRefreshControl {
    func initRefresh() {
        self.backgroundColor = .clear//"F3F3F3".color
        self.tintColor = PRIMARY_COLOR
        let title = REFRESH
        let font = MyTools.appFont(.Regular,size: 14)
        let att : [NSAttributedString.Key : Any]  = [.font: font,
                                                     .foregroundColor: PRIMARY_COLOR]
        let attStr = NSAttributedString(string: title, attributes: att)
        self.attributedTitle = attStr;
    }
}

extension Array where Element : Equatable {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
    
    func allEqual() -> Bool {
        if let firstElem = first {
            return !dropFirst().contains { $0 != firstElem }
        }
        return true
    }
}

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension BidirectionalCollection where Element: StringProtocol {
    var sentence: String {
        count <= 2 ?
            joined(separator: " and ") :
            dropLast().joined(separator: ", ") + ", and " + last!
    }
}
