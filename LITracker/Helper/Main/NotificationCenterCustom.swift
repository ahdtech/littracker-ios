//
//  NotificationCenterCustom.swift
//  LITtracker
//
//  Created by Mohammed on 5/6/21.
//

import Foundation

import UIKit

extension UIViewController {

    func addNotifcaionObserver(_ name:NSNotification.Name,_ selector:Selector){
        NotificationCenter.default.addObserver(self, selector: selector, name: name , object: nil)
    }
    
    func postNotifcationCenter(_ name:NSNotification.Name,_ object:Any?=nil){
        NotificationCenter.default.post(name: name, object: object)
    }
}

extension NSNotification.Name {
    
    static var UpdateMessages            = NSNotification.Name("UpdateMessages")
    static var UpdateChatViewController  = NSNotification.Name("UpdateChatViewController")
    
    
}

