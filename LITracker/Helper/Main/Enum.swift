//
//  Enum.swift
//  LITtracker
//
//  Created by Mohammed on 4/20/21.
//

import Foundation
import UIKit

enum FontType : String {
    case Regular  = "Muli"
    case SemiBold = "Muli-SemiBold"
    case Light    = "Muli-Light"
    case Bold     = "Muli-Bold"
}

enum StatusCode : Int {
    case Success          = 200
    case Unauthorized     = 401
    case NotVerifyCode    = 405
    case NotVerifyEmail   = 406
    case DisbledAccount   = 407
    case UserInputError   = 422
    case ServerError      = 500
    case VerifyByAdmin    = 415
    case SocailRegister   = 416
    
    
    init (_ type:Int) {
        switch type {
        case 401:
            self = .Unauthorized
        case 405 :
            self = .NotVerifyCode
        case 406 :
            self = .NotVerifyEmail
        case 407 :
            self = .DisbledAccount
        case 422 :
            self = .UserInputError
        case 500 :
            self = .ServerError
        case 415 :
            self = .VerifyByAdmin
        case 416 :
            self = .SocailRegister
        default:
            self = .Success
        }
    }
}

enum GrantType : String {
    case RefreshToken   = "refresh_token"
    case Password = "password"
    
    init (_ type:String) {
        switch type {
        case "password":
            self =  .Password
        default:
            self = .RefreshToken
        }
    }
}

enum CellType : String {
    case RequestDocumentCell = "RequestDocumentTVC"
    case NotifiCell = "NotifiCell"
}

enum FlagType {
    
    case complete
    case not_complete
    case None
    
    var value : String {
        switch self {
        case .complete         : return "complete"
        case .not_complete     : return "not-complete"
        default                : return ""
        }
    }

    init(_ type:String) {
        switch type {
        
        case "complete"         : self = .complete
        case "not-complete"     : self = .not_complete
        default                 : self =  .None
        }
    }
}

enum TicketsType {
    
    case changed_phone
    case creditor_calling
    case counselling_session
    case stop_payment
    case request_document
    case extra_payment
    case talk_someone
    case alert
    case None
    
    
    var value : String {
        switch self {
        case .changed_phone         : return "changed_phone"
        case .creditor_calling      : return "creditor_calling"
        case .counselling_session   : return "counselling_session"
        case .stop_payment          : return "stop_payment"
        case .request_document      : return "request_document"
        case .extra_payment         : return "extra_payment"
        case .talk_someone          : return "talk_someone"
        case .alert                 : return "alert"
        default                     : return ""
        }
    }

    init(_ type:String) {
        switch type {
        
        case "changed_phone"        : self = .changed_phone
        case "creditor_calling"     : self = .creditor_calling
        case "counselling_session"  : self = .counselling_session
        case "stop_payment"         : self = .stop_payment
        case "request_document"     : self = .request_document
        case "extra_payment"        : self = .extra_payment
        case "talk_someone"         : self = .talk_someone
        case "alert"                : self = .alert
        default                     : self =  .None
        }
    }
}

enum NotificationType {

    case chat
    case new_alert
    case payment_reminder_10_days
    case payment_reminder_2_days
    case user_deactivate
    case user_delete
    case ticket_resolved
    case None
    
    var value : String {
        switch self {
        
        case .chat                      : return "chat"
        case .new_alert                 : return "new_alert"
        case .payment_reminder_10_days  : return "payment_reminder_10_days"
        case .payment_reminder_2_days   : return "payment_reminder_2_days"
        case .user_deactivate           : return "user_deactivate"
        case .user_delete               : return "user_delete"
        case .ticket_resolved           : return "ticket_resolved"
        default                         : return ""
            
        }
    }

    init(_ type:String) {
        switch type {
            
        case "chat"                         : self = .chat
        case "new_alert"                    : self = .new_alert
        case "payment_reminder_10_days"     : self = .payment_reminder_10_days
        case "payment_reminder_2_days"      : self = .payment_reminder_2_days
        case "user_deactivate"              : self = .user_deactivate
        case "user_delete"                  : self = .user_delete
        case "ticket_resolved"              : self = .ticket_resolved
        default                             : self = .None
        }
    }
}

enum IncomType : String {
    case Incomes = "incomes"
    case Expenses = "expenses"
}


enum StringType {
    
    case Image
    case PDF
    case None
    
    var value : String {
        switch self {
        
        case .Image  : return "image"
        case .PDF    : return "pdf"
        default      : return ""
            
        }
    }

    init(_ type:String) {
        switch type {
            
        case "image"  : self = .Image
        case "pdf"    : self = .PDF
        default       : self = .None
        }
    }
}


