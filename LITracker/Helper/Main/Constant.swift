//
//  Constant.swift
//  LITtracker
//
//  Created by Mohammed on 4/26/21.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase

let islive   = true

struct Constant {
    
    static var HeaderToken             = ["Accept" : "application/json",
                                          "Authorization" : "Bearer \(Auth_User.Token)"]
    
    static let DBRefrence    = islive ? Database.database().reference().child("live") : Database.database().reference().child("test")
        
    static var StorageRef    = Storage.storage().reference()
    
//MARK: if you change apiDomain to live or stage don't foreget change (isLive) to true or false
    static let ApiDomain     =
                "https://financialwellnesscentre.ca/portal/api/v1/" // Live
//        "http://littracker.ahdtech.com/api/v1/" // Stage
    
    
    //Grant Data
    static let client_id        = 2
    static let client_secret    = "PVuOuSdYRKT6l7n3ivMak9ghSNcl1kc2PyZFlVtL"
    static let device_id        = UIDevice.current.identifierForVendor?.uuidString ?? ""

    static let GoogleApiService =  "AIzaSyBpQ-dnfjkVSDBcnBdwGCa7PgKrab4Uhk4"
    static let GoogleLoginAuth  =  "888857804558-6p3suco1k211vuho69t2a193654vaf49.apps.googleusercontent.com"
    
    static let PageSize         =  10
    
    //Recent Tree
    static let RecentNode       = "Recent"
    static let Seen             = "Seen"
    static let UserNode         = "Users"
    static let chatRoomsNode    = "chat_rooms"
    
    static let Conversation     = "Conversation"
    static let counter          = "counter"
    
    //    chat rooms Tree
    static let TypeStatus       = "TypeStatus"
    static let TypeIndicator    = "TypeIndicator"
    static let messages         = "messages"
    static let Active           = "Active"
    static let lastMessage      = "lastMessage"
    
    
    static let RecentNodes        = DBRefrence.child(RecentNode)
    static let ConversationNodes  = DBRefrence.child(Conversation)
    static let UserNodes          = DBRefrence.child(UserNode)
    static let SeenNodes          = DBRefrence.child(Seen)
    
    static let  UserNoAvailable = NOT_EXIST
    
    static let Admin_Id = 0
    static var formsDictIncome  = [Int: [IncomeObject]]()
    static var formsDictExpense = [Int: [ExpenseObject]]()
}



enum UrlPath {
    
    case Instructions , Login , RefreshToken, RefreshFcmToken, Logout, User, ForgotPassword, changePassword
    case Notifications, DeleteNotification, UnseenNotification, SendChatNotification
    case Lookup, AddTicket, Logs, ReadAlert, TicketData(Int)
    case Home, paymentSchedule, FAQ
    case monthly_forms, incomeExpenseData, incomeExpenseDataUpdate
    case confirm_code, resend_code, activate, activate_two_factor_auth, attachments_form
    
    func path() -> String {
        var _path = ""
        switch self {
        
        case .Instructions:
            _path = "instructions"
        case .Login:
            _path = "login"
        case .RefreshFcmToken:
            _path = "refresh_fcm_token"
        case .RefreshToken:
            _path = "refresh_token"
        case .Logout:
            _path = "logout"
        case .ForgotPassword:
            _path = "forget"
        case .User:
            _path = "user"
        case .Home:
            _path = "home"
        case .paymentSchedule:
            _path = "payment_schedule"
        case .Notifications:
            _path = "notifications"
        case .DeleteNotification:
            _path = "notification"
        case .UnseenNotification:
            _path = "unseen-notification"
        case .Lookup:
            _path = "lookup"
        case .AddTicket:
            _path = "add_ticket"
        case let .TicketData(_id):
            _path = "ticket/\(_id)"
        case .Logs:
            _path = "logs"
        case .SendChatNotification:
            _path = "send-chat-notification"
        case .ReadAlert:
            _path = "read-alert"
        case .monthly_forms:
            _path = "monthly-forms"
        case .incomeExpenseData:
            _path = "income-expense-data"
        case .incomeExpenseDataUpdate:
            _path = "income-expense-data/update"
        case .changePassword:
            _path = "change-password"
        case .confirm_code:
            _path = "confirm_code"
        case .resend_code:
            _path = "resend_code"
        case .activate:
            _path = "activate"
        case .activate_two_factor_auth:
            _path = "activate-two-factor-auth"
        case .attachments_form:
            _path = "attachments-form"
        case .FAQ:
            _path = "faqs"
        }
        return Constant.ApiDomain + _path
    }
}




enum MethodType {
    case PUT, POST, GET, DELETE , NONE
    func path() -> String {
        switch self {
        case .PUT:
            return "PUT"
        case .POST:
            return "POST"
        case .GET:
            return "GET"
        case .DELETE:
            return "DELETE"
        case .NONE:
            return "NONE"
        }
        
    }
    
}
